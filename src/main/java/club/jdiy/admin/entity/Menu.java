package club.jdiy.admin.entity;

import club.jdiy.core.base.domain.JpaEntity;
import club.jdiy.core.base.domain.Sortable;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "jdiy_menu")
@org.hibernate.annotations.Table(appliesTo = "jdiy_menu", comment = "我会有表注释的哟...")
@NamedQueries({
        @NamedQuery(name = "Menu.findRootList",
                query = "SELECT o from Menu o where o.father is null order by o.sortIndex")
})
@NoArgsConstructor
public class Menu implements Sortable, JpaEntity<String> {
    @Id
    @Column(columnDefinition = "varchar(32)")
    private String id;
    @Column(length = 64)
    private String name;//菜单标题(后台左侧菜单显示)
    @Column(length = 16)
    private String type;//菜单类型:  list | tree | url | (empty string)
    @Column(length = 10)
    private String uid;//界面id
    private String url;
    private String icon;
    @Column(name = "sort")
    private Integer sortIndex;

    @Column(columnDefinition = "text comment '为目标界面附加URL参数．如：tid=5&foo=bar (目标界面可通过“${qo.参数名}”来取值)'")
    private String pageParam;

    @Column(nullable = false)
    private int state;//状态：0:正常  1:隐藏  2:仅开发者可见

    @Column(nullable = false)
    private boolean sys;//系统菜单不允许删除，不允许更改点击操作地址

    @ManyToOne
    @JoinColumn(name = "tid")
    private Menu father;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "father")
    @OrderBy("sortIndex ASC")
    private List<Menu> children;
    @Column(columnDefinition = "text comment '页面权限,存储用户手工添加的. 格式:＂权限名称:权限代码＂'")
    private String realms;

    @Transient
    private String pageParam_;

    @PreUpdate
    @PrePersist
    private void init() {
        if (sortIndex == null) sortIndex = 100;
    }

    public Menu(String id) {
        this.id = id;
    }

    public Menu(String name, String url) {
        this.name = name;
        this.url = url;
    }
}
