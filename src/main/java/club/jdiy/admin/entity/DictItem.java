package club.jdiy.admin.entity;

import club.jdiy.core.base.domain.JpaEntity;
import club.jdiy.core.base.domain.Sortable;
import club.jdiy.core.base.DslFilter;
import club.jdiy.core.base.domain.DslFilterable;
import club.jdiy.utils.StringUtils;
import com.querydsl.core.BooleanBuilder;
import lombok.Data;

import javax.persistence.*;

@Table(name = "jdiy_dict_item")
@Entity
@Data
public class DictItem implements JpaEntity<Long>, Sortable, DslFilterable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Dict dict;
    @Column(length = 64)
    private String txt;//显示文字
    @Column(length = 128)
    private String val;//记录值
    @Column(length = 32)
    private String color;//字典项颜色
    @Column(name = "sort", columnDefinition = "int default 100 not null")
    private Integer sortIndex;//排序索引

    @Column(columnDefinition = "bit default 0 not null")
    private Boolean hidden;//对(非开发者)隐藏
    @Column(columnDefinition = "bit default 1 not null")
    private Boolean editable;//(非开发者)是否允许修改此条目
    @Column(columnDefinition = "bit default 1 not null")
    private Boolean removable;//(非开发者)是否允许删除此条目

    public DictItem() {
    }

    public DictItem(Long id) {
        this.id = id;
    }


    @PreUpdate
    @PrePersist
    private void init() {
        if (sortIndex == null) sortIndex = 100;
    }

    @Override
    public DslFilter createFilter(BooleanBuilder builder) {
        QDictItem qo = QDictItem.dictItem;
        if (hidden != null) builder.and(qo.hidden.eq(hidden));
        if(dict!=null && StringUtils.hasText(dict.getId()))builder.and(qo.dict.eq(dict));

        return new DslFilter(builder, qo.sortIndex.asc());
    }

}
