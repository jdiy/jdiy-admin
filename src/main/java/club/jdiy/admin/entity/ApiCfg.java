package club.jdiy.admin.entity;


import club.jdiy.core.base.domain.JpaEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "jdiy_api")
public class ApiCfg implements JpaEntity<Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 512)
    private String file_path;
    @Column(columnDefinition = "text")
    private String file_content;
}
