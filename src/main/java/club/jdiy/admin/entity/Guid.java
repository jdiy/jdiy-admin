package club.jdiy.admin.entity;

import club.jdiy.core.base.domain.JpaEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * 生产编号
 */
@Entity
@Table(name="jdiy_guid")
@Data
public class Guid implements JpaEntity<String> {
    @Id
    private String id;//前缀
    @Column(nullable = false)
    private long num=1;//序号
}
