package club.jdiy.admin.entity;

import club.jdiy.core.base.DslFilter;
import club.jdiy.core.base.domain.DslFilterable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import club.jdiy.core.base.domain.JpaEntity;
import club.jdiy.core.base.domain.Sortable;
import club.jdiy.core.base.domain.Removable;
import com.querydsl.core.BooleanBuilder;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "jdiy_role")
public class Role implements Sortable, Removable, JpaEntity<Integer>, DslFilterable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 32)
    private String name;
    @Column(name = "sort")
    private Integer sortIndex;
    private String remark;
    @JsonIgnore
    @ManyToMany(mappedBy = "roleList", fetch = FetchType.LAZY)
    private List<User> userList;
    @Column(columnDefinition = "text")
    private String grantAuth;//授权信息
    private boolean sys;//是否系统角色(不允许更改授权和删除)
    //隐藏角色（在用户修改页，和角色列表页都不会显示
    //主要是为了非AdminUser用户要登录到后台的特殊需求，由编程方式为这些用户授权到这个隐藏角色）
    private boolean hidden;
    private Boolean removed;

    public Role() {
    }

    public Role(Integer id) {
        this.id = id;
    }

    @PreUpdate
    @PrePersist
    private void init(){
        if(this.removed==null)this.removed=false;
        if(sortIndex==null)sortIndex=100;
    }
    @Override
    public DslFilter createFilter(BooleanBuilder builder) {
        QRole qo = QRole.role;
        builder.and(qo.removed.isFalse()).and(qo.hidden.isFalse());

        return new DslFilter(builder, qo.sortIndex.asc());
    }
}
