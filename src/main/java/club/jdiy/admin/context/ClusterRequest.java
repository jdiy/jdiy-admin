package club.jdiy.admin.context;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ClusterRequest extends HttpServletRequestWrapper {
    private final HttpServletResponse response;

    ClusterRequest(HttpServletRequest request, HttpServletResponse response) {
        super(request);
        this.response = response;
    }

    @Override
    public HttpSession getSession() {
        return new ClusterSession(this, response, super.getSession());
    }
}


