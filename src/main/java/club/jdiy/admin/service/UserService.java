package club.jdiy.admin.service;

import club.jdiy.core.base.JDiyService;
import club.jdiy.admin.dao.UserDao;
import club.jdiy.admin.entity.User;

public interface UserService extends JDiyService<User, UserDao, Integer> {
    User changePwd(Integer userId, String oldPwd, String newPwd);

    User findUser(String uid);
    void updateProfile(User vo);

}