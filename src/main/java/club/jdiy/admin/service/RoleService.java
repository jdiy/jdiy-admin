package club.jdiy.admin.service;

import club.jdiy.core.base.JDiyService;
import club.jdiy.admin.dao.RoleDao;
import club.jdiy.admin.entity.Role;

import java.util.List;

public interface RoleService extends JDiyService<Role, RoleDao,Integer> {
}
