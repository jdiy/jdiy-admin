package club.jdiy.admin.service;

import club.jdiy.core.base.JDiyService;
import club.jdiy.admin.dao.GuidDao;
import club.jdiy.admin.entity.Guid;

public interface GuidService extends JDiyService<Guid, GuidDao, String> {
    long getNo(String s);
    String getNo(String s, int len);
}
