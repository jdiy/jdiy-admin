package club.jdiy.admin.service;

import club.jdiy.core.base.JDiyService;
import club.jdiy.admin.dao.DictDao;
import club.jdiy.admin.entity.Dict;

public interface DictService extends JDiyService<Dict, DictDao,String> {
}
