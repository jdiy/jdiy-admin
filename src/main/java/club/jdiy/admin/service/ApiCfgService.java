package club.jdiy.admin.service;

import club.jdiy.admin.dao.ApiCfgDao;
import club.jdiy.admin.entity.ApiCfg;
import club.jdiy.core.base.JDiyService;

public interface ApiCfgService extends JDiyService<ApiCfg, ApiCfgDao,Integer> {
}
