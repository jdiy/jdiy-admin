package club.jdiy.admin.service.impl;

import club.jdiy.admin.dao.DictDao;
import club.jdiy.admin.entity.Dict;
import club.jdiy.admin.service.DictService;
import club.jdiy.core.base.JDiyBaseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DictServiceImpl extends JDiyBaseService<Dict, DictDao, String>
        implements DictService {

    @Override
    @Transactional
    public Dict logicSave(Dict vo) {
        Dict po = dao.findById(vo.getId()).orElse(new Dict(vo.getId()));
        po.setName(vo.getName());
        po.setType(vo.getType());
        po.setSortIndex(vo.getSortIndex());
        po.setKeyName(vo.getKeyName());
        po.setValName(vo.getValName());
        po.setRemark(vo.getRemark());
        return dao.save(po);
    }
}
