package club.jdiy.admin.service.impl;

import club.jdiy.admin.dao.MenuDao;
import club.jdiy.admin.service.MenuService;
import club.jdiy.admin.entity.Menu;
import club.jdiy.core.base.JDiyBaseService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("jdiyMenuServiceDev")
public class MenuServiceImpl extends JDiyBaseService<Menu, MenuDao, String>
        implements MenuService {

    @Override
    public List<Menu> findRootList() {
        return dao.findRootList();
    }
}
