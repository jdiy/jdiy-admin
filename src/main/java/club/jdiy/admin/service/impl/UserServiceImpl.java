package club.jdiy.admin.service.impl;

import club.jdiy.admin.entity.User;
import club.jdiy.admin.service.UserService;
import club.jdiy.admin.dao.UserDao;
import club.jdiy.core.base.JDiyBaseService;
import club.jdiy.core.ex.JDiyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl extends JDiyBaseService<User, UserDao, Integer>
        implements UserService {
    @Override
    @Transactional
    public User changePwd(Integer userId, String oldPwd, String newPwd) {
        return dao.changePwd(userId, oldPwd, newPwd);
    }
    @Override
    @Transactional
    public void updateProfile(User vo) {
        User po = dao.findById(vo.getId()).orElseThrow(() -> new JDiyException("获取用户信息失败,请重新登录后再试!"));

        po.setName(vo.getName());
        po.setPhone(vo.getPhone());
        po.setEml(vo.getEml());
        po.setGender(vo.getGender());
        po.setRemark(vo.getRemark());
        dao.save(po);
    }

    @Override
    public User findUser(String uid) {
        return dao.findUser(uid);
    }
}
