package club.jdiy.admin.service.impl;

import club.jdiy.admin.dao.GuidDao;
import club.jdiy.admin.entity.Guid;
import club.jdiy.admin.service.GuidService;
import club.jdiy.core.base.JDiyBaseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class GuidServiceImpl extends JDiyBaseService<Guid, GuidDao, String>
        implements GuidService {

    @Override
    @Transactional
    public long getNo(String s) {
        return dao.getNo(s);
    }

    @Override
    @Transactional
    public String getNo(String s, int len)  {
        return dao.getNo(s,len);
    }
}
