package club.jdiy.admin.service.impl;

import club.jdiy.admin.dao.ApiCfgDao;
import club.jdiy.admin.entity.ApiCfg;
import club.jdiy.admin.service.ApiCfgService;
import club.jdiy.core.base.JDiyBaseService;
import org.springframework.stereotype.Service;

@Service
public class ApiCfgServiceImpl extends JDiyBaseService<ApiCfg, ApiCfgDao, Integer>
        implements ApiCfgService {



}
