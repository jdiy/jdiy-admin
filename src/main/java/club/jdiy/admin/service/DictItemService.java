package club.jdiy.admin.service;

import club.jdiy.core.base.JDiyService;
import club.jdiy.admin.dao.DictItemDao;
import club.jdiy.admin.entity.DictItem;

public interface DictItemService extends JDiyService<DictItem, DictItemDao,Long> {

}
