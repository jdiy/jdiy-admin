package club.jdiy.admin.service;


import club.jdiy.admin.dao.MenuDao;
import club.jdiy.admin.entity.Menu;
import club.jdiy.core.base.JDiyService;

import java.util.List;

public interface MenuService extends JDiyService<Menu, MenuDao,String> {
    List<Menu> findRootList();
}
