package club.jdiy.admin.freemarker.func;

import club.jdiy.core.AdminContext;
import club.jdiy.core.ex.JDiyException;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class HasRole implements TemplateMethodModelEx {
    public Object exec(List argList) throws TemplateModelException {
        if (argList.size() < 1) {
            throw new TemplateModelException("未指定角色id参数");
        }
        String code = ((SimpleScalar) argList.get(0)).getAsString();
        try {
            return adminContext.hasRole(Integer.parseInt(code));
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Resource
    private AdminContext adminContext;
}