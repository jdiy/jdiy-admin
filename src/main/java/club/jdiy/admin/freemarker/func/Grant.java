package club.jdiy.admin.freemarker.func;

import club.jdiy.core.AdminContext;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class Grant implements TemplateMethodModelEx {
    public Object exec(List argList) throws TemplateModelException {
        if (argList.size() < 1) {
            throw new TemplateModelException("未指定权限参数");
        }
        String code = ((SimpleScalar) argList.get(0)).getAsString();
        return adminContext.hasGrant(code);
    }

    @Resource
    private AdminContext adminContext;
}