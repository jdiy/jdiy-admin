package club.jdiy.admin.freemarker.func;

import freemarker.template.SimpleNumber;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import club.jdiy.utils.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CutText implements TemplateMethodModelEx {
    public Object exec(List argList) throws TemplateModelException {
        if (argList.size() < 2) {
            throw new TemplateModelException("错误的参数。");
        }
        Object ss = argList.get(0);
        if (ss != null) {
            String text = ((SimpleScalar) ss).getAsString();
            int cutNum = ((SimpleNumber) argList.get(1)).getAsNumber().intValue();
            return StringUtils.cut(StringUtils.htmlToText(text), cutNum);
        }
        return "";
    }
}