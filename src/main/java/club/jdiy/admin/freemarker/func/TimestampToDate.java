package club.jdiy.admin.freemarker.func;

import freemarker.template.SimpleNumber;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
@Component
public class TimestampToDate implements TemplateMethodModelEx {
    public Object exec(List argList) throws TemplateModelException {
        if (argList.size() < 1) {
            throw new TemplateModelException("错误的参数。");
        }
        long num = ((SimpleNumber) argList.get(0)).getAsNumber().longValue();
        return new Date(num*1000);
    }
}