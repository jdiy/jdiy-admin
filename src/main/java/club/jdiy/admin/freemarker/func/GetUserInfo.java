package club.jdiy.admin.freemarker.func;

import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import club.jdiy.admin.service.UserService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
@Component
public class GetUserInfo implements TemplateMethodModelEx {
    public Object exec(List argList) throws TemplateModelException {
        if (argList.size() < 1) {
            throw new TemplateModelException("错误的参数。");
        }
        int userId = Integer.parseInt (((SimpleScalar) argList.get(0)).getAsString());
        return userService.findById(userId).orElse(null);
    }

    @Resource
    private UserService userService;
}