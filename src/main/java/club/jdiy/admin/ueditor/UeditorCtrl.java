package club.jdiy.admin.ueditor;
import club.jdiy.core.AppContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("mgmt/ueditor")
public class UeditorCtrl {

    @RequestMapping(value = "process")
    public void bindAuthTT(HttpServletRequest request, HttpServletResponse response) {
        try {

            request.setCharacterEncoding( "utf-8" );
            response.setHeader("Content-Type" , "text/html");
            String rootPath =  appContext.getUploadDir().getAbsolutePath();
            response.getWriter().write( new ActionEnter( appContext,request, rootPath).exec() );
        } catch (Exception ignore) {
        }
    }
    @Resource
    private AppContext appContext;
}
