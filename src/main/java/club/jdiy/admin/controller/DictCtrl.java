package club.jdiy.admin.controller;

import club.jdiy.admin.entity.Dict;
import club.jdiy.admin.entity.DictItem;
import club.jdiy.admin.service.DictItemService;
import club.jdiy.admin.service.DictService;
import club.jdiy.core.ex.JDiyException;
import club.jdiy.admin.interceptor.GuestDisabled;
import club.jdiy.core.base.domain.Ret;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "mgmt/dict")
public class DictCtrl extends JDiyAdminCtrl<Dict, DictService> {
    @Override
    public String ls(Dict qo, String orderField, String orderDirection, Integer pageSize, Integer page, ModelMap map, HttpServletRequest request) {
        qo.setHidden(false);
        return super.ls(qo, orderField, orderDirection, pageSize, page, map, request);
    }

    @Override
    public String in(String id, ModelMap map, HttpServletRequest request) throws Exception {
        String ret = super.in(id, map, request);
        Dict vo = (Dict) map.get("vo");
        if ("0".equals(vo.getId())) {
            vo.setAddable(true);
            vo.setEditable(true);
            vo.setRemovable(true);
            vo.setHidden(false);
        }
        return ret;
    }

    @RequestMapping("{dictId}/ls")
    public String item_ls(@PathVariable String dictId,
                          @RequestParam(defaultValue = "15") Integer pageSize,
                          @RequestParam(defaultValue = "1") Integer page,
                          DictItem qo, ModelMap map) {
        map.put("dict", service.findById(dictId).orElse(null));
        qo.setDict(new Dict(dictId));
        qo.setHidden(false);
        map.put("pager", dictItemService.findPager(pageSize, page, qo));
        return "mgmt/dict/item_ls.ftl";
    }


    @RequestMapping("{dictId}/in")
    public String item_in(Long id, @PathVariable String dictId, ModelMap map) {
        map.put("dict", service.findById(dictId).orElse(null));
        DictItem vo = dictItemService.findById(id).orElseGet(() -> {
            DictItem vo1 = new DictItem();
            vo1.setId(0L);
            vo1.setSortIndex(100);
            return vo1;
        });
        map.put("vo", vo);
        return "mgmt/dict/item_in.ftl";
    }

    @RequestMapping("{dictId}/save")
    @ResponseBody
    @GuestDisabled
    public Ret<?> item_save(@PathVariable String dictId, DictItem vo) {
        try {
            vo.setDict(new Dict(dictId));
            dictItemService.logicSave(vo);
            return Ret.success();
        } catch (JDiyException le) {
            return Ret.error(le);
        }
    }


    @RequestMapping("deleteItem")
    @ResponseBody
    @GuestDisabled
    public Ret<?> deleteItem(DictItem vo) {
        try {
            dictItemService.delete(vo);
            return Ret.success();
        } catch (Exception le) {
            return Ret.fail("此信息已被系统业务使用．不能删除!");
        }
    }

    @Resource
    public void setService(DictService service) {
        this.service = service;
    }

    @Resource
    private DictItemService dictItemService;

}
