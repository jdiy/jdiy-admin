package club.jdiy.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorCtrl {
    @RequestMapping(value = "/etc/error/{code}")
    public String error(@PathVariable int code, Model model) {
        String pager = "errors/status";
        switch (code) {
            case 404:
                model.addAttribute("code", 404);
                pager = "errors/404.ftl";
                break;
            case 500:
                model.addAttribute("code", 500);
                pager = "errors/500.ftl";
                break;
        }
        return pager;
    }
}
