package club.jdiy.admin;

import club.jdiy.core.AppContext;
import club.jdiy.admin.interceptor.*;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.web.servlet.config.annotation.*;

import javax.annotation.Resource;

@SpringBootConfiguration
public class WebMvcConfig implements WebMvcConfigurer {
    @Resource
    private GlobalInterceptor allGlobalInterceptor;
    @Resource
    private JDiyAdminInterceptor JDiyAdminInterceptor;

    @Resource
    private AppContext appContext;

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.setUseSuffixPatternMatch(true); //设置是否是后缀模式匹配,即:/test.*
       // configurer.setUseTrailingSlashMatch(false);//设置是否自动后缀路径模式匹配（默认为true）,即：/test 可以匹配到/test/
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //addResourceLocations先添加的优先级更高，若先添加的路径下对应的资源不存在，则往后找，
        //因此下面优先使用外部资源，若外部资源不存在，再从jar内部取。

        registry.addResourceHandler("/static/**")
                .addResourceLocations("file:" + appContext.getStaticDir().getAbsolutePath() + "/")
                .addResourceLocations("classpath:/static/");

        registry.addResourceHandler("/uploads/**")
                .addResourceLocations("file:" + appContext.getUploadDir().getAbsolutePath() + "/")
                .addResourceLocations("classpath:/uploads/");


        registry.addResourceHandler("/jdiy-admin-resource/**")
                .addResourceLocations("file:" + appContext.getStaticDir().getAbsolutePath() + "/")
                .addResourceLocations("classpath:/club/jdiy/dev/view/res/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(JDiyAdminInterceptor).addPathPatterns("/mgmt/**").excludePathPatterns("/mgmt/login");
        registry.addInterceptor(allGlobalInterceptor).addPathPatterns("/**").excludePathPatterns("/static/**", "/uploads/**");
    }
}
