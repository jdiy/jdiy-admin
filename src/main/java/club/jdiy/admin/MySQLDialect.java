package club.jdiy.admin;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StringType;

@SuppressWarnings("unused")
public class MySQLDialect extends org.hibernate.dialect.MariaDBDialect {
    public MySQLDialect() {
        super();
        registerFunction("convert_charset",
                new SQLFunctionTemplate(StringType.INSTANCE, "convert(?1 using ?2)"));
        registerFunction("date_add",
                new SQLFunctionTemplate(StringType.INSTANCE, "DATE_ADD(?1 , INTERVAL ?2  DAY)"));

        registerFunction("group_concat", new StandardSQLFunction("group_concat", StringType.INSTANCE));
    }
}