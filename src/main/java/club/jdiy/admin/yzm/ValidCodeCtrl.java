package club.jdiy.admin.yzm;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RequestMapping("/etc")
@Controller
public class ValidCodeCtrl {
    @RequestMapping("yzm")
    public void print(@RequestParam(defaultValue = "yzm") String key, HttpSession session, HttpServletResponse response) throws IOException {
        ImageVerificationCode ivc = new ImageVerificationCode();
        session.setAttribute(key, ivc.getText());
        ivc.output(response.getOutputStream());
    }
}
