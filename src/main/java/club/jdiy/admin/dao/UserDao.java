package club.jdiy.admin.dao;

import club.jdiy.admin.entity.User;
import club.jdiy.core.base.JDiyDao;
import club.jdiy.core.cache.DelCache;
import club.jdiy.core.cache.UseCache;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao extends JDiyDao<User, Integer> {
    User findUser(String uid);

    boolean hasRepeat(User user);

    @DelCache(value = "user@{1}")
    User changePwd(Integer userId, String oldPwd, String newPwd);

    @Override
    @UseCache(value = "user@{1}")
    Optional<User> findById(Integer var1);

    @Override
    @DelCache(value = "user@{1.id}")
    <S extends User> S save(S var1);

    @Override
    @DelCache(value = "user@{1.id}")
    void delete(User var1);
}
