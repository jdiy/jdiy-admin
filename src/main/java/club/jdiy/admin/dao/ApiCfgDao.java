package club.jdiy.admin.dao;

import club.jdiy.admin.entity.ApiCfg;
import club.jdiy.core.base.JDiyDao;
import org.springframework.stereotype.Repository;



@Repository
public interface ApiCfgDao extends JDiyDao<ApiCfg,Integer> {

}
