package club.jdiy.admin.dao;

import club.jdiy.admin.entity.Dict;
import club.jdiy.admin.entity.DictItem;
import club.jdiy.core.base.JDiyDao;
import club.jdiy.core.base.domain.DictInfo;
import club.jdiy.core.cache.DelCache;
import club.jdiy.core.cache.UseCache;
import org.springframework.stereotype.Repository;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public interface DictItemDao extends JDiyDao<DictItem, Long> {
    List<DictItem> findAllByDictIdOrderBySortIndex(String dictId);
    DictItem findFirstByDictAndVal(Dict dict, String val);

    @UseCache(value = "dictInfo#{1}")
    default Map<String, DictInfo> get(String key) {
        return findAllByDictIdOrderBySortIndex(key)
                .stream().collect(Collectors.toMap(DictItem::getVal, DictInfo::new, (a, b) -> b, LinkedHashMap::new));
    }

    @Override
    @DelCache(value = {"dictInfo#{1.dict.id}"})
    <S extends DictItem> S save(S entity);

    @Override
    @DelCache(value = {"dictInfo#{1.dict.id}"})
    void delete(DictItem var1);
}
