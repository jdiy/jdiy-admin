package club.jdiy.admin.dao;
import club.jdiy.admin.entity.Role;
import club.jdiy.core.base.JDiyDao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleDao extends JDiyDao<Role,Integer> {
    @Query("select o from Role o where o.removed=false and o.hidden=false order by o.sortIndex")
    List<Role> findAll();
}
