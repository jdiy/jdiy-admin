package club.jdiy.admin.dao;

import club.jdiy.admin.entity.Guid;
import club.jdiy.core.base.JDiyDao;
import org.springframework.stereotype.Repository;

@Repository
public interface GuidDao extends JDiyDao<Guid,String> {
    long getNo(String s);
    String getNo(String s, int len);
}
