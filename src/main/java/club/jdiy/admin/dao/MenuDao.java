package club.jdiy.admin.dao;

import club.jdiy.admin.entity.Menu;
import club.jdiy.core.base.JDiyDao;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuDao extends JDiyDao<Menu,String> {
    List<Menu> findRootList();
}
