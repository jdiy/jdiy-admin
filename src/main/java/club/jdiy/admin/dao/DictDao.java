package club.jdiy.admin.dao;

import club.jdiy.admin.entity.Dict;
import club.jdiy.core.base.JDiyDao;
import club.jdiy.core.cache.DelCache;
import org.springframework.stereotype.Repository;

@Repository
public interface DictDao extends JDiyDao<Dict, String> {
    @Override
    @DelCache("dictInfo#{1}")
    void deleteById(String id);

    @Override
    @DelCache("dictInfo#{1.id}")
    void delete(Dict entity);

    @Override
    @DelCache("dictInfo#{1.id}")
    void insert(Dict entity);

    @Override
    @DelCache("dictInfo#{1.id}")
    Dict save(Dict entity);

}
