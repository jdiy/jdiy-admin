package club.jdiy.admin;

import club.jdiy.core.base.domain.ConfigBean;
import club.jdiy.core.base.domain.ConfigPrefix;
import lombok.Data;

/**
 * JDiy平台配置．
 */
@Data
@ConfigPrefix("jdiy")
public class SystemConfig implements ConfigBean {
    //应用程序名:
    private String projectName = "JDiy Admin";
    //后台单点登录开关
    private boolean singleLoginOn = true;
    //维护开关
    private boolean maintainOn;
    //维护期间文字显示
    private String maintainMsg;
    //维护路径(一行一个，只匹配以目标路径开头)如填：  /api/  则禁此路径下的所有访问
    private String maintainPaths;
    //后台单点登录开关
    private boolean yzmOn = true;

    private String welcomeUrl;
    private String notifyUrl;
    //版本信息:
    private String about = "<p>平台版本：JDiyAdmin v4.0</p>\n" +
            "<p>核心开发：子秋(QQ:39886616)</p>\n" +
            "<p>技术网站：<a href=\"http://jdiy.club\" target=\"_blank\">http://jdiy.club</a></p>";

}
