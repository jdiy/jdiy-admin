package club.jdiy.core.base.domain;

import club.jdiy.admin.entity.DictItem;
import lombok.Getter;

import java.io.Serializable;

@Getter
public class DictInfo implements Serializable {
    private final String name;
    private final String value;
    private final String color;

    public DictInfo(DictItem po) {
        this.name = po.getTxt();
        this.value = po.getVal();
        this.color = po.getDict().getType()==3?po.getColor():null;
    }

    public DictInfo(String name, String value, String color) {
        this.name = name;
        this.value = value;
        this.color = color;
    }
}
