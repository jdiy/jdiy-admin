package club.jdiy.dev.entity;

import club.jdiy.core.base.domain.JpaEntity;
import club.jdiy.core.base.DslFilter;
import club.jdiy.core.base.domain.DslFilterable;
import club.jdiy.dev.types.UiTpl;
import club.jdiy.utils.StringUtils;
import com.querydsl.core.BooleanBuilder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "jdiy_ui")
@Data
@NoArgsConstructor
public class JDiyUi implements JpaEntity<String>, DslFilterable {
    @Id
    @Column(length = 10)
    private String id;
    @Column(length = 64)
    protected String name;//显示名称
    @Column(length = 16)
    protected String bindType;//entity|table

    @Column(length = 16)
    @Enumerated(value = EnumType.STRING)
    protected UiTpl type;//类型： form|list|tree
    @Column(columnDefinition = "varchar(255) comment '绑定的实体类名'")
    protected String mainEntity;
    @Column(columnDefinition = "varchar(64) comment '绑定的表名(与mainEntity二选一)'")
    protected String mainTable;

    @Column(columnDefinition = "longtext comment '界面设计-配置信息'")
    private String metaData;

    public JDiyUi(String id) {
        this.id = id;
    }

    @Override
    public DslFilter createFilter(BooleanBuilder builder) {
        QJDiyUi qo = QJDiyUi.jDiyUi;
        if (type != null) builder.and(qo.type.eq(type));
        if (StringUtils.hasText(mainTable))
            builder.and(qo.mainTable.contains(mainTable).or(qo.mainEntity.contains(mainTable)));
        if (StringUtils.hasText(name)) builder.and(qo.name.contains(name));
        return new DslFilter(builder, qo.id.desc());
    }

    @Transient
    public String getEt_() {
        if ("entity".equals(bindType) && mainEntity != null) {
            return mainEntity.substring(mainEntity.lastIndexOf(".") + 1);
        }
        return mainTable;
    }
}
