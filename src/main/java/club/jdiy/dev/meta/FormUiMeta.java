package club.jdiy.dev.meta;

import club.jdiy.dev.types.PkTpl;
import club.jdiy.dev.types.UiTpl;
import club.jdiy.utils.JsonUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@SuppressWarnings("Lombok")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FormUiMeta extends UiMeta {
    private boolean ajaxSave;//提交到开发者自己的ajax地址去保存
    private String ajaxUrl;//保存地址(相对于/mgmt/ 当ajaxSave=true时有效)

    private String pk;
    private String pkPre;
    private PkTpl pkTpl;
    private int pkLen;
    private String hiddenParams;
    private InputMeta[] inputs;

    private String eventScript;

    public FormUiMeta() {
        super(UiTpl.form);
    }

    @Override
    public String toString() {
        return JsonUtils.stringify(this);
    }
}
