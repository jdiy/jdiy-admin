package club.jdiy.dev.meta;

import club.jdiy.dev.types.OptTpl;
import lombok.Data;
import java.io.Serializable;

@Data
public abstract class Opts  implements Serializable {
    private String id;//控件ID

    //===================当type=select|radio|checkbox时，以下字段有用：
    private String items;//-----(1)静态可选项条目内容 格式如："1:男"(一行一个选项)  又如：仅增加＂请选择＂一个静态项，就填":=请选择="
    private OptTpl optType;//动态数据来源类型

    private String optEntity;//---(2) A. 动态选项条目来源实体（与opTable二选一）
    private String optTable;//----(2) B. 动态选项条目来源表（与opEntity二选一）
    private String optTxtField;//----(2)动态选项条目显示字段
    private String optValField;//----(2)动态选项条目值字段
    private String optWhere;//----(2)动态选项附件查询条件
    private String optSort;//----(2)动态选项排序方式 格式如填:  "o.id asc, o.name desc"

    private String optDict;//----(3)动态选项从dict获取

    private String optQry;//----(4)动态选项直接从sql/jql获取
}
