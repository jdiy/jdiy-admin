package club.jdiy.dev.meta;

import club.jdiy.dev.types.RootPidTpl;
import club.jdiy.dev.types.UiTpl;
import club.jdiy.utils.JsonUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@SuppressWarnings("Lombok")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TreeUiMeta extends Lstr {

    private String nameField = "name";
    private String pidField = "parentId";
    private RootPidTpl rootPid = RootPidTpl.nullValue;
    private String sortField = "sortIndex";
    private String sortType = "asc";
    private String pathField = "sortPath";

    private boolean lazy = true;

    public TreeUiMeta() {
        super(UiTpl.tree);
    }
    @Override
    public String toString() {
        return JsonUtils.stringify(this);
    }
}
