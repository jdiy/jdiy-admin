package club.jdiy.dev.meta;

import club.jdiy.dev.types.UiTpl;
import club.jdiy.utils.JsonUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@SuppressWarnings("Lombok")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListUiMeta extends Lstr {

    private QoMeta[] qos;

    private int pageSize=15;
    private String defaultSort;

    private boolean excel;
    private String excelName;
    private int sumPos;

    public ListUiMeta() {
        super(UiTpl.list);
    }
    @Override
    public String toString() {
        return JsonUtils.stringify(this);
    }
}
