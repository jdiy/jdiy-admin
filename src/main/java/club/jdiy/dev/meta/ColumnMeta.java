package club.jdiy.dev.meta;

import club.jdiy.dev.types.ColFormatTpl;
import club.jdiy.dev.types.TreePathTpl;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ColumnMeta implements Serializable {
    private String id;
    private String field;
    private String label;

    //@ format=manyToOne|oneToMany|manyToMany时有效：
    private String joinTable;//外联表名
    private String joinField;//外联表显示字段
    private String joinRef;//(oneToMany时)，外部表的反向外键
    private String manyTable;//当为多对多中间时(format=ManyToMany), 指定多对多关系表
    private String manyField0;//当为多对多中间时(format=ManyToMany), 指定多对多关系表正向关系字段
    private String manyField1;//当为多对多中间时(format=ManyToMany), 指定多对多关系表反向关系字段

    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    private ColFormatTpl format;

    private String kv;//@ format=kv时有效
    private String dictId;//@format=dict时指定字典id
    //@format=tree时有效(树形层级路径显示)
    private String treeId;//树界面的id
    ////层级显示样式：可取值：
    private TreePathTpl treeDisplay;
    private String tpl;//@ format=tpl时有效
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


    private String align; //left|center|right
    private int width = 0;//0为自动分配
    private boolean sortable;
    private boolean sum=false;//(列表界面)底部合计

}
