package club.jdiy.dev.meta;

import club.jdiy.dev.types.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ViewTdMeta  implements Serializable {
    private String id;
    private String field;//字段
    private String label;

    private JoinTypeTpl joinType;

    //@ joinType=manyToOne|oneToMany|manyToMany时有效：
    private String joinTable;//外联表名
    private String joinField;//外联表显示字段
    private String joinRef;//(oneToMany时)，外部表的反向外键
    private String manyTable;//当为多对多中间时(format=ManyToMany), 指定多对多关系表
    private String manyField0;//当为多对多中间时(format=ManyToMany), 指定多对多关系表正向关系字段
    private String manyField1;//当为多对多中间时(format=ManyToMany), 指定多对多关系表反向关系字段

    private OutTpl type;//输出类型

    private String kv;//type=kv时填写k/v键值对
    private String dictId;//@type=dict时指定字典id
    //@format=tree时有效(树形层级路径显示)
    private String treeId;//树界面的id
    ////层级显示样式：可取值：
    private TreePathTpl treeDisplay;
    private String tpl;//@ format=tpl时有效



    private Integer row;//当前控件所在行
    private Integer col;//当前控件所在列
    private String preText;//前置文字
    private String aftText;//后置文字
}
