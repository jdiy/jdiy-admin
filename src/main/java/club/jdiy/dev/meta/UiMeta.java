package club.jdiy.dev.meta;

import club.jdiy.dev.types.UiTpl;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;

@Data
public abstract class UiMeta implements Serializable {
    @JsonIgnore
    protected String id;
    @JsonIgnore
    protected final UiTpl type;
    @JsonIgnore
    protected String bindType;
    @JsonIgnore
    protected String mainEntity;
    @JsonIgnore
    protected String mainTable;

    private String pageHandler;
    private String headTips;
    private String footTips;

    public UiMeta(UiTpl type) {
        this.type = type;
    }
}
