package club.jdiy.dev.meta;

import club.jdiy.dev.types.UiTpl;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ViewTabMeta  implements Serializable {
    private String id;
    private String label;//tab标题

    private Integer type;//tab打开类型(1:目标界面   2:自定义页面地址)

    private String goPageId;//type=1时，指定界面id
    private UiTpl goPageType;//type=1时．界面类型
    private String goPageParam;//type=1时,增加界面参数
    private String goPageFilter;//type=1时，为目标界面附加查询条件

    private String goUrl;//type=2时自定义页面
}
