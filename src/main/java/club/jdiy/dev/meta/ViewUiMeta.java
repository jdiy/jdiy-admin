package club.jdiy.dev.meta;

import club.jdiy.dev.types.UiTpl;
import club.jdiy.utils.JsonUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@SuppressWarnings("Lombok")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ViewUiMeta extends UiMeta {
    private String tabTitle;//当前主表信息的默认tab标题显示．
    private ViewTdMeta[] tds;

    private boolean btn;
    private ButtonMeta[] btns;

    private Integer tabPos;//子表tab位置：0:不显示(默认) 　1:页面上方  2:页面下方
    private ViewTabMeta[] tabs;
    private String sqlFilter;//数据查询条件，为空默认根据主键参数查询．

    public ViewUiMeta() {
        super(UiTpl.view);
    }

    @Override
    public String toString() {
        return JsonUtils.stringify(this);
    }
}
