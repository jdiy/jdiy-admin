package club.jdiy.dev.meta;

import club.jdiy.dev.types.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 表单界面里的输入控件条目配置信息
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InputMeta extends Opts {
    private String field;//绑定要保存的主表字段
    private String label;//控件标题

    //===============================================================================================不同控件类型不同配置
    private InputTpl type;//控件类型
    private FormatTpl format;//输入格式类型

    //===================当type="checkbox"时以下配置启用：
    //manyType用来表示checkbox打钩后数据更新方式，
    private ManyTpl manyType;
    //（2）  manyType=ManyToMany时：
    private String manyTable;//当为多对多中间时(manyType=ManyToMany), 指定多对多关系表
    private String manyField0;//当为多对多中间时(manyType=ManyToMany), 指定多对多关系表正向关系字段
    private String manyField1;//当为多对多中间时(manyType=ManyToMany), 指定多对多关系表反向关系字段

    //===================当type="text" && format='joinTable'||format='joinEntity' （外键反查）时，以下配置启用：
    // 关于外键反查：
    // 例如输入框用户输入 'foo', 则到joinTable中找joinField='foo'的记录，并把该记录的id值保存到当前输入框绑定的主表字段(field)
    private String joinTable;//指示要反查的数据表
    private String joinEntity;//指示要反查的实体
    private String joinField;//指示要反查的字段名称.

    //===================当type="treeSelect" 时，指定树界面id
    private String treeId;//树界面的id
    private boolean treeOnlyLeaf;//是否只能选叶子节点
    ////层级显示样式：可取值：
    private TreePathTpl treeDisplay;

    //===================当type="lookup"时以下配置启用：
    private String lookupId;//查找带回:界面id
    private Integer lookupWidth;//带回界面宽
    private Integer lookupHeight;//带回界面高
    private String lookupField;//带回显示字段
    private String lookupParam;//查找界面url参数
    private String lookupFilter;//查找界面附加sql条件
    private Boolean lookupMulti;//(0) 是否带回多选
    private ManyTpl lookupType;// (0) 多选带回的更新方式
    private String lookupJoinField;//(1) 查找带回OneToMany时,many表外键字段名
    private String lookupJoinTable;// (2) 查找带回ManytoMany时，中间关联表名
    private String lookupJoinField0;//(2) 查找带回ManytoMany时，中间关联表正向关联字段
    private String lookupJoinField1;//(2) 查找带回ManytoMany时，中间关联表反向关联字段

    //===============================================================================================不同控件类型不同配置


    //级联选项(使用级联的前置条件）：　
    // (1)父级控件类型必须是select|radio之一)
    // (2)当前控件类型必须是 select|radio|checkbox 之一
    // (3)当前控件的动态选项查询条件(或sql语句)中，包含 ${vo.XXXX}这样的内容（XXXX为级联触发的父级控件字段名）
    private boolean cascade;

    private int width;//控件宽度(自定义布局用)
    private String placeholder;//输入提示
    private String tips;//控件后面的提示文字（仅列布局有效。）
    private String initial;//输入框初始值（在数据添加界面，默认填入控件的值）
    private boolean required;//指示输入控件是否必填(选)项
    private int readonly;//只读选项： 0:否  1:总是  2:仅修改时
    private Integer maxlength;//最大长度（用于输入类型的控件，如单行文本框和滚动文本框）

    private String pwdType;//加密方式：clearText|md5|md5salt
    private String pattern;//正则验证匹配模式
    private String patternMsg;//验证不通过时显示给用户的提示信息
    private Integer height;//控件高

    private String ont;//type=switch时，开|关 显示文字， 要求格式例如："开|关"
    private String onv;//type=switch时，开|关 对应的值， 要求格式例如："1|0"

    private Boolean search;//type=select，指示下拉菜单是否可输入进行快速定位检索

    private Integer minChk;//（type=checkbox时）最少必须钩选数
    private Integer maxChk;//（type=checkbox时）最多只能钩选数
    private boolean existsChk;//验证重复（拿输入的内容到数据表中查找是否存在，如存在，则不允许保存）
    private String existsFilter;//验重附加查询条件.默认为: field='${vo.field}' and id<>'${vo.id}' 若填写,则在此基础上and
    private String existsMsg;//验证重复数据表中存在时，给用户的提示信息
    private String conditionShow;//显示条件（经ftl解析，返回true则显示，如填： vo.state==1）

    private Integer row;//当前控件所在行
    private Integer col;//当前控件所在列
    private FormLayoutTpl layout;//布局方式

    //===================当type=file时，下面配置有效：
    private String fileMultipart;//one|more  (是否开启多选)

    //false: 不写入JDiy Store对象, 仅以 fileUrl1|fileUrl2|fileUrl3 这样的路径形式存到数据表字段
    //true: 正常写入到Store对象（JDiy极力推荐的附件存储方式）
    private Boolean noStore;
    private Boolean fileDel;//是否允许删除已上传的
    private FileTypeTpl fileType;//允许上传文件类型
    private String fileExtensions;//允许上传的文件扩展名
    private RoomTpl room;
    private Integer roomW;//自动缩放限定宽
    private Integer roomH;//自动缩放限定高
    //对上传的文件重新命名的处理方式:
    //0 自动(重命名)
    //1:不重命名（与上传前的文件的名称一致，如果是多文件字段，则同名文件将被覆盖）
    //2:自己指定一个名字
    private int renameType;
    //当renameType=2时有效
    // 上传文件重命名的名称字符串(注意不要写扩展名)
    //　空着不填将改名为"文件字段名"+"扩展名"；
    // 若填写，例如fileName="foo"，当用户上传一个jpg格式的文件时,文件被重命名为：foo.jpg
    // 支持ftl表达式. 如填：${vo.id}, 若用户上传一个jpg文件，则文件被重命名为数据记录的 "id值"+".jpg" 如id=5时，文件名为 5.jpg
    // 如果是多文件上传模式，如filename="foo", 用户上传3个jpg文件时，文件名依次为：
    //      xx.jpg xx_1.jpg xx_2.jpg　依此类推，若有同名文件存在，则按顺序号往后编
    private String fileName;
    private Long fileSize;//限定上传文件字节大小（单位：kb）
}
