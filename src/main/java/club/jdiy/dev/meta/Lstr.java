package club.jdiy.dev.meta;

import club.jdiy.dev.types.UiTpl;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@SuppressWarnings("Lombok")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class Lstr extends UiMeta {
    private ButtonMeta[] topButtons;
    private ButtonMeta[] rowButtons;
    private ColumnMeta[] columns;
    private String sqlFilter;
    private boolean rowNum;
    private boolean mge = true;
    private boolean rowBtnDropdown;
    private Integer mgeWidth;
    private String tableHeight;// <empty> |  number  | full-n

    public Lstr(UiTpl type) {
        super(type);
    }
}
