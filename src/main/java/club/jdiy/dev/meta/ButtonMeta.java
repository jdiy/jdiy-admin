package club.jdiy.dev.meta;

import club.jdiy.dev.types.BtnActTpl;
import club.jdiy.dev.types.BtnTargetTpl;
import club.jdiy.dev.types.UiTpl;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ButtonMeta implements Cloneable, Serializable {
    private String id;
    private String text;
    private String icon;
    private String bg;
    private String fg;
    private BtnActTpl act;
    private BtnTargetTpl target;//目标界面或页面打开方式：dialog | tab || 外部地址(link)，是否还要？->：ifdialog, iftab, _blank
    private String updateCode;//act=exec时指定要更新信息
    private String goPageId;//act=page时，指定界面id
    private UiTpl goPageType;//act=page时．界面类型
    private String title;//按钮打开的界面标题
    private String pageParam;//act=page时,界面参数
    private String outLink;//act=link时的外链
    private String ajaxUrl;//act=ajax时指定自定义ajax请求地址
    private Boolean footer;//是否显示底部操作按钮(仅“打开外部链接”且为弹窗时，此配置有效)
    private Boolean dbl;//是否绑定行双击
    private String confirm;//操作确认
    private Integer width;
    private Integer height;
    private String grantCode;//授权代码
    private String depthShow;//树界面:按钮显示层级( a:顶层 e:末层  1~6层, 如： e,1,3)
    private String conditionShow;//显示条件（经ftl解析，返回true则显示，如填： vo.state==1）

    @Override
    public ButtonMeta clone() throws CloneNotSupportedException {
        return (ButtonMeta) super.clone();
    }
}
