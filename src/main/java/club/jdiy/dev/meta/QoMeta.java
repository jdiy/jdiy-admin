package club.jdiy.dev.meta;

import club.jdiy.dev.types.OperTpl;
import club.jdiy.dev.types.QoJoinTpl;
import club.jdiy.dev.types.QoTpl;
import club.jdiy.dev.types.TreePathTpl;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QoMeta extends Opts {
    private String label;
    private String placeholder;
    private String field;

    private QoTpl type;// 控件类型

    //级联选项(使用级联的前置条件）：　
    // (1)父级控件和当前控件类型都是 select
    // (3)当前控件的动态选项查询条件(或sql语句)中，包含 ${qo.XXXX}这样的内容（XXXX为级联触发的父级控件字段名）
    private boolean cascade;

    //===================当type="joinTable" 时:
    private QoJoinTpl joinType;//外联查询类型: none | manyToOne | oneToMany | manyToMany | sql
    private String joinTable;//外联表名
    private String joinField;//外联查询字段
    private String joinRef;//(oneToMany时)，外部表的反向外键
    private String manyTable;//当为多对多中间时(format=ManyToMany), 指定多对多关系表
    private String manyField0;//当为多对多中间时(format=ManyToMany), 指定多对多关系表正向关系字段
    private String manyField1;//当为多对多中间时(format=ManyToMany), 指定多对多关系表反向关系字段
    private String sql;//当joinType=sql时有用．

    //===================当type="treeSelect" 时:
    private String treeId;//树界面的id
    private boolean treeOnlyLeaf;//是否只能选叶子节点
    private TreePathTpl treeDisplay;//层级显示样式

    private OperTpl oper;
    private String initial;//默认值（若有值，列表查询时默认带上该条件）
    private int width;//0为自动
}

