package club.jdiy.dev.dao;

import club.jdiy.core.base.JDiyDao;
import club.jdiy.core.cache.DelCache;
import club.jdiy.core.cache.UseCache;
import club.jdiy.dev.entity.JDiyUi;
import club.jdiy.dev.meta.UiMeta;
import club.jdiy.dev.types.UiTpl;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JDiyUiDao extends JDiyDao<JDiyUi, String> {
    JDiyUi findFirstByTypeAndMainTable(UiTpl type, String mainType);

    boolean existsByNameAndIdNot(String name, String id);

    @Modifying
    @Query("update Menu o set o.realms=?2 where o.uid=?1 and o.type in ('list','tree')")
    void updateMenuRealms(String uid, String realms);

    @Override
    @DelCache("JDiyUiMetaCache-{1.id}")
    <S extends JDiyUi> S save(S entity);

    @Override
    @DelCache("JDiyUiMetaCache-{1}")
    void deleteById(String s);

    @Override
    @DelCache("JDiyUiMetaCache-{1.id}")
    void delete(JDiyUi entity);

    @Override
    @DelCache("JDiyUiMetaCache-{1.id}")
    void insert(JDiyUi entity);

    @UseCache("JDiyUiMetaCache-{1}")
    <T extends UiMeta> T getUiMeta(String id);

    List<String> getRealms(String id);
}
