package club.jdiy.dev.view;

public class ListWhere {
    private String where = "1=1";

    public ListWhere and(String s) {
        if (s != null && !"".equals(s.trim())) {
            where = "1=1".equals(where) ? s : where + " AND " + s;
        }
        return this;
    }

    public String toString() {
        return where;
    }
}
