package club.jdiy.dev.view;

import club.jdiy.core.sql.Rs;
import club.jdiy.dev.meta.ButtonMeta;

public interface TreeHandler extends TlBaseHandler {

    /**
     * 在树界面渲染显示前，对数据进行预处理增强．（例如可对部分字段内容修改或格式化处理等）
     * <br/><br/><strong>示例：</strong>(仅演示用法，不代表实际业务)<br/>
     * 假设数据表中有一个hits字段，当其值小于100时，在页面上直接显示出来，否则页面上就只显示＂99+＂:<br/><br/>
     * <pre>
     * public void onView(Rs nodeData, int depth) {
     *      Integer hits = nodeData.getInt("hits");
     *      if(hits!=null && hits>=100) nodeData.set("hits","99+");
     * }
     * </pre>
     *
     * @param nodeData 节点数据
     */
    default void onView(Rs nodeData, int depth) {
    }

    /**
     * 对数据行右边管理栏中的管理按钮进行增强处理．比如修改按钮颜色，文字，以及根据条件判断按钮是否显示等．<br/>
     * <br/><strong>示例：</strong>(仅演示用法，不代表实际业务)<br/><br/>
     * 假如数据记录有一个state字段，当其值为1时，不显示[删除]按钮, 当state=2时，[修改]按钮的背景显示为红色：<br/><br/>
     * <pre>
     * public boolean onButton(ButtonMeta buttonMeta, Rs rowData) {
     *
     *  　　//下行用于判断是[删除]按钮. 当然也可以通过 buttonMeta.getText()判断按钮类型
     *     if(buttonMeta.act==BtnActTpl.del){
     *         return state!=1; //state=1时，返回false,也就是该按钮不在页面上显示
     *
     *     }else if(buttonMeta.act==BtnActTpl.edit){//是[修改]按钮
     *         if(rowData.getInt("state")==2) buttonMeta.setBg("#ff0000"); //当记录的state==2时,设置[修改]按钮背景为红色
     *     }
     *     return true;//返回true，显示该按钮
     *
     * }
     * </pre>
     *
     * @param buttonMeta button的Meta配置信息．因为一个界面上可以配置多个按钮，此参数用来区分是哪个按钮，同时可修改目标按钮相应字段的值改变其属性或行为.
     * @param rowData    当前行数据
     * @return 若返回false, 则不显示目标按钮
     */
    default boolean onButton(ButtonMeta buttonMeta, Rs rowData, int depth) {
        return true;
    }
}
