package club.jdiy.dev.view;

import club.jdiy.core.ex.JDiyException;
import club.jdiy.dev.meta.ButtonMeta;

public interface TlBaseHandler {

    /**
     * 当点击列表界面中的删除按钮（包括行右侧管理栏中的删除按钮，或者列表顶部配置的批量删除按钮）时，目标数据被删除之前，会先执行此增强处理.<br/>
     * 例如根据业务逻辑，判断数据是否允许被删除，或者在删除的同时，做一些其它事情．<br/>
     * 如果是执行的是批量删除，其中任意一条数据删除失败，整个事务都将回滚（批量删除是在同一个事务中执行的）<br/>
     * 针对批量删除（例如用户钩选了３条数据进行操作），此方法会被执行３次（一条数据一次）
     *
     * @param dataId 要删除的数据id
     * @return （１）当此方法返回 false时，表示您已经在此方法内部执行了目标数据的删除（JDiy不会再对该数据进行处理了），将直接返回操作成功的提示信息告知用户；<br/>
     * （２）当此方法返回true时，JDiy后续代码即对目标数据执行删除操作，并将删除结果告知用户；<br/>
     * （３）当此方法抛出异常，JDiy将取消本次删除操作，并将异常的错误信息(message)反馈显示给用户（例如经过增强处理，开发者觉得这条数据不能被删除，就直接抛出异常告知用户＂数据已被业务使用，您暂时无法删除＂）
     */
    default boolean onDelete(String dataId) {
        return true;
    }

    /**
     * 当点击列表界面的相应的更新按钮（即配置的操作是＂更新字段值＂的那些按钮）时，会先进入到这个方法进行增强处理．　<br/>
     * 例如您可以在更新之前做一些前置判断，或者其它相关的数据同步更新等等．
     * 如果是执行的是批量更新，其中任意一条数据更新失败，整个事务都将回滚（批量更新是在同一个事务中执行的）<br/>
     * 针对批量更新（例如用户钩选了３条数据进行操作），此方法会被执行３次（一条数据一次）
     *
     * @param dataId 要更新的数据id
     * @return （１）当此方法返回 false时，表示您已经在此方法内部执行了目标数据的更新（JDiy不会再对该数据进行更新处理了），将直接返回操作成功的提示信息告知用户；<br/>
     * （２）当此方法返回true时，JDiy后续代码即对目标数据执行更新操作，并将更新结果告知用户；<br/>
     * （３）当此方法抛出异常，JDiy将取消本次更新操作，并将异常的错误信息(message)反馈显示给用户．
     */
    default boolean onUpdate(String dataId) {
        return true;
    }

    /**
     * 当点击列表界面的相应执行按钮（即该按钮配置的操作是＂执行代码(列表增强)＂）时，会进入到这个方法进行处理．　<br/>
     * 如果是列表界面打钩选择多条数据后批量操作，此方法会针对每条记录各执行一次．<br/><br/>
     * <strong>示例：</strong><br/>
     * <pre>
     * public Ret&lt;?&gt; execute(String[] ids, ButtonMeta buttonMeta) {
     *    if("审核通过".equals(buttonMeta.getText()){ //用于确认目标按钮是不是您要增强处理的那个按钮，这儿直接根据配置的按钮文字来区分了
     *        //根据id查询出目标数据，做相应增强处理
     *        //若此方法抛出异常，JDiy会将异常的message信息显示给用户.
     *    }
     * }
     *</pre>
     *
     * @param dataId 要执行代码的目标数据id.
     * @param buttonMeta 因为一个界面上可能配置多个执行按钮，此参数用于区分它们．
     */
    default void execute(String dataId, ButtonMeta buttonMeta) {
        throw new JDiyException("请在列表增强类的execute方法中判断目标按钮并编写执行代码，由于您未实现execute方法，本次操作未进行任何处理。");
    }
}
