package club.jdiy.dev.view;

import club.jdiy.core.sql.Rs;

import java.util.Map;

public interface ViewHandler extends TlBaseHandler{
    /**
     * 渲染显示之前，对数据模型预处理增强.
     *
     * @param viewData    当前的数据库记录对象封包
     * @param queryParams 页面请求参数model对象Map(Key为请求参数，value为参数值)
     */
    default void onView(Rs viewData, Map<String, String> queryParams) {
    }
}
