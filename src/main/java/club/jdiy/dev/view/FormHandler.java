package club.jdiy.dev.view;

import club.jdiy.core.sql.Rs;

import java.util.Map;

public interface FormHandler {

    /**
     * 渲染表单之前，对数据模型预处理增强.
     *
     * @param formData    当前的数据库记录对象封包
     * @param queryParams 页面请求参数model对象Map(Key为请求参数，value为参数值)
     */
    default void onView(Rs formData, Map<String, String> queryParams) {
    }

    /**
     * 表单在保存到数据库之前的增强处理.
     * <br/>(例如自定义验证，相关数预处理等)
     * <br/>
     * <strong>业务实现</strong>：<br/>
     * 　　系统根据表单从底层数据表中查询到目录记录并封包为Rs对象（若查不到，就创建一个，此时formData.isNew()=true）<br/>
     * 　　然后将表单提交过来的数据预处理后合并到formData,等待存库．在准备存库之前执行此增强处理方法．<br/>
     * 　　若此方法抛出运行时异常，则事务回滚(当前保存操作中止)，并返回错误信息至表单页面。
     *
     * @param formData 当前表单数据对象．
     */
    default void beforeSave(Rs formData) {
    }

    /**
     * 表单数据在提交保存之后(事务commit之前)的增强处理.
     * <br/>(例如自定义其它关联表数据更新)
     * <br/>
     * 若此方法抛出运行时异常，则事务回滚(当前保存操作中止)，并返回错误信息至表单页面。
     *
     * @param formData 当前表单（已持久化保存到数据库之后的）数据对象
     */
    default void afterSave(Rs formData) {
    }


    /**
     * 表单在提交保存全部完成之后的增强处理.
     * <br/>(与{@link #afterSave(Rs)}不同的是，此方法已脱离数据库事务环境．
     * <br/>例如带附件上传的表单提交，附件的持久化保存操作是在事务外执行的．
     * 此方法会在所有保存操作全部完成后，最后被调用（此方法内可以通过Store正常取到当前formData已上传的附件信息,
     * 而 {@link #afterSave(Rs)}则不行，因为{@link #afterSave(Rs)}是在附件存储处理之前就已经被调用）．
     * <br/>
     * 这意味着，无论此方法是否正常执行（是否抛出异常），都不影响此方法之前的数据持久化事务已经提交的结果．
     *
     * @param formData 当前表单（已持久化保存到数据库之后的）数据对象
     */
    default void completeSave(Rs formData) {
    }
}
