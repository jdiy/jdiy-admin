package club.jdiy.dev.types;

import lombok.Getter;

/**
 * checkbox关联更新类型
 */
@Getter
public enum ManyTpl implements Tpl {
    none("无(本表字段直接存储)"),
    OneToMany("一对多外键反向更新"),
    ManyToMany("多对多关系表更新"),
    ;
    private final String value;

    ManyTpl(String value) {
        this.value = value;
    }
}
