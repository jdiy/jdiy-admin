package club.jdiy.dev.types;

import lombok.Getter;

@Getter
public enum BtnTargetTpl implements Tpl {
    dialog("弹出对话框"),
    tab("新Tab标签"),
    ajaxTodo("执行ajax请求"),
    _blank("浏览器新页面"),
    ;

    private final String value;

    BtnTargetTpl(String value) {
        this.value = value;
    }

}
