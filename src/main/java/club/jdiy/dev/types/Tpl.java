package club.jdiy.dev.types;

import java.io.Serializable;

public interface Tpl extends Serializable {
    String getValue();
}
