package club.jdiy.dev.types;

import club.jdiy.dev.meta.*;
import lombok.Getter;

/**
 * 配置树表顶层节点的parentId取值.
 */
@Getter
public enum UiTpl implements Tpl {
    list("分页列表", "layui-icon-table", ListUiMeta.class),
    tree("树形表格", "layui-icon-tree", TreeUiMeta.class),
    form("表单录入", "layui-icon-form", FormUiMeta.class),
    view("详情查看", "layui-icon-read", ViewUiMeta.class);

    private final String value;
    private final String icon;
    public final Class<? extends UiMeta> metaClass;

    UiTpl(String value, String icon, Class<? extends UiMeta> c) {
        this.value = value;
        this.icon = icon;
        this.metaClass = c;
    }
}
