package club.jdiy.dev.types;

import lombok.Getter;

@Getter
public enum OptTpl implements Tpl {
    table("数据库表"),
    entity("JPA实体"),
    dict("数据字典"),
    sql("SQL查询"),
    jql("JQL查询");

    private final String value;

    OptTpl(String value) {
        this.value = value;
    }
}
