package club.jdiy.dev.types;

import lombok.Getter;

/**
 * 输入控件类型枚举
 */
@Getter
public enum InputTpl implements Tpl {
    input("input 单行文本输入"),
    textarea("textarea 多行文本输入"),
    switcher("switch 开关"),
    select("select 下拉菜单"),
    radio("radio 单选框"),
    checkbox("checkbox 多选框"),
    lookup("lookup 查找带回"),
    treeSelect("treeSelect 树形下拉菜单"),
    file("file 文件上传"),
    ueditor("ueditor 富文本编辑器"),
    colorbox("colorbox 颜色选择器"),
    ;

    private final String value;

    InputTpl(String value) {
        this.value = value;
    }

}
