package club.jdiy.dev.types;

import lombok.Getter;

@Getter
public enum OperTpl implements Tpl {
    eq("等于", "="),
    ne("不等于", "<>"),
    lt("小于", "<"),
    gt("大于", ">"),
    lte("小于等于", "<="),
    gte("大于等于", ">="),
    like("包含", " LIKE "),
    not_like("不包含", " NOT LIKE "),

    treeOnly("只查询本层直属数据",""),
    treeAll( "本层及全部子级数据","")
    ;

    private final String text;
    private final String value;

    OperTpl(String text, String value) {
        this.text = text;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public String getValue() {
        return value;
    }
}
