package club.jdiy.dev.types;

import lombok.Getter;

@Getter
public enum FormatTpl implements Tpl {
    text("普通文本"),
    pwd("密码框"),
    pwdAgain("密码确认"),
    integer("整数"),
    number("整数或小数"),
    datetime("日期和时间"),
    date("日期"),
    time("时间"),
    phone("手机号"),
    email("邮件地址"),
    url("网址"),
    sfz("身份证号"),
    joinEntity("外键反查(JPA实体)"),
    joinTable("外键反查(数据表)");

    private final String value;

    FormatTpl(String value) {
        this.value = value;
    }
}
