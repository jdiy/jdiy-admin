package club.jdiy.dev.types;

import lombok.Getter;

/**
 * 搜索控件外联表查询
 */
@Getter
public enum QoJoinTpl implements Tpl {
    manyToOne("多对一(外键反查)"),
    oneToMany("一对多(子表查询)"),
    manyToMany("多对多(子表查询)"),
    sql("自定义SQL查询"),
    ;
    private final String value;

    QoJoinTpl(String value) {
        this.value = value;
    }
}
