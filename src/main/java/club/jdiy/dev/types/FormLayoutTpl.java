package club.jdiy.dev.types;

import lombok.Getter;

@Getter
public enum FormLayoutTpl implements Tpl{
    inline("列布局"),
    col12("栅格化(占整行)"),
    col6("栅格化(占半行)"),
    col4("栅格化(占1/3行)"),
    col3("栅格化(占1/4行)"),
    free("自定义");

    private final String value;

    FormLayoutTpl(String value) {
        this.value = value;
    }
}
