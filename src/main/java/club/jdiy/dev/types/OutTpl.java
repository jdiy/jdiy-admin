package club.jdiy.dev.types;

import lombok.Getter;

/**
 * 输出控件类型枚举
 */
@Getter
public enum OutTpl implements Tpl {
    direct("直接输出"),
    kv("(K/V)键值转键名"),
    dict("数据字典值转键名"),
    img("图片(缩图显示)"),
    file("附件(链接显示)"),
    tree("树层级"),
    tpl("自定义模板");

    private final String value;

    OutTpl(String value) {
        this.value = value;
    }

}
