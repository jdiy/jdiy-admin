package club.jdiy.dev.types;

import lombok.Getter;

@Getter
public enum BtnActTpl implements Tpl{
    add("添加"),
    edit("修改"),
    view("详情页"),
    del("删除"),
    page("打开新界面"),
    post("选中项post到自定义界面"),
    link("打开自定义页面"),
    update("更新字段值"),
    ajax("执行自定义Ajax请求"),
    exec("执行代码(列表增强)");

    private final String value;

    BtnActTpl(String value) {
        this.value = value;
    }

}
