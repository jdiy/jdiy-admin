package club.jdiy.dev.types;

import lombok.Getter;

import static club.jdiy.dev.types.OperTpl.*;

/**
 * 搜索控件类型
 */
@Getter
public enum QoTpl implements Tpl {
    text("文本输入", new OperTpl[]{eq, ne, like, not_like}),
    select("下拉菜单", new OperTpl[]{eq, ne, lt, gt, lte, gte, like, not_like}),
    treeSelect("树形下拉菜单", new OperTpl[]{treeAll, treeOnly}),
    datetime("日期和时间", new OperTpl[]{eq, ne, lt, gt, lte, gte}),
    date("日期", new OperTpl[]{eq, ne, lt, gt, lte, gte}),
    integer("整数输入", new OperTpl[]{eq, ne, lt, gt, lte, gte}),
    number("小数或整数", new OperTpl[]{eq, ne, lt, gt, lte, gte});

    private final String value;
    private final OperTpl[] opers;

    QoTpl(String value, OperTpl[] opers) {
        this.value = value;
        this.opers = opers;
    }

}
