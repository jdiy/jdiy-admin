package club.jdiy.dev.types;

import lombok.Getter;

@Getter
public enum FileTypeTpl implements Tpl{
    any("不限制(任意文件)","*/*"),
    image("图片文件(jpg,png,gif)","image/jpeg,image/png,image/gif"),
    word("Word文档(doc,docx)","application/wps-office.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
    excel("Excel文档(xls,xlsx,csv)","application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,text/csv"),
    office("office办公类,pdf,txt等","application/msword,application/vnd.ms-excel,application/mspowerpoint,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.ms-access,application/pdf,text/plain,application/rtf,text/csv"),
    txt("纯文本文档(txt)","text/plain,text/csv"),
    apk("安卓安装包(*.apk)","application/vnd.android,application/vnd.android.package-archive"),
    ipa("IOS安装包(*.ipa)","application/iphone"),
    archive("压缩包(rar,zip,7z,tgz等)","application/zip,application/x-rar-compressed,application/x7zcompressed,application/x-gtar,application/x-bzip2,application/x-gzip"),
    music("音乐(mp3,wma,midi)","audio/x-mpeg,audio/x-ms-wma,audio/midi,audio/x-midi"),
    custom("自定义扩展名","*/*");

    private final String value;
    private final String mime;

    FileTypeTpl(String value,String mime) {
        this.value = value;
        this.mime=mime;
    }
}
