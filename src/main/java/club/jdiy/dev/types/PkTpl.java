package club.jdiy.dev.types;

import lombok.Getter;

/**
 * ID生成策略
 */
@Getter
public enum PkTpl implements Tpl{
    db("数据库自增主键"),
    char10("JDiy10位字符串定长"),
    times("不重复毫秒值"),
    nows("YYYYmmddHHmmss+3位数字"),
    dates("YYYYmmdd+顺序号"),
    pres("自定义前缀+顺序号"),
    uuid("32位UUID");

    private final String value;

    PkTpl(String value) {
        this.value = value;
    }
}
