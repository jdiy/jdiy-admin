package club.jdiy.dev.types;

import lombok.Getter;

/**
 * 图片缩放类型
 * <p>
 * 当上传类型为图片时，是否开启自动缩放
 * none:    不缩放
 * only:    等比缩放至尺寸范围（一长的一边为准）
 * white:   等比缩放至尺寸大小（空白的用白色填充）
 * opacity: 等比缩放至尺寸大小(空白的用透底填充，注意仅gif|png支持透底)
 * force:   强制拉伸（或压缩）变形至目标尺寸大小。
 */
@Getter
public enum RoomTpl implements Tpl {
    none("不做处理"),
    only("等比缩放至尺寸范围"),
    white("等比缩放(白底填充)"),
    opacity("等比缩放(透底填充)"),
    force("直接拉伸(变形)"),
    ;

    private final String value;

    RoomTpl(String value) {
        this.value = value;
    }

}
