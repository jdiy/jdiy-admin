package club.jdiy.dev.types;

import lombok.Getter;

@Getter
public enum ColFormatTpl implements Tpl {
    text("直接显示", "常规格式"),
    datetime("日期和时间", "常规格式"),
    date("仅日期", "常规格式"),
    time("仅时间", "常规格式"),
    number("保留两位小数格式", "常规格式"),
    kv("(K/V)键值转键名", "常规格式"),
    dict("数据字典值转键名", "常规格式"),

    manyToOne("(多对一)外联字段", "外联引用"),
    oneToMany("(一对多)外联列表", "外联引用"),
    manyToMany("(多对多)外联列表", "外联引用"),
    tree("树形节点路径层级", "外联引用"),
    //
    img("附件（缩略图）", "其它格式"),
    file("附件（文件名链接）", "其它格式"),
    tpl("自定义模板", "其它格式");

    private final String value;
    private final String group;

    ColFormatTpl(String value, String group) {
        this.value = value;
        this.group = group;
    }

}
