package club.jdiy.dev.types;

import lombok.Getter;

@Getter
public enum JoinTypeTpl implements Tpl {
    none("无"),
    manyToOne("(多对一)外联字段"),
    oneToMany("(一对多)外联列表"),
    manyToMany("(多对多)外联列表");

    private final String value;

    JoinTypeTpl(String value) {
        this.value = value;
    }

}
