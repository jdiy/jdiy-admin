package club.jdiy.dev.types;

import lombok.Getter;

/**
 * 配置树表顶层节点的parentId取值.
 */
@Getter
public enum RootPidTpl implements Tpl {
    nullValue("空值null (推荐)", " is null", null),
    emptyString("空字符串", "=''", ""),
    zero("数字0", "=0", 0);

    private final String value;
    private final String operationString;
    private final Object dataValue;

    RootPidTpl(String value, String operationString, Object dataValue) {
        this.value = value;
        this.operationString = operationString;
        this.dataValue = dataValue;
    }
}
