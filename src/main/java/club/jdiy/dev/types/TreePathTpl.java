package club.jdiy.dev.types;

import lombok.Getter;

/**
 * 所在节点层路径显示
 */
@Getter
public enum TreePathTpl implements Tpl {
    separator("/", "层级显示样式： A/B/C"),
    arrow(" &gt; ", "层级显示样式： A&gt;B&gt;C"),
    space(" ", "层级显示样式： A B C"),
    serial("", "层级显示样式： ABC"),
    leaf("leaf", "仅显示末层： C");

    private final String value;
    private final String text;

    TreePathTpl(String value, String text) {
        this.value = value;
        this.text = text;
    }

}
