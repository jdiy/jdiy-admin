package club.jdiy.dev.service;

import club.jdiy.core.base.JDiyService;
import club.jdiy.core.sql.Rs;
import club.jdiy.dev.dao.JDiyUiDao;
import club.jdiy.dev.entity.JDiyUi;
import club.jdiy.dev.meta.FormUiMeta;
import club.jdiy.dev.meta.UiMeta;
import club.jdiy.dev.view.FormHandler;
import club.jdiy.dev.view.FtlParser;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

public interface JDiyUiService extends JDiyService<JDiyUi, JDiyUiDao,String> {
    @Transactional
    Rs do_save(String do_save, Map<String, String> qo, FormUiMeta uiMeta, FormHandler handler, FtlParser parser) throws Exception;

    @Transactional
    void do_batchUpdate(Map<String, String> qo, FormUiMeta uiMeta, FormHandler handler, FtlParser parser) throws Exception;

    void do_ajax(String uiType, String uid, String btnId, String[] id, FtlParser parser);
    <T extends UiMeta> T getUiMeta(String id);
}
