package club.jdiy.dev.zlt.service;

import club.jdiy.admin.dao.DictItemDao;
import club.jdiy.admin.entity.DictItem;
import club.jdiy.core.base.JDiyService;

import java.util.List;

public interface JDevDictItemService extends JDiyService<DictItem, DictItemDao,Long> {
}
