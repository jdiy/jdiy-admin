package club.jdiy.dev.zlt.service.impl;

import club.jdiy.admin.util.IdUtil;
import club.jdiy.core.base.JDiyBaseService;
import club.jdiy.core.ex.JDiyException;
import club.jdiy.dev.dao.JDiyUiDao;
import club.jdiy.dev.entity.JDiyUi;
import club.jdiy.dev.meta.*;
import club.jdiy.dev.zlt.service.JDevPageService;
import club.jdiy.utils.BeanUtils;
import club.jdiy.utils.JsonUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

@Service
public class JDevPageServiceImpl extends JDiyBaseService<JDiyUi, JDiyUiDao, String>
        implements JDevPageService {

    @Override
    @Transactional
    public void updateName(JDiyUi vo) {
        JDiyUi po = dao.findById(vo.getId()).orElseThrow(() -> new JDiyException("目标界面不存在或被删除。"));
        if (dao.existsByNameAndIdNot(vo.getName().trim(), po.getId())) throw new JDiyException("界面名称与数据库中的重复,本次修改未生效。");
        po.setName(vo.getName());
        dao.save(po);
    }

    @Transactional
    @Override
    public JDiyUi clone(JDiyUi vo) {
        JDiyUi oo = dao.findById(vo.getId()).orElseThrow(() -> new JDiyException("目标界面不存在或被删除！"));
        if (dao.existsByNameAndIdNot(vo.getName(), oo.getId())) throw new JDiyException("界面名称与数据库中的重复。请修改后提交。");
        JDiyUi po = BeanUtils.clone(oo);
        po.setId(IdUtil.newId());
        po.setName(vo.getName());

        if ("table".equals(oo.getBindType())) {
            po.setMainTable(vo.getMainTable());
        } else {
            po.setMainEntity(vo.getMainEntity());
            po.setMainTable(context.getDao().getEntityMeta(vo.getMainEntity()).getTableName());
        }


        switch (po.getType()) {
            case form:
                FormUiMeta formMeta = dao.getUiMeta(vo.getId());
                formMeta.setId(po.getId());
                if (formMeta.getInputs() != null)
                    Arrays.stream(formMeta.getInputs()).forEach(it -> it.setId(IdUtil.newId()));
                po.setMetaData(JsonUtils.stringify(formMeta));
                break;
            case list:
                ListUiMeta listMeta = dao.getUiMeta(vo.getId());
                listMeta.setId(po.getId());
                if (listMeta.getQos() != null) Arrays.stream(listMeta.getQos()).forEach(it -> it.setId(IdUtil.newId()));
                if (listMeta.getTopButtons() != null)
                    Arrays.stream(listMeta.getTopButtons()).forEach(it -> it.setId(IdUtil.newId()));
                if (listMeta.getRowButtons() != null)
                    Arrays.stream(listMeta.getRowButtons()).forEach(it -> it.setId(IdUtil.newId()));
                if (listMeta.getColumns() != null)
                    Arrays.stream(listMeta.getColumns()).forEach(it -> it.setId(IdUtil.newId()));
                po.setMetaData(JsonUtils.stringify(listMeta));
                break;
            case tree:
                TreeUiMeta treeMeta = dao.getUiMeta(vo.getId());
                treeMeta.setId(po.getId());
                //if (treeMeta.getQos() != null) Arrays.stream(treeMeta.getQos()).forEach(it -> it.setId(IdUtil.newId()));
                if (treeMeta.getTopButtons() != null)
                    Arrays.stream(treeMeta.getTopButtons()).forEach(it -> it.setId(IdUtil.newId()));
                if (treeMeta.getRowButtons() != null)
                    Arrays.stream(treeMeta.getRowButtons()).forEach(it -> it.setId(IdUtil.newId()));
                if (treeMeta.getColumns() != null)
                    Arrays.stream(treeMeta.getColumns()).forEach(it -> it.setId(IdUtil.newId()));
                po.setMetaData(JsonUtils.stringify(treeMeta));
                break;
        }

        po = dao.save(po);
        return po;
    }
}
