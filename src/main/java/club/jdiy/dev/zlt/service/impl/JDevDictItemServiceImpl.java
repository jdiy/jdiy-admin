package club.jdiy.dev.zlt.service.impl;

import club.jdiy.admin.dao.DictDao;
import club.jdiy.admin.dao.DictItemDao;
import club.jdiy.admin.entity.Dict;
import club.jdiy.admin.entity.DictItem;
import club.jdiy.core.base.JDiyBaseService;
import club.jdiy.core.ex.JDiyException;
import club.jdiy.dev.zlt.service.JDevDictItemService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class JDevDictItemServiceImpl extends JDiyBaseService<DictItem, DictItemDao, Long>
        implements JDevDictItemService {
    @Override
    @Transactional
    public DictItem logicSave(DictItem vo) {
        Dict dict = dictDao.findById(vo.getDict().getId()).orElseThrow(() -> new JDiyException("获取字典信息失败．"));

        if (vo.getVal() == null) vo.setVal(vo.getTxt());
        if (!Boolean.TRUE.equals(dict.getAutovalues())) {
            DictItem di = dao.findFirstByDictAndVal(dict, vo.getVal());
            if (di != null && !di.getId().equals(vo.getId()))
                throw new JDiyException(dict.getValName() + "[" + vo.getVal() + "]与现有条目重复．");
        }
        DictItem po = dao.findById(vo.getId()).orElseGet(DictItem::new);
        po.setTxt(vo.getTxt());
        boolean isAdd = po.getVal() == null;
        po.setVal(vo.getVal());
        if (dict.getType() == 3) {//k/v/color
            po.setColor(vo.getColor());
        }
        po.setDict(vo.getDict());
        po.setSortIndex(vo.getSortIndex());
        po.setHidden(Boolean.TRUE.equals(vo.getHidden()));
        po.setEditable(!Boolean.FALSE.equals(vo.getEditable()));
        po.setRemovable(!Boolean.FALSE.equals(vo.getRemovable()));
        po = dao.save(po);
        //保证仅在添加时设置其值，修改时不应该再更新值已防对现有业务数据产生影响.
        if (isAdd && dict.getType() != 1 && Boolean.TRUE.equals(dict.getAutovalues())) po.setVal("" + po.getId());
        return dao.save(po);
    }

    @Resource
    DictDao dictDao;
}
