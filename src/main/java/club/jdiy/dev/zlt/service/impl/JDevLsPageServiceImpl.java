package club.jdiy.dev.zlt.service.impl;

import club.jdiy.core.AppContext;
import club.jdiy.core.base.JDiyBaseService;
import club.jdiy.core.sql.ColumnInfo;
import club.jdiy.core.sql.TableInfo;
import club.jdiy.dev.helper.OptsHelper;
import club.jdiy.dev.meta.*;
import club.jdiy.dev.types.BtnActTpl;
import club.jdiy.dev.types.*;
import club.jdiy.dev.zlt.service.JDevLsPageService;
import club.jdiy.dev.dao.*;
import club.jdiy.dev.entity.*;
import club.jdiy.admin.util.IdUtil;
import club.jdiy.core.ex.JDiyFormException;
import club.jdiy.utils.ArrayUtils;
import club.jdiy.utils.JsonUtils;
import club.jdiy.utils.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.regex.Pattern;

@Service
public class JDevLsPageServiceImpl extends JDiyBaseService<JDiyUi, JDiyUiDao, String>
        implements JDevLsPageService {
    private static final Pattern update_code_p = Pattern.compile("^(\\s*`*[a-zA-Z0-9_]+?`*\\s*=\\s*'*[^'=]+?'*\\s*)(\n(\\s*`*[a-zA-Z0-9_]+?`*\\s*=\\s*'*[^'=]+?'*\\s*))*$");

    @Override
    @Transactional
    public JDiyUi logicSave(JDiyUi vo) {
        JDiyUi po = dao.findById(vo.getId()).orElse(null);
        if (po == null) {
            po = new JDiyUi();
            po.setType(vo.getType());
            po.setId(IdUtil.newId());
            po.setBindType(vo.getBindType());
            if ("table".equals(vo.getBindType())) {
                po.setMainTable(vo.getMainTable());
            } else {
                po.setMainEntity(vo.getMainEntity());
                po.setMainTable(context.getDao().getEntityMeta(vo.getMainEntity()).getTableName());
            }

            ListUiMeta lpv = new ListUiMeta();
            lpv.setMge(true);

            JDiyUi inPage = dao.findFirstByTypeAndMainTable(UiTpl.form, po.getMainTable());

            ButtonMeta b1 = new ButtonMeta();
            b1.setId(IdUtil.newId());
            b1.setText("添加");
            b1.setIcon("layui-icon-add-circle");
            b1.setAct(BtnActTpl.add);
            b1.setTarget(BtnTargetTpl.dialog);
            b1.setWidth(640);
            b1.setHeight(480);
            if (inPage != null) {
                b1.setGoPageId(inPage.getId());
                b1.setGoPageType(inPage.getType());
            }
            lpv.setTopButtons(new ButtonMeta[]{b1});

            ButtonMeta b2 = new ButtonMeta();
            b2.setId(IdUtil.newId());
            b2.setText("修改");
            b2.setIcon("layui-icon-edit");
            b2.setDbl(true);
            b2.setWidth(640);
            b2.setHeight(480);
            b2.setAct(BtnActTpl.edit);
            b2.setTarget(BtnTargetTpl.dialog);
            if (inPage != null) {
                b2.setGoPageId(inPage.getId());
                b2.setGoPageType(inPage.getType());
            }

            ButtonMeta b3 = new ButtonMeta();
            b3.setId(IdUtil.newId());
            b3.setText("删除");
            b3.setIcon("layui-icon-delete");
            b3.setAct(BtnActTpl.del);
            b3.setTarget(BtnTargetTpl.ajaxTodo);
            b3.setBg("#ff5722");
            b3.setConfirm("您真的要删除此条记录吗？");

            lpv.setRowButtons(new ButtonMeta[]{b2, b3});

            TableInfo tableInfo = appContext.getDao().getTableInfo(po.getMainTable());
            Collection<ColumnInfo> columnInfos = tableInfo.getColumns().values();
            if (columnInfos.size() < 1) {
                ColumnMeta c1 = new ColumnMeta();
                c1.setId(IdUtil.newId());

                c1.setField(tableInfo.getPrimaryKey());
                c1.setLabel("ID");
                c1.setFormat(ColFormatTpl.text);

                ColumnMeta c2 = new ColumnMeta();
                c2.setId(IdUtil.newId());
                c2.setLabel("未配置列");
                c2.setFormat(ColFormatTpl.text);

                lpv.setColumns(new ColumnMeta[]{c1, c2});
            } else {
                int i = 0;
                List<ColumnMeta> cs = new ArrayList<>();
                for (ColumnInfo fv : columnInfos) {
                    ColumnMeta c1 = new ColumnMeta();
                    c1.setId(IdUtil.newId());
                    c1.setField(fv.getName());
                    c1.setLabel(fv.isPrimaryKey() ? "ID" : "列标题" + ++i);
                    c1.setFormat(ColFormatTpl.text);
                    cs.add(c1);
                    if (i > 3) break;
                }
                lpv.setColumns(cs.toArray(new ColumnMeta[0]));
            }

            QoMeta q1 = new QoMeta();
            q1.setId(IdUtil.newId());
            q1.setType(QoTpl.text);
            q1.setField("");
            q1.setLabel("关键字");
            q1.setOper(OperTpl.eq);

            QoMeta q2 = new QoMeta();
            q2.setId(IdUtil.newId());
            q2.setType(QoTpl.select);
            q2.setField("");
            q2.setLabel("状态");
            q2.setOper(OperTpl.eq);
            q2.setItems(":=全部状态=\n0:待审核\n1:已审核\n2:不通过");
            lpv.setQos(new QoMeta[]{q1, q2});

            po.setMetaData(JsonUtils.stringify(lpv));
        }
        po.setName(vo.getName());
        po = dao.save(po);

        return po;
    }

    @Override
    @Transactional
    public void saveDesign(String uid, ListUiMeta uiMeta) {
        JDiyUi ui = dao.findById(uid).orElseThrow(()
                -> new JDiyFormException("列表界面信息不存在或被删除(uiId=" + uid + ")", ""));
        if (StringUtils.isEmpty(uiMeta.getPageHandler())) uiMeta.setPageHandler(null);
        if (uiMeta.getColumns() == null || uiMeta.getColumns().length < 1)
            throw new JDiyFormException("列表中至少需要配置一个显示字段", "");
        if(StringUtils.hasText(uiMeta.getTableHeight())){
            if(!Pattern.compile("(full\\-)*\\d+").matcher(uiMeta.getTableHeight().trim()).matches())
                throw new JDiyFormException("[全局设置-表格高度]配置值格式不正确，请按要求设置,详情见该配置输入框右侧帮助.", "");
        }
        __saveQos(uiMeta);
        __saveCols(uiMeta, ui);
        __saveBtns(uiMeta.getTopButtons(), 0);
        if (uiMeta.isMge()) __saveBtns(uiMeta.getRowButtons(), 1);
        ui.setMetaData(JsonUtils.stringify(uiMeta));
        dao.save(ui);
        dao.updateMenuRealms(uid, ArrayUtils.join(dao.getRealms(uid), "\n"));
    }

    private void __saveQos(ListUiMeta uiMeta) {
        for (QoMeta vo : uiMeta.getQos()) {
            if (vo.getJoinType() == null) {
                if (StringUtils.isEmpty(vo.getField()))
                    throw new JDiyFormException("选中的控件未绑定查询字段！", "jdiylsobj_" + vo.getId());
            } else {
                switch (vo.getJoinType()) {
                    case manyToOne:
                        if (StringUtils.isEmpty(vo.getField()))
                            throw new JDiyFormException("选中的控件为“多对一”外键反查，但您还未设置本表外键！", "jdiylsobj_" + vo.getId());
                        if (StringUtils.isEmpty(vo.getJoinTable()))
                            throw new JDiyFormException("选中的控件为“多对一”外键反查，但您还未设置外联表！", "jdiylsobj_" + vo.getId());
                        if (StringUtils.isEmpty(vo.getJoinField()))
                            throw new JDiyFormException("选中的控件为“多对一”外键反查，但您还未设置查询字段！", "jdiylsobj_" + vo.getId());
                        break;
                    case oneToMany:
                        if (StringUtils.isEmpty(vo.getJoinRef()))
                            throw new JDiyFormException("选中的控件为“一对多子表查询”，但您还未设置外联表外键！", "jdiylsobj_" + vo.getId());
                        if (StringUtils.isEmpty(vo.getJoinTable()))
                            throw new JDiyFormException("选中的控件为“一对多子表查询”，但您还未设置外联表！", "jdiylsobj_" + vo.getId());
                        if (StringUtils.isEmpty(vo.getJoinField()))
                            throw new JDiyFormException("选中的控件为“一对多子表查询”，但您还未设置查询字段！", "jdiylsobj_" + vo.getId());
                        break;
                    case manyToMany:
                        if (StringUtils.isEmpty(vo.getJoinTable()))
                            throw new JDiyFormException("选中的控件为“多对多子表查询”，但您还未设置外联表！", "jdiylsobj_" + vo.getId());
                        if (StringUtils.isEmpty(vo.getJoinField()))
                            throw new JDiyFormException("选中的控件为“多对多子表查询”，但您还未设置查询字段！", "jdiylsobj_" + vo.getId());
                        if (StringUtils.isEmpty(vo.getManyTable()))
                            throw new JDiyFormException("选中的控件为“多对多子表查询”，但您还未设置中间表！", "jdiylsobj_" + vo.getId());
                        if (StringUtils.isEmpty(vo.getManyField0()))
                            throw new JDiyFormException("选中的控件为“多对多子表查询”，但您还未设置中间表正向关联字段！", "jdiylsobj_" + vo.getId());
                        if (StringUtils.isEmpty(vo.getManyField1()))
                            throw new JDiyFormException("选中的控件为“多对多子表查询”，但您还未设置中间表反向关联字段！", "jdiylsobj_" + vo.getId());
                        break;
                }
            }
            if (vo.getType() == QoTpl.treeSelect) {
                if (StringUtils.isEmpty(vo.getTreeId()))
                    throw new JDiyFormException("选中的控件为“树形下拉菜单”，但您还没有选择树形数据来源。请在页面右侧属性面板中完成配置。", "jdiylsobj_" + vo.getId());
            }

            if (vo.getType() == QoTpl.select)
                OptsHelper.designValidate(vo, "jdiylsobj_" + vo.getId());


        }
    }

    private void __saveCols(ListUiMeta uiMeta, JDiyUi ui) {
        Map<String, ColumnInfo> ci = context.getDao().getTableInfo(ui.getMainTable()).getColumns();
        //Set<String> uniqueCheck = new HashSet<>();
        for (ColumnMeta vo : uiMeta.getColumns()) {
            /*if (!StringUtils.isEmpty(vo.getField())) {
                if (uniqueCheck.contains(vo.getField()))
                    throw new JDiyFormException("表格列显示字段(" + vo.getField() + ")重复绑定了。请修改或移除此列 。", "jdiylsoth_" + vo.getId());
                uniqueCheck.add(vo.getField());
            }*/
            switch (vo.getFormat()) {
                case kv:
                    if (StringUtils.isEmpty(vo.getKv()))
                        throw new JDiyFormException("选中的列为“键值转键名”的显示方式，但[键值:键名]列表没有填写。填写格式详见配置框右侧帮助。", "jdiylsoth_" + vo.getId());
                    for (String s : vo.getKv().trim().split("\n")) {
                        if (StringUtils.isEmpty(s)) continue;
                        int n = s.split(":").length;
                        if (n < 2 || n > 3)
                            throw new JDiyFormException("选中的列[键值:键名]列表输入格式不正确。详见配置框右侧帮助。", "jdiylsoth_" + vo.getId());
                    }
                    break;
                case dict:
                    if (StringUtils.isEmpty(vo.getDictId()))
                        throw new JDiyFormException("选中的列为“数据字典”的显示方式，但您还没有选择字典．", "jdiylsoth_" + vo.getId());
                    break;
                case oneToMany:
                    if (StringUtils.isEmpty(vo.getJoinTable()))
                        throw new JDiyFormException("选中的列为外联表显示，但您还没有选择外联表。", "jdiylsoth_" + vo.getId());
                    if (StringUtils.isEmpty(vo.getJoinRef()))
                        throw new JDiyFormException("选中的列为(一对多)外联表显示，但您还没有选择外键。", "jdiylsoth_" + vo.getId());
                    if (StringUtils.isEmpty(vo.getJoinField()))
                        throw new JDiyFormException("选中的列为外联表显示，但您还没有选择要显示的外联表字段。", "jdiylsoth_" + vo.getId());
                    break;
                case manyToOne:
                    if (StringUtils.isEmpty(vo.getJoinTable()))
                        throw new JDiyFormException("选中的列为(一对多)外联表显示，但您还没有选择外联表。", "jdiylsoth_" + vo.getId());
                    if (StringUtils.isEmpty(vo.getJoinField()))
                        throw new JDiyFormException("选中的列为(一对多)外联表显示，但您还没有选择显示字段。", "jdiylsoth_" + vo.getId());
                    break;
                case manyToMany:
                    if (StringUtils.isEmpty(vo.getJoinTable()))
                        throw new JDiyFormException("选中的列为(多对多)外联表显示，但您还没有选择外联表。", "jdiylsoth_" + vo.getId());
                    if (StringUtils.isEmpty(vo.getJoinField()))
                        throw new JDiyFormException("选中的列为(多对多)外联表显示，但您还没有选择显示字段。", "jdiylsoth_" + vo.getId());
                    if (StringUtils.isEmpty(vo.getManyTable()))
                        throw new JDiyFormException("选中的列为(多对多)外联表显示，但您还没有选择中间表。", "jdiylsoth_" + vo.getId());
                    if (StringUtils.isEmpty(vo.getManyField0()))
                        throw new JDiyFormException("选中的列为(多对多)外联表显示，但您还没有选择中间表的正向关联字段。", "jdiylsoth_" + vo.getId());
                    if (StringUtils.isEmpty(vo.getManyField1()))
                        throw new JDiyFormException("选中的列为(多对多)外联表显示，但您还没有选择中间表的反向关联字段。", "jdiylsoth_" + vo.getId());
                    break;
                case tree:
                    if (StringUtils.isEmpty(vo.getTreeId()))
                        throw new JDiyFormException("选中的列为“外联树路径层级”的显示方式，但您还没有选择树形。", "jdiylsoth_" + vo.getId());
                case img:
                case file:
                    vo.setAlign("center");
            }
            vo.setAlign(StringUtils.isEmpty(vo.getAlign()) ? "center" : vo.getAlign());

            if (vo.isSum()) {
                ColumnInfo _ci = ci.get(vo.getField());
                if (_ci == null || !_ci.getType().contains("int(") && !_ci.getType().contains("decimal(") && !_ci.getType().contains("double")) {
                    vo.setSum(false);
                }
            }
        }
    }

    private void __saveBtns(ButtonMeta[] buttons, int pos) {
        for (ButtonMeta vo : buttons) {
            if (StringUtils.isEmpty(vo.getText()) && StringUtils.isEmpty(vo.getIcon())) {
                throw new JDiyFormException("选中的按钮“按钮文字”或者“显示图标”至少需要设置一个。", "jdiylsobj_" + vo.getId());
            }
            if (BtnActTpl.add == vo.getAct() || BtnActTpl.edit == vo.getAct() || BtnActTpl.view == vo.getAct() || BtnActTpl.page == vo.getAct() || BtnActTpl.link == vo.getAct() || BtnActTpl.post == vo.getAct()) {
                if (BtnActTpl.link != vo.getAct() && BtnActTpl.post != vo.getAct()) {
                    if (StringUtils.isEmpty(vo.getGoPageId()))
                        throw new JDiyFormException("选中的按钮未设置要打开的目标界面！", "jdiylsobj_" + vo.getId());
                    else {
                        try {
                            UiMeta uim = dao.getUiMeta(vo.getGoPageId());
                            vo.setGoPageType(uim.getType());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            throw new JDiyFormException("获取目标界面类型失败！", "jdiylsobj_" + vo.getId());
                        }
                    }
                }
                if ((BtnActTpl.link == vo.getAct() || BtnActTpl.post == vo.getAct()) && StringUtils.isEmpty(vo.getOutLink()))
                    throw new JDiyFormException("目标按钮未设定外链地址。", "jdiylsobj_" + vo.getId());
                if (BtnActTpl.post == vo.getAct())
                    vo.setTarget(BtnTargetTpl._blank);//选中项POST到新界面，只能是打开新网页(应用场景如：选中项后打开新界面执行导出 zwb220325)
                if (BtnTargetTpl.dialog == vo.getTarget()) {
                    if (vo.getWidth() == null || vo.getWidth() < 300)
                        throw new JDiyFormException("目标按钮弹框宽度设置不正确(建议设置为300~960之间)。", "jdiylsobj_" + vo.getId());
                    if (vo.getHeight() == null || vo.getHeight() < 150)
                        throw new JDiyFormException("目标按钮弹框高度设置不正确(建议设置为180~720之间)。", "jdiylsobj_" + vo.getId());
                }

            } else {
                vo.setTarget(BtnTargetTpl.ajaxTodo);
                if (BtnActTpl.update == vo.getAct()) {
                    if (StringUtils.isEmpty(vo.getUpdateCode()))
                        throw new JDiyFormException("目标按钮操作类型为“更新字段值”，但未填写要更新的字段内容。请在右侧属性栏中修改。", "jdiylsobj_" + vo.getId());
                    if (!update_code_p.matcher(vo.getUpdateCode()).matches())
                        throw new JDiyFormException("目标按钮更新的字段内容格式输入不正确，需要为：“字段名=值”的格式，一行填一个，中间不要出现多个空行或其它无关字符。", "jdiylsobj_" + vo.getId());
                    String ss = vo.getUpdateCode().trim().replaceAll("\\s*,*\\s*([\\n\\r])+\\s*,*\\s*", "\n");
                    vo.setUpdateCode(ss);
                } else if (BtnActTpl.ajax == vo.getAct()) {
                    if (StringUtils.isEmpty(vo.getAjaxUrl()))
                        throw new JDiyFormException("选中的按钮未配置要请求的自定义ajax页面地址。", "jdiylsobj_" + vo.getId());
                }
            }
            if (StringUtils.hasText(vo.getGrantCode()) && !codeP.matcher(vo.getGrantCode()).matches()) {
                throw new JDiyFormException("按钮权限代码只能为英文字母(数字或下划线组合），且不要以数字开头(符合变量命名规范)", "jdiylsobj_" + vo.getId());
            }
            if (StringUtils.hasText(vo.getConditionShow())) {
                if (vo.getConditionShow().contains("${") || vo.getConditionShow().contains("<>"))
                    throw new JDiyFormException("按钮显示条件配置可能有误，请检查。", "jdiylsobj_" + vo.getId());
            }
        }
    }

    @Resource
    private AppContext appContext;
    static Pattern codeP = Pattern.compile("^[a-zA-Z][a-zA-Z0-9_]+$");
}
