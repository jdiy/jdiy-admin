package club.jdiy.dev.zlt.service;

import club.jdiy.admin.dao.MenuDao;
import club.jdiy.admin.entity.Menu;
import club.jdiy.core.base.JDiyService;

import java.util.List;

public interface JDevMenuService extends JDiyService<Menu, MenuDao,String> {
    List<Menu> findRootList();

    void doSave(String id, Menu vo);
}
