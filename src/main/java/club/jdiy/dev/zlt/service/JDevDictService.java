package club.jdiy.dev.zlt.service;

import club.jdiy.admin.dao.DictDao;
import club.jdiy.admin.entity.Dict;
import club.jdiy.core.base.JDiyService;

public interface JDevDictService extends JDiyService<Dict, DictDao,String> {
}
