package club.jdiy.dev.zlt.service.impl;

import club.jdiy.admin.dao.DictDao;
import club.jdiy.admin.entity.Dict;
import club.jdiy.core.base.JDiyBaseService;
import club.jdiy.dev.zlt.service.JDevDictService;
import club.jdiy.utils.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class JDevDictServiceImpl extends JDiyBaseService<Dict, DictDao, String>
        implements JDevDictService {

    @Override
    @Transactional
    public Dict logicSave(Dict vo) {
        Dict po = dao.findById(vo.getId()).orElse(new Dict(vo.getId()));
        po.setName(vo.getName());
        po.setType(vo.getType());
        po.setSortIndex(vo.getSortIndex());
        po.setKeyName(vo.getKeyName());
        po.setValName(StringUtils.isEmpty(vo.getValName()) ? "值" : vo.getValName());
        po.setRemark(vo.getRemark());
        po.setHidden(Boolean.TRUE.equals(vo.getHidden()));
        po.setAddable(!Boolean.FALSE.equals(vo.getAddable()));
        po.setEditable(!Boolean.FALSE.equals(vo.getEditable()));
        po.setRemovable(!Boolean.FALSE.equals(vo.getRemovable()));
        po.setAutovalues(Boolean.TRUE.equals(vo.getAutovalues()));
        return dao.save(po);
    }

}
