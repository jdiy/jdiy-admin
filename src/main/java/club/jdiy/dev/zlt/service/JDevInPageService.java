package club.jdiy.dev.zlt.service;


import club.jdiy.core.base.JDiyService;
import club.jdiy.dev.dao.JDiyUiDao;
import club.jdiy.dev.entity.JDiyUi;
import club.jdiy.dev.meta.FormUiMeta;

public interface JDevInPageService extends JDiyService<JDiyUi, JDiyUiDao,String> {
    void saveDesign(String uid, FormUiMeta formMeta);
}
