package club.jdiy.dev.zlt.service;


import club.jdiy.core.base.JDiyService;
import club.jdiy.dev.dao.JDiyUiDao;
import club.jdiy.dev.entity.JDiyUi;

public interface JDevPageService extends JDiyService<JDiyUi, JDiyUiDao,String> {

    void updateName(JDiyUi vo);

    JDiyUi clone(JDiyUi vo);
}
