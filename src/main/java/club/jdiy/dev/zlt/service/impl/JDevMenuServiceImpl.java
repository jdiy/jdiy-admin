package club.jdiy.dev.zlt.service.impl;

import club.jdiy.admin.dao.MenuDao;
import club.jdiy.admin.entity.Menu;
import club.jdiy.core.base.JDiyBaseService;
import club.jdiy.core.ex.JDiyException;
import club.jdiy.admin.dao.RoleDao;
import club.jdiy.admin.entity.Role;
import club.jdiy.dev.zlt.service.JDevMenuService;
import club.jdiy.utils.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class JDevMenuServiceImpl extends JDiyBaseService<Menu, MenuDao, String>
        implements JDevMenuService {

    @Override
    public List<Menu> findRootList() {
        return dao.findRootList();
    }

    @Override
    @Transactional
    public void doSave(String id, Menu vo) {
        Menu po;
        if ("0".equals(id)) {
            dao.findById(vo.getId()).ifPresent((exists) -> {
                throw new JDiyException("菜单ID在库中已被［" + exists.getName() + "］使用，请更改后提交．");
            });
            po = new Menu();
            po.setId(vo.getId());
            __sets(po, vo);
            dao.save(po);
        } else {
            if (!id.equals(vo.getId())) {
                dao.findById(vo.getId()).ifPresent(exists -> {
                    throw new JDiyException("菜单ID在库中已被［" + exists.getName() + "］使用，请更改后提交．");
                });

                po = new Menu();
                po.setId(vo.getId());
                __sets(po, vo);
                po = dao.save(po);

                Menu finalPo = po;
                dao.findById(id).ifPresent(old -> {
                    //更新children关联
                    if (old.getChildren() != null) {
                        for (Menu c : old.getChildren()) {
                            c.setFather(finalPo);
                            dao.save(c);
                        }
                    }

                    //更新角色role配置
                    String newGrantAuth;
                    List<Role> roleList = roleDao.findAll();
                    if (roleList != null) {
                        for (Role role : roleList) {
                            if (role.getGrantAuth() != null) {
                                newGrantAuth = role.getGrantAuth().replaceAll("'" + id + "'", "'" + vo.getId() + "'");
                                if (!newGrantAuth.equals(role.getGrantAuth())) {
                                    role.setGrantAuth(newGrantAuth);
                                    roleDao.save(role);
                                }
                            }
                        }
                    }
                    //delete old
                    dao.delete(old);
                });
            } else {
                dao.findById(id).ifPresent(t -> {
                    __sets(t, vo);
                    dao.save(t);
                });
            }
        }
    }

    private void __setu(Menu po, Menu vo, String errStr) {
        if (StringUtils.isBlank(vo.getUid())) {
            throw new JDiyException(errStr);
        }
        po.setUid(vo.getUid());
    }

    private void __sets(Menu po, Menu vo) {
        if (!Boolean.TRUE.equals(po.isSys())) {//系统菜单部分字段不允许修改
            if ("list".equals(vo.getType())) {
                __setu(po, vo, "请选择要打开的列表界面");
            } else if ("tree".equals(vo.getType())) {
                __setu(po, vo, "请选择要打开的树形界面");
            } else if ("form".equals(vo.getType())) {
                __setu(po, vo, "请选择要打开的表单界面");
            } else if ("view".equals(vo.getType())) {
                __setu(po, vo, "请选择要打开的详情界面");
            } else if ("url".equals(vo.getType())) {
                if (StringUtils.isBlank(vo.getUrl())) {
                    throw new JDiyException("请输入要打开的自定义页面地址.");
                }
                po.setUrl(vo.getUrl());
                po.setUid(null);
            } else {
                po.setUid(null);
            }
            po.setType(vo.getType());
            po.setUrl(vo.getUrl());
            po.setPageParam(vo.getPageParam());
        }
        po.setFather("0".equals(vo.getFather().getId()) ? null : vo.getFather());
        po.setName(vo.getName());
        po.setSortIndex(vo.getSortIndex());
        po.setIcon(vo.getIcon());
        po.setState(vo.getState());
        po.setRealms(vo.getRealms());
    }

    @Resource
    private RoleDao roleDao;
}
