package club.jdiy.dev.zlt.controller;

import club.jdiy.admin.interceptor.GuestDisabled;
import club.jdiy.admin.service.DictService;
import club.jdiy.core.base.domain.Ret;
import club.jdiy.core.properties.JDiyProperties;
import club.jdiy.dev.controller.JDiyController;
import club.jdiy.dev.entity.JDiyUi;
import club.jdiy.dev.meta.ViewUiMeta;
import club.jdiy.dev.types.*;
import club.jdiy.dev.view.DefaultViewHandler;
import club.jdiy.dev.view.ViewHandler;
import club.jdiy.dev.zlt.service.JDevPageService;
import club.jdiy.dev.zlt.service.JDevViUiService;
import club.jdiy.utils.JsonUtils;
import club.jdiy.utils.reflex.PackageScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("mgmt/JDiyAdmin")
public class JDevViewCtrl extends JDevCtrl<JDiyUi, JDevViUiService> implements JDiyController {

    @GuestDisabled
    @RequestMapping("view.save")
    @ResponseBody
    public Ret<?> save(JDiyUi vo) {
        try {
            JDiyUi p = service.logicSave(vo);
            Map<String, String> map = new HashMap<>();
            map.put("url", "/mgmt/JDiyAdmin/view.design." + p.getId());
            map.put("tit", p.getName());
            return Ret.success(map);
        } catch (Exception ex) {
            return Ret.error(ex);
        }
    }

    @RequestMapping("view.design.{uid}")
    public String design(@PathVariable String uid, ModelMap map) {
        JDiyUi vo = service.findById(uid).orElse(null);
        if (vo != null) {
            map.put("primaryKey", context.getDao().getTableInfo(vo.getMainTable()).getPrimaryKey());
            map.put("vo", vo);
            map.put("tableList", listTables());
            map.put("entityList", context.getDao().getEntityMeta());
            map.put("typeTplList", OutTpl.values());
            map.put("joinTypeTplList", JoinTypeTpl.values());

            map.put("treePathTplList", TreePathTpl.values());
            map.put("dictList", dictService.findAll());
            map.put("pageList", jDiyPageService.findAll());
            map.put("pageHandlerList", PackageScan.getClasses(jDiyProperties.getHandlerScan(),
                    clazz -> clazz != ViewHandler.class && clazz != DefaultViewHandler.class && ViewHandler.class.isAssignableFrom(clazz)));
        }
        return "view.design";
    }

    @GuestDisabled
    @RequestMapping("view.saveDesign.{uid}")
    @ResponseBody
    public Ret<?> saveDesign(@PathVariable String uid, String s) {
        try {
            ViewUiMeta uiMeta = JsonUtils.parse(s, ViewUiMeta.class);
            service.saveDesign(uid, uiMeta);
            return Ret.success();
        } catch (Exception le) {
            return Ret.error(le);
        }
    }

    @Resource
    private JDiyProperties jDiyProperties;
    @Resource
    private DictService dictService;
    @Resource
    private JDevPageService jDiyPageService;
}
