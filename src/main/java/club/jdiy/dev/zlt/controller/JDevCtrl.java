package club.jdiy.dev.zlt.controller;

import club.jdiy.core.AdminContext;
import club.jdiy.core.base.JDiyCtrl;
import club.jdiy.core.base.JDiyService;
import club.jdiy.core.base.domain.DBEntity;
import club.jdiy.core.sql.CommentInfo;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
public abstract class JDevCtrl<T extends DBEntity, TService extends JDiyService> extends JDiyCtrl<T, TService> {
    private static final List<String> exclude_tables = Arrays.asList("jdiy_conf", "jdiy_dict", "jdiy_dict_item", "jdiy_guid", "jdiy_menu", "jdiy_store", "jdiy_ui","jdiy_job");

    protected List<CommentInfo> listTables() {
        return context.getDao().getTables().stream().filter(t ->
                !t.getName().startsWith("QRTZ_")
                        && !exclude_tables.contains(t.getName().toLowerCase())
        ).collect(Collectors.toList());
    }

    @Resource
    protected AdminContext context;
}
