package club.jdiy.dev.zlt.controller;

import club.jdiy.admin.entity.Dict;
import club.jdiy.admin.entity.DictItem;
import club.jdiy.admin.interceptor.GuestDisabled;
import club.jdiy.core.base.JDiyCtrl;
import club.jdiy.core.base.domain.Ret;
import club.jdiy.core.ex.JDiyException;
import club.jdiy.dev.controller.JDiyController;
import club.jdiy.dev.zlt.service.JDevDictItemService;
import club.jdiy.dev.zlt.service.JDevDictService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("mgmt/JDiyAdmin")
public class JDevDictCtrl extends JDiyCtrl<Dict, JDevDictService> implements JDiyController {
    @RequestMapping("dict.form")
    public String form(String id, ModelMap map, HttpServletRequest request) throws Exception {
        Dict vo = service.findById(id).orElse(null);
        if (vo == null) {
            vo = new Dict("0");
            vo.setAddable(true);
            vo.setEditable(true);
            vo.setRemovable(true);
            vo.setHidden(false);
        }
        map.put("vo", vo);
        return "dict.in";
    }

    @GuestDisabled
    @RequestMapping("dict.save")
    @ResponseBody
    public Ret<?> save(Dict vo) {
        try {
            service.logicSave(vo);
            return Ret.success();
        } catch (Exception ex) {
            return Ret.error(ex);
        }
    }

    @GuestDisabled
    @RequestMapping("dict.delete.{dictId}")
    @ResponseBody
    public Ret<?> delete(@PathVariable String dictId) {
        try {
            service.delete(new Dict(dictId));
            return Ret.success();
        } catch (Exception ex) {
            return Ret.error(ex);
        }
    }

    @RequestMapping("dict.list")
    public String ls(ModelMap map,
                     @RequestParam(defaultValue = "1") Integer page,
                     @RequestParam(defaultValue = "15") Integer pageSize,
                     Dict qo
    ) {
        map.put("qo", qo);
        map.put("pager", service.findPager(pageSize, page, qo));
        return "dict.ls";
    }

    @RequestMapping("dict.listItem.{dictId}")
    public String item_ls(@PathVariable String dictId,
                          @RequestParam(defaultValue = "15") Integer pageSize,
                          @RequestParam(defaultValue = "1") Integer page,
                          DictItem qo, ModelMap map) {
        map.put("dict", service.findById(dictId).orElse(null));
        qo.setDict(new Dict(dictId));
        map.put("pager", jDevDictItemService.findPager(pageSize, page, qo));
        return "dict.item.ls";
    }


    @RequestMapping("dict.formItem.{dictId}")
    public String formItem(Long id, @PathVariable String dictId, ModelMap map) {
        map.put("dict", service.findById(dictId).orElse(null));
        DictItem vo = jDevDictItemService.findById(id).orElseGet(() -> {
            DictItem vo1 = new DictItem();
            vo1.setId(0L);
            vo1.setSortIndex(100);
            return vo1;
        });
        map.put("vo", vo);
        return "dict.item.in";
    }

    @GuestDisabled
    @RequestMapping("dict.saveItem.{dictId}")
    @ResponseBody
    public Ret<?> saveItem(@PathVariable String dictId, DictItem vo) {
        try {
            vo.setDict(new Dict(dictId));
            jDevDictItemService.logicSave(vo);
            return Ret.success();
        } catch (JDiyException le) {
            return Ret.error(le);
        }
    }

    @GuestDisabled
    @RequestMapping("dict.deleteItem.{itemId}")
    @ResponseBody
    public Ret<?> deleteItem(@PathVariable Long itemId) {
        try {
            jDevDictItemService.delete(new DictItem(itemId));
            return Ret.success();
        } catch (Exception le) {
            return Ret.fail("此字典条目已被系统业务使用．不能删除!");
        }
    }

    @Resource
    private JDevDictItemService jDevDictItemService;

}
