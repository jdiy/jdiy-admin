package club.jdiy.dev.zlt.controller;

import club.jdiy.admin.interceptor.GuestDisabled;
import club.jdiy.admin.service.DictService;
import club.jdiy.core.base.JDiyCtrl;
import club.jdiy.dev.controller.JDiyController;
import club.jdiy.dev.entity.JDiyUi;
import club.jdiy.dev.types.*;
import club.jdiy.dev.view.DefaultFormHandler;
import club.jdiy.dev.zlt.service.JDevInPageService;
import club.jdiy.dev.view.FormHandler;
import club.jdiy.dev.meta.FormUiMeta;
import club.jdiy.core.properties.JDiyProperties;
import club.jdiy.dev.zlt.service.JDevPageService;
import club.jdiy.utils.reflex.PackageScan;
import club.jdiy.utils.JsonUtils;
import club.jdiy.core.base.domain.Ret;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("mgmt/JDiyAdmin")
public class JDevFormCtrl extends JDevCtrl<JDiyUi, JDevInPageService> implements JDiyController {

    @GuestDisabled
    @RequestMapping("form.save")
    @ResponseBody
    public Ret<?> save(JDiyUi vo) {
        try {
            JDiyUi p = service.logicSave(vo);
            Map<String, String> map = new HashMap<>();
            map.put("url", "/mgmt/JDiyAdmin/form.design." + p.getId());
            map.put("tit", p.getName());
            return Ret.success(map);
        } catch (Exception ex) {
            return Ret.error(ex);
        }
    }

    @RequestMapping("form.design.{uid}")
    public String design(@PathVariable String uid, ModelMap map) {
        JDiyUi vo = service.findById(uid).orElse(null);
        if (vo != null) {
            map.put("vo", vo);
            map.put("tableList", listTables());
            map.put("entityList", context.getDao().getEntityMeta());
            map.put("layoutList", FormLayoutTpl.values());
            map.put("typeTplList", InputTpl.values());
            map.put("fileTypeList", FileTypeTpl.values());
            map.put("formatTplList", FormatTpl.values());
            map.put("pkTplList", PkTpl.values());
            map.put("manyTplList", ManyTpl.values());
            map.put("roomTplList", RoomTpl.values());
            map.put("treePathTplList", TreePathTpl.values());
            map.put("optTplList", OptTpl.values());
            map.put("dictList", dictService.findAll());
            map.put("pageList", jDiyPageService.findAll());
            map.put("pageHandlerList", PackageScan.getClasses(jDiyProperties.getHandlerScan(),
                    clazz -> clazz != FormHandler.class && clazz != DefaultFormHandler.class && FormHandler.class.isAssignableFrom(clazz)));
        }
        return "form.design";
    }

    @GuestDisabled
    @RequestMapping("form.saveDesign.{uid}")
    @ResponseBody
    public Ret<?> saveDesign(@PathVariable String uid, String s) {
        try {
            FormUiMeta uiMeta = JsonUtils.parse(s, FormUiMeta.class);
            service.saveDesign(uid, uiMeta);
            return Ret.success();
        } catch (Exception le) {
            return Ret.error(le);
        }
    }

    @Resource
    private JDiyProperties jDiyProperties;
    @Resource
    private DictService dictService;
    @Resource
    private JDevPageService jDiyPageService;
}
