package club.jdiy.dev.zlt.controller;

import club.jdiy.admin.interceptor.GuestDisabled;
import club.jdiy.core.base.JDiyCtrl;
import club.jdiy.core.base.domain.Ret;
import club.jdiy.core.job.entity.JDiyJob;
import club.jdiy.dev.controller.JDiyController;
import club.jdiy.core.job.service.JDiyJobService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("mgmt/JDiyAdmin")
public class JDevJobCtrl extends JDiyCtrl<JDiyJob, JDiyJobService> implements JDiyController {
    @RequestMapping("job.form")
    public String form(Long id, ModelMap map, HttpServletRequest request) throws Exception {
        map.put("vo", service.findById(id).orElseGet(() -> new JDiyJob(0L)));
        return "job.in";
    }

    @GuestDisabled
    @RequestMapping("job.save")
    @ResponseBody
    public Ret<?> save(@Valid JDiyJob qo) {
        try {
            service.save(qo);
            return Ret.success();
        } catch (Exception ex) {
            return Ret.error(ex);
        }
    }

    @GuestDisabled
    @RequestMapping("job.remove")
    @ResponseBody
    public Ret<?> remove(Long id) {
        try {
            service.deleteById(id);
            return Ret.success();
        } catch (Exception ex) {
            return Ret.error(ex);
        }
    }

    @GuestDisabled
    @RequestMapping("job.run")
    @ResponseBody
    public Ret<?> run(Long id) {
        try {
            service.runById(id);
            return Ret.success();
        } catch (Exception ex) {
            return Ret.error(ex);
        }
    }
}
