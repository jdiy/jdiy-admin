package club.jdiy.dev.zlt.controller;

import club.jdiy.admin.interceptor.GuestDisabled;
import club.jdiy.admin.service.DictService;
import club.jdiy.dev.controller.JDiyController;
import club.jdiy.dev.types.*;
import club.jdiy.dev.view.DefaultListHandler;
import club.jdiy.dev.zlt.service.JDevPageService;
import club.jdiy.dev.entity.JDiyUi;
import club.jdiy.dev.zlt.service.JDevLsPageService;
import club.jdiy.dev.view.ListHandler;
import club.jdiy.dev.meta.ListUiMeta;
import club.jdiy.core.properties.JDiyProperties;
import club.jdiy.core.ex.JDiyException;
import club.jdiy.utils.reflex.PackageScan;
import club.jdiy.utils.JsonUtils;
import club.jdiy.core.base.domain.Ret;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("mgmt/JDiyAdmin")
public class JDevListCtrl extends JDevCtrl<JDiyUi, JDevLsPageService> implements JDiyController {

    @GuestDisabled
    @RequestMapping("list.save")
    @ResponseBody
    public Ret<?> save(JDiyUi vo) {
        try {
            JDiyUi p = service.logicSave(vo);
            Map<String, String> map = new HashMap<>();
            map.put("url", "/mgmt/JDiyAdmin/list.design." + p.getId());
            map.put("tit", p.getName());
            return Ret.success(map);
        } catch (Exception ex) {
            return Ret.error(ex);
        }
    }


    @RequestMapping("list.design.{uid}")
    public String design(@PathVariable String uid, ModelMap map) throws Exception {

        JDiyUi vo = service.findById(uid).orElseThrow(() -> new JDiyException("目标界面不存在或被删除．"));
        map.put("vo", vo);
        map.put("tableList", listTables());
        map.put("entityList", context.getDao().getEntityMeta());
        map.put("pageList", jDiyPageService.findAll());
        map.put("dictList", dictService.findAll());
        map.put("qoTplList", QoTpl.values());
        map.put("treePathTplList", TreePathTpl.values());
        map.put("colFormatTplList", ColFormatTpl.values());
        map.put("operTplList", OperTpl.values());
        map.put("optTplList", OptTpl.values());
        map.put("QoJoinTplList", QoJoinTpl.values());
        map.put("pageHandlerList", PackageScan.getClasses(jDiyProperties.getHandlerScan(),
                clazz -> clazz != ListHandler.class && clazz != DefaultListHandler.class && ListHandler.class.isAssignableFrom(clazz)));
        return "list.design";
    }

    @GuestDisabled
    @RequestMapping("list.saveDesign.{uid}")
    @ResponseBody
    public Ret<?> saveDesign(@PathVariable String uid, String s) {
        try {
            service.saveDesign(uid, JsonUtils.parse(s, ListUiMeta.class));
            return Ret.success();
        } catch (Exception ex) {
            return Ret.error(ex);
        }
    }

    @Resource
    private JDevPageService jDiyPageService;
    @Resource
    private JDiyProperties jDiyProperties;
    @Resource
    private DictService dictService;
}
