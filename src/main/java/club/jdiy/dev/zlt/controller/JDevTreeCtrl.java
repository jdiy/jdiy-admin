package club.jdiy.dev.zlt.controller;

import club.jdiy.admin.interceptor.GuestDisabled;
import club.jdiy.admin.service.DictService;
import club.jdiy.core.base.JDiyCtrl;
import club.jdiy.core.base.domain.Ret;
import club.jdiy.core.properties.JDiyProperties;
import club.jdiy.core.ex.JDiyException;
import club.jdiy.dev.controller.JDiyController;
import club.jdiy.dev.entity.JDiyUi;
import club.jdiy.dev.types.ColFormatTpl;
import club.jdiy.dev.types.RootPidTpl;
import club.jdiy.dev.types.TreePathTpl;
import club.jdiy.dev.meta.TreeUiMeta;
import club.jdiy.dev.view.DefaultTreeHandler;
import club.jdiy.dev.view.TreeHandler;
import club.jdiy.dev.zlt.service.JDevTrPageService;
import club.jdiy.dev.zlt.service.JDevPageService;
import club.jdiy.utils.JsonUtils;
import club.jdiy.utils.reflex.PackageScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("mgmt/JDiyAdmin")
public class JDevTreeCtrl extends JDevCtrl<JDiyUi, JDevTrPageService> implements JDiyController {

    @GuestDisabled
    @RequestMapping("tree.save")
    @ResponseBody
    public Ret<?> save(JDiyUi vo) {
        try {
            JDiyUi p = service.logicSave(vo);
            Map<String, String> map = new HashMap<>();
            map.put("url", "/mgmt/JDiyAdmin/tree.design." + p.getId());
            map.put("tit", p.getName());
            return Ret.success(map);
        } catch (Exception ex) {
            return Ret.error(ex);
        }
    }


    @RequestMapping("tree.design.{uid}")
    public String design(@PathVariable String uid, ModelMap map) throws Exception {

        JDiyUi vo = service.findById(uid).orElseThrow(() -> new JDiyException("目标界面不存在或被删除．"));
        map.put("vo", vo);
        map.put("tableList", listTables());
        map.put("entityList", context.getDao().getEntityMeta());
        map.put("pageList", jDiyPageService.findAll());
        map.put("dictList", dictService.findAll());
        map.put("treePathTplList", TreePathTpl.values());
        map.put("colFormatTplList", ColFormatTpl.values());
        map.put("rootPidTplList", RootPidTpl.values());
        map.put("pageHandlerList", PackageScan.getClasses(jDiyProperties.getHandlerScan(),
                clazz -> clazz != TreeHandler.class && clazz != DefaultTreeHandler.class && TreeHandler.class.isAssignableFrom(clazz)));
        return "tree.design";
    }

    @GuestDisabled
    @RequestMapping("tree.saveDesign.{uid}")
    @ResponseBody
    public Ret<?> saveDesign(@PathVariable String uid, String s) {
        try {
            service.saveDesign(uid, JsonUtils.parse(s, TreeUiMeta.class));
            return Ret.success();
        } catch (Exception ex) {
            return Ret.error(ex);
        }
    }

    @Resource
    private JDevPageService jDiyPageService;
    @Resource
    private JDiyProperties jDiyProperties;
    @Resource
    private DictService dictService;
}
