package club.jdiy.dev.zlt.utils;

import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

import java.math.BigInteger;

public class POIWordUtil {

    /**
     * 换行设置
     *
     * @param times 换几行
     */
    public static void nextLine(XWPFDocument doc, int times) {
        for (int i = 0; i < times; i++) {
            XWPFParagraph nextLine = doc.createParagraph();
            nextLine.createRun().setText("\r");
        }
    }

    /**
     * 设置标题
     */
    public static void setTitle(XWPFDocument doc, String text, int fontSize, ParagraphAlignment align) {
        XWPFParagraph titleParagraph = doc.createParagraph();
        titleParagraph.setAlignment(align);
        XWPFRun titleRun = titleParagraph.createRun();
        titleRun.setText(text);
        titleRun.setFontSize(fontSize);
    }

    //添加自定义标题样式
    public static void addCustomHeadingStyle(XWPFStyles styles, String strStyleId, int headingLevel, int pointSize) {

        CTStyle ctStyle = CTStyle.Factory.newInstance();
        ctStyle.setStyleId(strStyleId);


        CTString styleName = CTString.Factory.newInstance();
        styleName.setVal(strStyleId);
        ctStyle.setName(styleName);

        CTDecimalNumber indentNumber = CTDecimalNumber.Factory.newInstance();
        indentNumber.setVal(BigInteger.valueOf(headingLevel));

        // lower number > style is more prominent in the formats bar
        ctStyle.setUiPriority(indentNumber);

        CTOnOff onoffnull = CTOnOff.Factory.newInstance();
        ctStyle.setUnhideWhenUsed(onoffnull);

        // style shows up in the formats bar
        ctStyle.setQFormat(onoffnull);

        // style defines a heading of the given level
        CTPPr ppr = CTPPr.Factory.newInstance();
        ppr.setOutlineLvl(indentNumber);
        //ctStyle.setPPr(ppr);

        XWPFStyle style = new XWPFStyle(ctStyle);

        CTHpsMeasure size = CTHpsMeasure.Factory.newInstance();
        size.setVal(new BigInteger(String.valueOf(pointSize)));
        CTHpsMeasure size2 = CTHpsMeasure.Factory.newInstance();
        size2.setVal(new BigInteger("24"));

        CTFonts fonts = CTFonts.Factory.newInstance();
        fonts.setAscii("宋体");

        CTRPr rpr = CTRPr.Factory.newInstance();
        rpr.setRFontsArray(new CTFonts[]{fonts});
        rpr.setSzArray(new CTHpsMeasure[]{size});
        rpr.setSzCsArray(new CTHpsMeasure[]{size2});

        //定义的样式颜色
        CTColor color = CTColor.Factory.newInstance();
        color.setVal(hexToBytes("4288BC"));
        rpr.setColorArray(new CTColor[]{color});
        style.getCTStyle().setRPr(rpr);
        // is a null op if already defined
        style.setType(STStyleType.PARAGRAPH);
        styles.addStyle(style);
    }



    public static void setTableCellHeader(XWPFTableRow row, int CellPos, String text, String width) {
        XWPFTableCell cell = row.getCell(CellPos);
        cell.setColor("01AAED");
        cell.setText(text);
        POIWordUtil.setTableCellTextCenter(cell);
        if (!"0".equals(width)) {
            POIWordUtil.setTableCellWidth(cell, width);
        }
    }

    public static void setTableCell(XWPFTableRow row, int CellPos, String text, String width) {
        XWPFTableCell cell = row.getCell(CellPos);
        cell.setText(text);
        POIWordUtil.setTableCellTextCenter(cell);
        if (!"0".equals(width)) {
            POIWordUtil.setTableCellWidth(cell, width);
        }
    }

    /**
     * 新开一个段落，插入文本
     */
    public static void newParagraphText(XWPFDocument doc, String text) {
        XWPFParagraph xwpfParagraph = doc.createParagraph();
        xwpfParagraph.createRun().setText(text);
    }


    /**
     * 将 该paragraph 设置成Word中的一级 二级标签
     * 这里设置完成后 document.createTOC  就能够显示 目录结构
     */
    public static void createLevel(XWPFDocument document, String style,
                                   ParagraphAlignment para_align, String color, Integer font_size,
                                   String content, boolean bold) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(para_align);
        paragraph.setStyle(style);
        XWPFRun r = paragraph.createRun();
        r.setColor(color);
        r.setFontSize(font_size);
        r.setText(content);
        r.setBold(bold);
    }

    /**
     * 新开一个段落，插入文本指定对齐方式．
     */
    public static void newParagraphText(XWPFDocument doc, String text, ParagraphAlignment align) {
        XWPFParagraph xwpfParagraph = doc.createParagraph();
        xwpfParagraph.setAlignment(align);
        xwpfParagraph.createRun().setText(text);
    }

    /**
     * 新开一个段落，插入文本(zlt 改 有样式)
     */
    public static void newParagraphText(XWPFDocument doc, String text, String style) {
        XWPFParagraph xwpfParagraph = doc.createParagraph();
        xwpfParagraph.setStyle(style);
        xwpfParagraph.createRun().setText(text);
    }

    public static XWPFTable createTable(XWPFDocument doc, int rows, int cols) {
        XWPFTable table = doc.createTable(rows, cols);
        CTTblPr tablePr = table.getCTTbl().getTblPr();
        tablePr.getTblW().setType(STTblWidth.DXA);
        tablePr.getTblW().setW(new BigInteger("9000"));
        return table;
    }

    /**
     * 设置表格中单元格的宽度
     */
    public static void setTableCellWidth(XWPFTableCell cell, String width) {
        CTTc cttc = cell.getCTTc();
        CTTblWidth ctTblWidth = cttc.addNewTcPr().addNewTcW();
        ctTblWidth.setType(STTblWidth.DXA);
        ctTblWidth.setW(new BigInteger(width));
    }

    /**
     * 设置单元格文本居中
     */
    public static void setTableCellTextCenter(XWPFTableCell cell) {
        CTTc cttc = cell.getCTTc();
        CTTcPr ctPr = cttc.addNewTcPr();
        ctPr.addNewVAlign().setVal(STVerticalJc.CENTER);
        if(cttc.getPList().size()>0) cttc.getPList().get(0).addNewPPr().addNewJc().setVal(STJc.CENTER);
    }

    private static byte[] hexToBytes(String hex) {
        if (hex.length() < 1) {
            return null;
        } else {
            byte[] result = new byte[hex.length() / 2];
            int j = 0;
            for (int i = 0; i < hex.length(); i += 2) {
                result[j++] = (byte) Integer.parseInt(hex.substring(i, i + 2), 16);
            }
            return result;
        }
    }
}
