<#include "/templates/common/jdiy_macros.ftl"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>${jdiy.projectName}　管理平台</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="${ctx}/static/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="${ctx}/static/mgmt/style/admin.css" media="all">
    <style>
        table.inTable {
            width: 99%;
            margin-left: 15px;
            border-right: 1px #e2e2e2 solid;
            border-bottom: 1px #e2e2e2 solid;
        }

        .inTable th, .inTable td {
            padding: 6px;
            border-left: 1px #e2e2e2 solid;
            border-top: 1px #e2e2e2 solid;
        }

        .inTable th {
            background-color: #f2f2f2;
            text-align: right;
            padding-right: 10px;
        }

        .inTable td {
            background-color: #fff
        }

        .link {
            color: #2d97ff;
        }

        .link:hover {
            color: #cc2300;
            text-decoration: underline;
        }
    </style>
</head>
<body class="layui-layout-body">

<div id="LAY_app">
    <div class="layui-layout layui-layout-admin" id="body${_}">
        <div class="layui-header">
            <!-- 头部区域 -->
            <ul class="layui-nav layui-layout-left">
                <li class="layui-nav-item layadmin-flexible" lay-unselect>
                    <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
                        <i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
                    </a>
                </li>
                <li class="layui-nav-item" lay-unselect>
                    <a href="javascript:;" layadmin-event="refresh" title="刷新当前Tab标签页">
                        <i class="layui-icon layui-icon-refresh-3"></i>
                    </a>
                </li>

                <#if topMenu??>
                <#-- <li class="layui-nav-item"><a href="">控制台</a></li>
                 <li class="layui-nav-item"><a href="">商品管理</a></li>
                 <li class="layui-nav-item"><a href="">用户</a></li>-->
                    <#list topMenu as mm>
                        <li class="layui-nav-item">
                            <a <#if mm.url??&&mm.url!=''>lay-href="${mm.url}"</#if> style="cursor: pointer;">
                                <i class="layui-icon layui-icon-star-fill layui-anim layui-anim-rotate layui-anim-loop"></i>${mm.name}
                            </a>
                            <#if mm.children?? && mm.children?size gt 0>
                                <dl class="layui-nav-child">
                                    <#list mm.children as sm>
                                        <dd><a lay-href="${sm.url}">${sm.name}</a></dd></#list>
                                </dl>
                            </#if>
                        </li>
                    </#list>
                </#if>
                <#--<li class="layui-nav-item" style="color:#000;font-size:18px;">
                   xxxxxxxx
                </li>-->
                <#--<li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="${ctx}/" target="_blank" title="前台">
                        <i class="layui-icon layui-icon-website"></i>
                    </a>
                </li>-->
            </ul>
            <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">

                <#if jdiy.notifyUrl?? && jdiy.notifyUrl!=''>
                <li class="layui-nav-item" lay-unselect>
                    <audio id="notifyMp3"></audio>
                    <a id="adminNotify" href="${jdiy.notifyUrl}">
                        <i class="layui-icon layui-icon-notice"></i>
                        <span class="text">通知</span>
                        <span class="layui-badge layui-anim"
                              style="display:none;top: 0;margin-top: 16px;font-size: 11px;border-radius: 6px;margin-left: 0;padding: 0 5px 0 5px;">0</span>
                    </a>
                </li>
                </#if>

                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="theme">
                        <i class="layui-icon layui-icon-theme"></i>
                    </a>
                </li>
                <#--<li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="note">
                        <i class="layui-icon layui-icon-note"></i>
                    </a>
                </li>-->

                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="fullscreen">
                        <i class="layui-icon layui-icon-screen-full"></i>
                    </a>
                </li>
                <li class="layui-nav-item" lay-unselect>
                    <a href="javascript:;">
                        <#if store.avatar.url??><img src="${store.avatar}?_=${_}"
                                                     class="layui-nav-img"></#if>
                        <cite><#if CURRENT_USER.name?? && CURRENT_USER.name!=''>${CURRENT_USER.name}<#else>${CURRENT_USER.uid}</#if></cite>
                    </a>
                    <dl class="layui-nav-child">
                        <dd><a lay-href="${ctx}/mgmt/main/profile">基本资料</a></dd>
                        <dd><a lay-href="${ctx}/mgmt/main/pwd">修改密码</a></dd>
                        <hr>
                        <dd layadmin-event="logout" style="text-align: center;"><a>退出</a></dd>
                    </dl>
                </li>

                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="about"><i
                                class="layui-icon layui-icon-more-vertical"></i></a>
                </li>
                <li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm" lay-unselect>
                    <a href="javascript:;" layadmin-event="more"><i class="layui-icon layui-icon-more-vertical"></i></a>
                </li>
            </ul>
        </div>

        <!-- 侧边菜单 -->
        <div class="layui-side layui-side-menu">
            <div class="layui-side-scroll">
                <#assign welcome =jdiy.welcomeUrl/>
                <#if jdiy.welcomeUrl?? && jdiy.welcomeUrl!=''>
                    <#if welcome?starts_with("/")><#assign welcome=ctx+welcome/>
                    <#elseif !welcome?starts_with('http://') && !welcome?starts_with('https://') && !welcome?starts_with('../')>
                        <#assign welcome=ctx+"/"+welcome/>
                    </#if>
                <#else>
                    <#assign welcome =ctx + "/mgmt/main/welcome"/>
                </#if>
                <div class="layui-logo" lay-href="${welcome}">
                    <strong>主菜单</strong>
                </div>
                <ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu"
                    lay-filter="layadmin-system-side-menu">

                    <#macro printSysMenu menuList depth>
                        <dl class="layui-nav-child">
                            <#list menuList as menu>
                                <#if grant(menu.id) && (menu.state=0 || menu.state=2&&isDeveloper)>
                                    <#if menu.type?? && menu.type!='' && menu.type!='url' && menu.uid??>
                                        <#assign url = ctx+'/mgmt/JDiy/'+menu.id+"/"+menu.type+'.tab.'+menu.uid+
                                        ((menu.pageParam_?? && menu.pageParam_!='')?string('?'+menu.pageParam_,''))
                                        />
                                    <#elseif menu.type=='url'><#assign url = ctx+'/mgmt/'+menu.url/>
                                    <#else><#assign url=''/></#if>
                                    <dd data-name="${menu.id}">
                                        <#if url!=''>
                                            <a id="JDIY_MENU_${menu.id}" lay-href="${url}" lay-text="${menu.name}">
                                                <#if menu.icon?? && menu.icon!=''><i class="layui-icon ${menu.icon} main-mm-${depth}"></i></#if>
                                                <cite>${menu.name}</cite><span class="layui-badge"
                                                                               style="display: none;">0</span>
                                                <#if menu.state=2><sup style="color:red;" title="仅开发者可见">Dev</sup></#if>
                                            </a>
                                        <#else>
                                            <a id="JDIY_MENU_${menu.id}" href="javascript:;" lay-text="${menu.name}">
                                                <#if menu.icon?? && menu.icon!=''><i class="layui-icon ${menu.icon} main-mm-${depth}"></i></#if>
                                                <cite>${menu.name}</cite><span class="layui-badge"
                                                                               style="display: none;">0</span>
                                                <#if menu.state=2><sup style="color:red;" title="仅开发者可见">Dev</sup></#if>
                                            </a>
                                        </#if>
                                        <#if menu.children?? && (menu.children)?size gt 0>
                                            <@printSysMenu menuList=menu.children depth=depth+1/>
                                        </#if>
                                    </dd>
                                </#if>
                            </#list>
                        </dl>
                    </#macro>

                    <#assign firstIsOpen=false/><#--第一个有下级的主菜单默认展开-->
                    <#list menuList as menu>
                        <#if grant(menu.id) && (menu.state=0 || menu.state=2&&isDeveloper)>
                            <#if menu.type?? && menu.type!='' && menu.type!='url' && menu.uid??>
                                <#assign url = ctx+'/mgmt/JDiy/'+menu.id+'/'+menu.type+'.tab.'+menu.uid+
                                ((menu.pageParam_?? && menu.pageParam_!='')?string('?'+menu.pageParam_,''))
                                />
                            <#elseif menu.type=='url'><#assign url = ctx+'/mgmt/'+menu.url/>
                            <#else><#assign url=''/></#if>
                            <#assign hasChild = menu.children?? && menu.children?size gt 0/>
                            <li data-name="${menu.id}"
                                class="layui-nav-item <#if hasChild && !firstIsOpen><#assign firstIsOpen=true/> layui-nav-itemed</#if>">
                                <a id="JDIY_MENU_${menu.id}" <#if url!=''> lay-href="${url}"<#else> href="javascript:;"</#if>
                                   lay-text="${menu.name}" lay-tips="${menu.name}" lay-direction="2">
                                    <i class="layui-icon<#if menu.icon??> ${menu.icon}</#if> main-mm-0"></i>
                                    <cite>${menu.name}</cite>
                                    <span class="layui-badge" style="display: none;">0</span>
                                </a>
                                <#if hasChild><@printSysMenu menuList=menu.children depth=1/></#if>
                            </li>
                        </#if>
                    </#list>
                </ul>
            </div>
        </div>

        <!-- 页面标签 -->
        <div class="layadmin-pagetabs" id="LAY_app_tabs">
            <div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-down">
                <ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
                    <li class="layui-nav-item" lay-unselect>
                        <a href="javascript:;"></a>
                        <dl class="layui-nav-child layui-anim-fadein">
                            <dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
                            <dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
                            <dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
                <ul class="layui-tab-title" id="LAY_app_tabsheader">
                    <li lay-id="${welcome}" lay-attr="jdiyAdminHome" class="layui-this"><i
                                class="layui-icon layui-icon-home"></i></li>
                </ul>
            </div>
        </div>


        <!-- 主体内容 -->
        <div class="layui-body" id="LAY_app_body">
            <div class="layadmin-tabsbody-item layui-show localRefresh" id="jdiyAdminHome"
                 data-url="${welcome}">
                <#--<iframe src="${ctx}/mgmt/main/welcome" frameborder="0" class="layadmin-iframe"
                        style="width:100%;height:100%;"></iframe>-->
            </div>
        </div>

        <!-- 辅助元素，一般用于移动设备下遮罩 -->
        <div class="layadmin-body-shade" layadmin-event="shade"></div>
    </div>
</div>

<script src="${ctx}/static/jslib/jquery.js"></script>
<script src="${ctx}/static/mgmt/lib/public.js"></script>
<script src="${ctx}/static/mgmt/lib/jdiyAdmin.js"></script>
<script>
    jdiyAdmin.ctx = '${ctx}';
</script>
<script type="text/javascript" charset="utf-8" src="${ctx}/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="${ctx}/static/ueditor/ueditor.all.js"></script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="${ctx}/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script src="${ctx}/static/layui/layui.js"></script>
<script>
    layui.config({
        ctx: '${ctx}',
        base: '${ctx}/static/mgmt/' //静态资源所在路径
    }).extend({
        index: 'lib/index',
        cron: 'cron/cron'
    }).use(['index', 'table', "form", 'layer', 'element', 'laypage', 'laydate', 'colorpicker', 'tree', 'jdiyAdmin', 'cron'], function () {
        // window.layer=layui.layer;
        var element = layui.element,
            tabid = location.hash;
        var _home = $('#jdiyAdminHome');
        jdiyAdmin.load(_home, _home.data('url'));
        if (tabid && (tabid = tabid.replace(/^#\(\^_\^\)/, '${ctx}/mgmt/'))) {
            let t = $('#LAY-system-side-menu a[lay-href="' + tabid + '"]');
            if (t.length) layui.index.openTabsPage(tabid, t.find('cite').text());
            else {
                let t = localStorage.getItem("JDiyTabTextOf_" + tabid);
                if (t) layui.index.openTabsPage(tabid, t);
            }
        }
        <#if jdiy.notifyUrl?? && jdiy.notifyUrl!=''>
        $('#adminNotify').click(function () {
            $.currentDilog = layer.open({
                type: 8
                , title: '通知消息'
                , content: $(this).attr('href')
                , opener: ''
                , maxmin: true
                , area: ['640px', '480px']
                , btn: []
            });
            return false;
        });
        </#if>

    });
    console.log("%cpoweredBy:%cjdiy.club",
        "height:20px;line-height:20px;padding:3px 5px 3px 10px;border-radius:5px 0 0 5px;background-color:#049EFF;color:#FFFFFF;",
        "height:20px;line-height:20px;padding:3px  10px;border-radius:0px 5px 5px 0;background-color:#F0F0F0;color:#333333;"
    );
</script>

<#-- 列表页图片查看器 -->
<script src="${ctx}/static/jslib/imgviewer/viewer-jquery.min.js"></script>
<link rel="stylesheet" href="${ctx}/static/jslib/imgviewer/viewer.min.css">

<#-- JDiy baidu统计 -->
<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?adf0fdff33f3465d3f536861c75446d9";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>


</body>
</html>


