<#include "/templates/common/jdiy_macros.ftl"/>
<@body>
    <form class="layui-form" id="form${_}" lay-filter="form${_}" action="${ctx}/mgmt/main/changePwd" method="post"
          enctype="multipart/form-data">
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">修改登录密码</div>
                        <div class="layui-card-body" pad15>

                            <div class="layui-form" lay-filter="">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">当前密码</label>
                                    <div class="layui-input-inline">
                                        <@password name='oldPwd' verify="required" placeholder='请输入当前密码'/>
                                    </div>
                                    <#--<div class="layui-form-mid layui-word-aux"> 输入原密码</div>-->

                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">新密码</label>
                                    <div class="layui-input-inline">
                                        <@password name='newPwd' verify="required" placeholder='请输入新密码'/>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">确认新密码</label>
                                    <div class="layui-input-inline">
                                        <@password name='confPwd' verify="required" placeholder="请再次输入新密码"/>
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <div style="padding-left: 110px;">
                                        <@submit show=true/>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </form>
</@body>
<@footer>
    <script>
        layui.use([], function () {
            var form = layui.form;

            form.on('submit(submit${_})', function (data) {
                if (data.field.newPwd.length < 6) {
                    jdiyAdmin.error('密码长度不得小于6个字符');
                    return false;
                }
                if (data.field.newPwd !== data.field.confPwd) {
                    jdiyAdmin.error('两次密码输入不一致');
                    return false;
                }
                return jdiyAdmin.ajaxSubmit(data.form);
            });
        })
    </script>
</@footer>