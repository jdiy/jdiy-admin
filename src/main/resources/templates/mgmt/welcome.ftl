<#include "/templates/common/jdiy_macros.ftl"/>
<@body>
    <@card>
        <@cardHead>欢迎登录 ${jdiy.projectName} 管理平台</@cardHead>
        <@cardBody>
            <dl class="layuiadmin-card-status">
                <dd>
                    <div>
                        <p>当前帐号： ${CURRENT_USER.uid}</p>
                    </div>
                </dd>
                <dd>
                    <div>
                        <p>
                            用户角色： <#if CURRENT_USER.roleList??><#list CURRENT_USER.roleList as  role><#if role?index!=0>；</#if>${role.name}</#list></#if></p>
                    </div>
                </dd>
                <dd>
                    <div>
                        <p>用户姓名： ${CURRENT_USER.name}</p>
                    </div>
                </dd>
                <dd>
                    <div>
                        <p>登录时间： <#if CURRENT_USER.loginDt??>${CURRENT_USER.loginDt?datetime}</#if></p>
                    </div>
                </dd>
                <dd>
                    <div>
                        <p>登录IP地址： <#if CURRENT_USER.loginIp??>${CURRENT_USER.loginIp}</#if></p>
                    </div>
                </dd>
            </dl>
        </@cardBody>
    </@card>
</@body>
<@footer/>