<#include "/templates/common/jdiy_macros.ftl"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>${jdiy.projectName}　管理平台</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="${ctx}/static/mgmt/login/camera.css" />
    <link rel="stylesheet" href="${ctx}/static/mgmt/login/matrix-login.css" />
    <script type="text/javascript" src="${ctx}/static/jslib/jquery.js"></script>
    <link rel="stylesheet" href="${ctx}/static/layui/css/layui.css">
    <#if s='timeout'>
        <script type="text/javascript">
            if (document.location != top.location) top.location = document.location;
        </script>
    </#if>
</head>
<body <#if systemStore.loginBg.urls?size=1>style="background:url(${systemStore.loginBg.url}) no-repeat;background-size:100%;"</#if>>

<div style="width:100%;text-align: center;margin: 0 auto;position: absolute;<#if !systemStore.loginBg.url??||systemStore.loginBg.urls?size=0>background-image: linear-gradient(#c0c0c0, white);</#if>">
    <div id="windows1">
        <div id="loginbox">
            <div style="padding:10px 30px 30px 30px;" class="mask">
                <div class="control-group normal_text">
                    <h3>
                        ${jdiy.projectName}
                    </h3>
                    <h4>管理平台</h4>
                </div>

            <form class="layui-form layui-form-pane" method="post" action="${ctx}/mgmt/login" name="loginForm" id="loginForm" onsubmit="return severCheck()">
                <div class="layui-form-item">
                    <label class="layui-form-label" style="padding:8px 12px;width:46px;background-color: #28b779;color:white;border-width:0;"><i class="layui-icon layui-icon-friends"></i></label>
                    <div class="layui-input-block" style="margin-left:47px;">
                        <input type="text" name="uid" id="uid" value="${uid}" autocomplete="off" placeholder="请输入用户名" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="padding:8px 12px;width:46px;background-color: #ffb848;color:white;border-width:0;"><i class="layui-icon layui-icon-password"></i></label>
                    <div class="layui-input-block" style="margin-left:47px;">
                        <input type="password" name="pwd" id="pwd" autocomplete="off" placeholder="请输入密码" class="layui-input">
                    </div>
                </div>
                <#if jdiy.yzmOn>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="padding:8px 12px;width:46px;background-color: #c43f6d;color:white;border-width:0;"><i class="layui-icon layui-icon-picture"></i></label>
                    <div style="padding:0;position: absolute;margin-left:216px;" id="yzmImgDiv"><img style="cursor: pointer;width:112px;height:38px;border-width:0;" id="iYzm" alt="点击更换" title="点击更换" src="" /></div>
                    <div class="layui-input-block" id="yzmDIV" style="margin-left:47px;width:163px;">
                        <input type="text" name="yzm" id="yzm" lay-verify="required" placeholder="请输入图形验证码" autocomplete="off" class="layui-input">
                    </div>
                </div>
                </#if>
                <div class="layui-form-item">
                    <div class="layui-input-block" style="margin-left:0;">
                        <button type="submit" id="subbtn" class="layui-btn layui-btn-normal" style="width:100%;" lay-submit="" lay-filter="demo1">　登　录　</button>
                    </div>
                </div>

                <div id="errmsg" style="color:yellow;">
                    <#if s='timeout'>未登录或登录超时...
                    <#elseif s='logout'>您已安全退出本系统.
                    </#if>
                </div>
                <input type="hidden" id="E" value="${exponent}"/>
                <input type="hidden" id="M" value="${modulus}"/>
            </form>
            </div><div class="controls">
                <div class="main_input_box">
                    <font color="white"><span id="nameerr">Copyright ©  ${jdiy.projectName}</span></font>
                </div>
            </div>

        </div>
    </div>

</div>
<#if systemStore.loginBg.urls?size gt 1>
<div id="templatemo_banner_slide" class="container_wapper">
    <div class="camera_wrap camera_emboss" id="camera_slide">
        <#list systemStore.loginBg.urls as it>
        <div data-src="${it}"></div>
        </#list>
    </div>
    <!-- #camera_wrap_3 -->
</div>
</#if>
<script>
    //window.setTimeout(showfh,3000);
    var timer;
    function showfh(){
        fhi = 1;
        /*//关闭提示晃动屏幕，注释掉这句话即可*/
        timer = setInterval(xzfh2, 10);
    };
    var current = 0;
    function xzfh(){
        current = (current)%360;
        document.body.style.transform = 'rotate('+current+'deg)';
        current ++;
        if(current>360){current = 0;}
    };
    var fhi = 1;
    var current2 = 1;
    function xzfh2(){
        if(fhi>50){
            document.body.style.transform = 'rotate(0deg)';
            clearInterval(timer);
            return;
        }
        current = (current2)%360;
        document.body.style.transform = 'rotate('+current+'deg)';
        current ++;
        if(current2 == 1){current2 = -1;}else{current2 = 1;}
        fhi++;
    };
</script>
<script type="text/javascript">

    function severCheck(){
        if(check()){
            var uid = $("#uid").val();
            var pwd = $("#pwd").val();
            var exponent = $("#E").val();
            var modulus = $("#M").val();
            var yzm = $("#yzm").val();
            $.ajax({
                type: 'post',
                url: $('#loginForm').attr('action'),
                data: {
                    uid: uid,
                    pwd: RSAUtils.encryptedString(RSAUtils.getKeyPair(exponent, '', modulus), pwd),
                    yzm: yzm,
                    rememberPW: 0
                },
                dataType: 'json',
                success: function (ret) {
                    if (ret.code==200) document.location.reload();
                    else if(ret.msg.indexOf('验证码')!=-1){
                        $("#yzm").tips({
                            side : 1,
                            msg : ret.msg,
                            bg : '#FF5080',
                            time : 5
                        });
                        showfh();
                        $("#yzm").focus();
                    }else if(ret.msg.indexOf('用户名')!=-1){
                        $("#uid").tips({
                            side : 1,
                            msg : ret.msg,
                            bg : '#FF5080',
                            time : 5
                        });
                        showfh();
                        $("#uid").focus();
                    }else if(ret.msg.indexOf('密码')!=-1){
                        $("#pwd").tips({
                            side : 1,
                            msg : ret.msg,
                            bg : '#FF5080',
                            time : 5
                        });
                        showfh();
                        $("#pwd").focus();
                    }else{
                        $("#subbtn").tips({
                            side : 1,
                            msg : ret.msg,
                            bg : '#ff2d21',
                            time : 5
                        });
                        showfh();
                    }
                    if(ret.data=='refreshYzm') changeyzm1();
                }
            });
        }
        return false;
    }

    function IsPC() {
        var userAgentInfo = navigator.userAgent;
        var Agents = ["Android", "iPhone",
            "SymbianOS", "Windows Phone",
            "iPad", "iPod"];
        var flag = true;
        for (var v = 0; v < Agents.length; v++) {
            if (userAgentInfo.indexOf(Agents[v]) > 0) {
                flag = false;
                break;
            }
        }
        return flag;
    }
    $(function() {
        if(!IsPC()){
            $('#yzmDIV').css({width:125+'px'});
            $('#yzmImgDiv').css({marginLeft:172+'px'}).find('img').css("width","89px");
        }
        changeyzm1();
        $("#iYzm").bind("click", changeyzm1);
        $(document).keyup(function(event) {
            if (event.keyCode == 13) {
                $("#to-recover").trigger("click");
            }
        });
    });


    function changeyzm1() {
        $("#iYzm").attr("src", '${ctx}/etc/yzm?key=${yzmKey}&timestamp=' + new Date().getTime());
    }

    //客户端校验
    function check() {
        if ($("#uid").val() == "") {
            $("#uid").tips({
                side : 2,
                msg : '用户名不得为空',
                bg : '#AE81FF',
                time : 3
            });
            showfh();
            $("#uid").focus();
            return false;
        } else {
            $("#uid").val(jQuery.trim($('#uid').val()));
        }
        if ($("#pwd").val() == "") {
            $("#pwd").tips({
                side : 2,
                msg : '密码不得为空',
                bg : '#AE81FF',
                time : 3
            });
            showfh();
            $("#pwd").focus();
            return false;
        }
        if ($("#yzm").val() == "") {
            $("#yzm").tips({
                side : 1,
                msg : '验证码不得为空',
                bg : '#AE81FF',
                time : 3
            });
            showfh();
            $("#yzm").focus();
            return false;
        }
        $('#errmsg').remove();
        return true;
    }
</script>
<script src="${ctx}/static/mgmt/login/bootstrap.min.js"></script>
<script src="${ctx}/static/mgmt/login/jquery-1.7.2.js"></script>
<script src="${ctx}/static/mgmt/login/jquery.easing.1.3.js"></script>
<#if systemStore.loginBg.urls?size gt 1>
<script src="${ctx}/static/mgmt/login/camera.min.js"></script>
<script src="${ctx}/static/mgmt/login/templatemo_script.js"></script>
</#if>
<script type="text/javascript" src="${ctx}/static/mgmt/login/jquery.tips.js"></script>
<script src="${ctx}/static/jslib/security.js" type="text/javascript"></script>

<#-- JDiy baidu统计 -->
<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?adf0fdff33f3465d3f536861c75446d9";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>
</body>

</html>