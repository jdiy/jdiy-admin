<#include "/templates/common/jdiy_macros.ftl"/>
<#assign inWh=[600,430]/>
<@body>
    <div class="layui-row">
        <div class="layui-col-md6">
            <@card>
                <@cardHead>
                    数据字典：
                </@cardHead>
                <@cardBody>
                    <div style="min-height: 300px;">
                        <table lay-filter="table${_}"
                               class="layui-table" <#--lay-data="{initSort:{field:'uid', type:'asc'}}"-->>
                            <thead>
                            <tr>
                                <th lay-data="{field:'id',align:'center'<#--,sort:true-->}" style="text-align: center">
                                    KEY
                                </th>
                                <th lay-data="{field:'name',align:'center'}" style="text-align: center">名称</th>
                                <th lay-data="{field:'sortIndex',align:'center'}" style="text-align: center">排序</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#list pager.items as it>
                                <tr>
                                    <td>${it.id}</td>
                                    <td>${it.name}<#if it.hidden><sup style="color:red;">隐藏</sup></#if></td>
                                    <td>${it.sortIndex}</td>
                            </#list>
                            </tbody>
                        </table>
                        <div id="pager${_}"></div>
                    </div>
                </@cardBody>
            </@card>
        </div>
        <div class="layui-col-md6">
            <div style="margin-left:10px;" id="dictItemDiv${_}" class="localRefresh" data-url="${ctx}/mgmt/dict/0/ls">
            </div>
        </div>
    </div>
</@body>

<@footer>
    <script>
        layui.use([], function () {
            layui.table.on('row(table${_})', function (obj) {
                var con = $("#dictItemDiv${_}").data('url','${ctx}/mgmt/dict/'+obj.data.id+'/ls');
                jdiyAdmin.load(con);
                obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');//标注选中样式
            });
        });
    </script>
</@footer>
