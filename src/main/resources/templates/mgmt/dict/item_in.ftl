<#include "/templates/common/jdiy_macros.ftl"/>
<@body local=true>
    <form class="layui-form" id="form${_}" lay-filter="form${_}"
          action="${ctx}/mgmt/dict/${dict.id}/save" method="post" style="padding: 20px 30px 0 0;">
        <input type="hidden" name="id" value="${vo.id}"/>
        <@formRow>
            <@formItem label=dict.keyName col=1>
                <@input name='txt' verify="required"/>
            </@formItem>
        </@formRow>
        <#if dict.type!=1>
        <@formRow>
            <@formItem label=dict.valName col=1>
                <#if dict.autovalues>
                    <@input name='val' readonly=true placeholder="添加后由系统自动生成,不可编辑 " class="layui-bg-gray"/>
                <#else>
                    <@input name='val' verify="required"/>
                </#if>
            </@formItem>
        </@formRow>
        </#if>
        <#if dict.type==3>
        <@formRow>
            <@formItem label='显示颜色' col=1>
                <div id="colorBox${_}"></div>
                <input type="hidden" name="color" id="color${_}" value="${vo.color}"/>
            </@formItem>
        </@formRow>
        </#if>
        <@formRow>
            <@formItem label='排序索引' col=1>
                <@input name='sortIndex' verify="required|number"/>
            </@formItem>
        </@formRow>
        <div style="display: none;"><@submit/></div>
    </form>
</@body>
<@footer>
    <script>
        layui.use([], function () {
            var form = layui.form;
            form.on('submit(submit${_})', function (data) {
                return jdiyAdmin.ajaxSubmit(data.form);
            });

            layui.colorpicker.render({
                elem: '#colorBox${_}'
                , color: '${vo.color}'||'#000'
                , predefine: true
                , done: function (color) {
                    $('#color${_}').val(color);
                }
            });
        })
    </script>
</@footer>
