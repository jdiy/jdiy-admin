<#include "/templates/common/jdiy_macros.ftl"/>
<#assign inWh=[600,400]/>
<@body local=true>
    <@card>
        <#if dict??>
            <@cardHead>
                数据字典：<strong>${dict.name}</strong>
                <#if dict.addable>
                <div style="float:right;margin:10px 10px 0 0;">
                    <a href="${ctx}/mgmt/dict/${dict.id}/in?id=0" class="layui-btn layui-btn-sm" width="${inWh[0]}"
                       height="${inWh[1]}" target="dialog">添加条目</a>
                </div>
                </#if>
            </@cardHead>
            <@cardBody>
                <div style="min-height: 300px;">
                    <#if dict.remark?? && dict.remark?trim!=''>
                        <blockquote class="layui-elem-quote" style="color:red;">
                            ${dict.remark?replace('\n','<br/>')}
                        </blockquote>
                    </#if>
                    <table lay-filter="table${_}"
                           class="layui-table" <#--lay-data="{initSort:{field:'uid', type:'asc'}}"-->>
                        <thead>
                        <tr>
                            <th lay-data="{field:'txt',align:'center'}"
                                style="text-align: center">${(dict.keyName??)?string(dict.keyName,'名称')}</th>
                            <#if dict.type!=1>
                                <th lay-data="{field:'val',align:'center'}"
                                    style="text-align: center">${(dict.valName??)?string(dict.valName,'值')}</th></#if>
                            <th lay-data="{field:'sortIndex',align:'center'}" style="text-align: center">排序</th>
                            <#if dict.editable||dict.removable>
                                <th lay-data="{field:'mge',align:'center',width:200}" style="text-align: center">管理</th>
                            </#if>
                        </tr>
                        </thead>
                        <tbody>
                        <#list pager.items as it>
                            <tr>
                                <td>
                                    <div<#if dict.type=3> style="color:${it.color};" </#if>>${it.txt}<#if it.hidden><sup
                                                style="color:red;">隐藏</sup></#if></div>
                                </td>
                                <#if dict.type!=1>
                                    <td>${it.val}</td></#if>
                                <td>${it.sortIndex}</td>
                                <#if dict.editable||dict.removable>
                                    <td style="text-align: center;">
                                        <#if dict.editable && it.editable>
                                            <a class="layui-btn layui-btn-xs dblclick"
                                               href="${ctx}/mgmt/dict/${dict.id}/in?id=${it.id}"
                                               target="dialog"
                                               width="${inWh[0]}" height="${inWh[1]}" title="修改字典项">修改</a>
                                        </#if>

                                        <#if dict.removable && it.removable>
                                            &nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs"
                                                     href="${ctx}/mgmt/dict/deleteItem?id=${it.id}" target="ajaxTodo"
                                                     confirm="您确定要删除此字典项吗？(警告：此操作不可恢复！)">删除</a>
                                        </#if>
                                    </td>
                                </#if>
                            </tr>
                        </#list>
                        </tbody>
                    </table>

                    <div id="pager${_}"></div>
                </div>
            </@cardBody>
        <#else>
            <@cardHead>提示</@cardHead>
            <@cardBody>
                <div style="min-height: 300px;color:red;">请从左边选择一个字典进行管理．</div>
            </@cardBody>
        </#if>
    </@card>
</@body>

<@footer>
    <script>
        layui.use([], function () {

        });
    </script>
</@footer>
