<#include "/templates/common/jdiy_macros.ftl"/>
    <#assign inWh=[980,640]/>
<@body>
    <div style="display: none"><@shForm/></div>
    <@card>
        <@cardHead>
            <button class="layui-btn layui-btn-sm" href="${ctx}/mgmt/role/in" target="dialog"
                    width="${inWh[0]}" height="${inWh[1]}" title="添加新角色">添加角色
            </button>
        </@cardHead>
        <@cardBody>
            <table lay-filter="table${_}" class="layui-table"<#-- lay-data="{initSort:{field:'uid', type:'asc'}}"-->>
                <thead>
                <tr>
                    <th lay-data="{field:'name',align:'center',width:150,sort:true}" style="text-align: center">角色名称</th>
                    <th lay-data="{field:'sys',align:'center',width:90,sort:true}" style="text-align: center">类型</th>
                    <th lay-data="{field:'remark',align:'center'}" style="text-align: center">备注</th>
                    <th lay-data="{field:'sortIndex',align:'center',width:90,sort:true}" style="text-align: center">排序索引</th>
                    <th lay-data="{field:'mge',align:'center',width:150}" style="text-align: center">管理</th>
                </tr>
                </thead>
                <tbody>
                <#list pager.items as it>
                    <tr>
                        <td>${it.name}</td>
                        <td><#if it.sys><div style="color:red" title="系统角色不允许删除">系统角色</div><#else>-</#if></td>
                        <td>${it.remark}</td>
                        <td>${it.sortIndex}</td>
                    <td style="text-align: center;">
                        <#if grant(modelName+':edt')>
                            <a class="layui-btn layui-btn-xs dblclick" href="${ctx}/mgmt/role/in?id=${it.id}"
                               target="dialog"
                               width="${inWh[0]}" height="${inWh[1]}" title="修改角色">修改</a>
                        </#if>

                        <#if grant(modelName+':del') && !it.sys>
                            <a class="layui-btn layui-btn-danger layui-btn-xs"
                                     href="${ctx}/mgmt/role/remove?id=${it.id}" target="ajaxTodo"
                                     confirm="您确定要删除此角色吗？(警告：此操作不可恢复！)">删除</a>
                        </#if>

                    </td>
                    </tr>
                </#list>
                </tbody>
            </table>
            <div id="pager${_}"></div>
        </@cardBody>
    </@card>
</@body>
<@footer>
<script>
    layui.use([], function () {

    });
</script>
</@footer>