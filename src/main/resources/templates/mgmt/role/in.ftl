<#include "/templates/common/jdiy_macros.ftl"/>
<@body local=true>
    <form class="layui-form" id="form${_}" lay-filter="form${_}" action="${ctx}/mgmt/role/save" method="post"
          style="padding: 20px 30px 0 0;">
        <input type="hidden" name="id" value="${vo.id}"/>
        <@formRow>
            <@formItem label='角色名称' col=2>
                <@input name='name' verify='required'/>
            </@formItem>
            <@formItem label='排序索引' col=2>
                <@input name='sortIndex' verify='required|number'/>
            </@formItem>
        </@formRow>
        <@formRow>
            <@formItem label='备注' col=1>
                <@textarea name="remark"/>
            </@formItem>
        </@formRow>
        <@formRow>
            <@formItem label='权限设置' col=1>
                <input type="hidden" name="grantAuth" value="${vo.grantAuth}">

                <#if vo.sys>
                    <div style="color:red">系统内置角色，无法更改权限设置．</div>
                <#else>
                    <div style="text-align: left;background-color: #f9f9f9; display: inline-block;width:99%;padding:12px;">
                        <table class="roleTb" width="98%">
                            <#macro printMenu menuList space=0>
                                <#if menuList?? && menuList?size gt 0>
                                    <#list menuList as menu>
                                        <tr<#if menu.father??> class="mm_${menu.father.id}"</#if>>
                                            <th style="padding-left: ${space}px;" valign="top">
                                                <input class="roleBox" type="checkbox" title="${menu.name}"
                                                       id="role${menu.id}" lay-filter="roleBox${_}" lay-skin="primary"
                                                       <#if menu.father??>father="role${menu.father.id}"</#if>
                                                       value="${menu.id}" ${vo.grantAuth?contains("'"+menu.id+"'") ?string( ' checked','')} />

                                            </th>
                                            <td class="mm_${menu.id}">
                                                <#if realmsMap?? && realmsMap[menu.id]?? && realmsMap[menu.id]?size gt 0>
                                                    <#list realmsMap[menu.id] as rs>
                                                        <#if rs?has_content && rs?split(":")?size==2>
                                                            <#local ra=rs?split(":")/>
                                                            <#local hasR=true/>
                                                            <input type="checkbox" class="roleBox realmsBox"
                                                                   lay-filter="roleBox${_}"
                                                                   title="${ra[0]?trim}" lay-skin="primary"
                                                                   father="role${menu.id}"
                                                                   value="${menu.id}:${ra[1]?trim}"<#if vo.grantAuth?contains("'"+menu.id+":"+ra[1]?trim+"'")>
                                                            checked</#if>/>
                                                        </#if>
                                                    </#list>
                                                    <#if hasR>
                                                        　
                                                    <button class="layui-btn layui-btn-xs realmAll" type="button"
                                                            style="margin-top: 10px;" data-mm="mm_${menu.id}">
                                                            反选</button></#if>
                                                </#if>
                                            </td>
                                        </tr>
                                        <@printMenu menuList=menu.children space=space+27/>
                                    </#list>
                                </#if>
                            </#macro>
                            <@printMenu menuList=menuList/>
                        </table>
                    </div>
                </#if>
            </@formItem>
        </@formRow>
        <div style="display: none;"><@submit/></div>
    </form>
</@body>
<@footer>
    <script>
        layui.use([], function () {
            var layForm = layui.form;
            var jqForm = $('#form${_}');
            layForm.render(null, 'form${_}');

            jqForm.find('.realmAll').click(function () {
                var checkedOne = null;
                jqForm.find('.' + $(this).data('mm') + ' .realmsBox').each(function () {
                    var c = !this.checked;
                    $(this).prop('checked', c);
                    if (checkedOne==null && c) dgUp(checkedOne=this);
                });
                layForm.render(null, 'form${_}');
            });

            function setGrantAuth(form) {
                var ra = [];
                jqForm.find(".roleBox").each(function () {
                    if (this.checked) ra.push("'" + this.value + "'");
                });
                form['grantAuth'].value = ra.join(',');
            }


            layForm.on('submit(submit${_})', function (data) {
                <#if !vo.sys>
                setGrantAuth(data.form);

                if (data.form.grantAuth.value === '') {
                    jdiyAdmin.error('请选择此角色可操作的权限！');
                    return false;
                }
                </#if>

                return jdiyAdmin.ajaxSubmit(data.form);
            });


            function dgDown(dom) {
                if ($(dom).hasClass('realmsBox')) return;
                var chk = dom.checked;
                jqForm.find('.mm_' + dom.value + ' .roleBox').each(function () {
                    $(this).prop('checked', chk);
                    dgDown(this);
                });
            }

            function dgUp(dom) {
                var po = jqForm.find('#' + ($(dom).attr('father') || '____'));
                if (po.length) {
                    po.prop("checked", true);
                    dgUp(po[0]);
                }
            }

            layui.form.on('checkbox(roleBox${_})', function (data) {
                if (data.elem.checked) dgUp(data.elem);
                dgDown(data.elem);
                layForm.render(null, 'form${_}');
            });
        })
    </script>
</@footer>