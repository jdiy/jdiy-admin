<#include "/templates/common/jdiy_macros.ftl"/>
<@body>
    <#assign vo=CURRENT_USER/>
<form class="layui-form" id="form${_}" lay-filter="form${_}" action="${ctx}/mgmt/main/updateProfile" method="post"
      enctype="multipart/form-data">
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-header">设置我的资料</div>
                    <div class="layui-card-body" pad15>

                        <div class="layui-form" lay-filter="">
                            <div class="layui-form-item">
                                <label class="layui-form-label">我的角色</label>
                                <div class="layui-input-inline">
                                    <input type="text" value="<#list vo.roleList as role>${role.name}；</#list>"
                                           class="layui-input" disabled/>

                                </div>
                                <div class="layui-form-mid layui-word-aux">当前角色不可更改为其它角色</div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">用户名</label>
                                <div class="layui-input-inline">
                                    <input type="text" value="${vo.uid}" class="layui-input" disabled/>
                                </div>
                                <div class="layui-form-mid layui-word-aux"> 用户名不可更改</div>

                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">姓名</label>
                                <div class="layui-input-inline">
                                    <@input name='name' verify="required"/>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">性别</label>
                                <div class="layui-input-block">
                                    <input type="radio" name="gender" value="1"
                                           title="男"<#if vo.gender==1> checked</#if>/>
                                    <input type="radio" name="gender" value="2"
                                           title="女"<#if vo.gender==2> checked</#if>/>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">头像</label>
                                <div class="layui-input-inline">
                                    <input type="file" name="avatar"/>
                                </div>
                                <div class="layui-input-inline layui-btn-container" style="width: auto;">
                                    <#if store.avatar.url??>
                                    <a href="${store.avatar}?_=${_}" target="_blank" style="margin-left:20px;"><img src="${store.avatar}?_=${_}" style="width:50px;height:50px;"/></a></#if>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">手机</label>
                                <div class="layui-input-inline">
                                    <@input name='phone'/>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">E-mail</label>
                                <div class="layui-input-inline">
                                    <@input name='eml'/>
                                </div>
                            </div>
                            <div class="layui-form-item layui-form-text">
                                <label class="layui-form-label">备注</label>
                                <div class="layui-input-block">
                                    <textarea name="remark" placeholder="请输入内容"
                                              class="layui-textarea">${vo.remark}</textarea>
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <div style="padding-left: 110px;">
                                    <@submit show=true/>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</@body>
<@footer>
    <script>
        layui.use([], function () {
            var form = layui.form;

            form.on('submit(submit${_})', function (data) {
                return jdiyAdmin.ajaxSubmit(data.form);
            });
        })
    </script>
</@footer>