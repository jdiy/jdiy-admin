<#assign ctx = request.contextPath/>
<#macro body local=false>
    <#if local>
        <div id="body${_}"><#nested /></div>
    <#else>
        <div class="layui-fluid" id="body${_}">
            <div class="layui-row layui-col-md12">
                <#nested />
            </div>
        </div>
    </#if>
</#macro>


<#macro card>
    <div class="layui-card">
        <#nested />
    </div>
</#macro>
<#macro cardHead>
    <div class="layui-card-header">
        <#nested />
    </div>
</#macro>
<#macro cardBody>
    <div class="layui-card-body">
        <#nested />
    </div>
</#macro>

<#macro shForm>
    <@card>
        <@cardBody>
            <form method="post" class="layui-form shForm" lay-filter="shForm${_}" id="shForm${_}">
                <div class="layui-form-item" style="margin-bottom: 0;">
                    <input type="hidden" name="orderField" value="${orderField}"/>
                    <input type="hidden" name="orderDirection" value="${orderDirection}"/>
                    <#nested />
                </div>
            </form>
        </@cardBody>
    </@card>
</#macro>
<#macro shFormItem label='' width=0>
    <div class="layui-inline">
        <#if label!=''><label class="layui-form-mid">${label}</label></#if>
        <div class="layui-input-inline"<#if width gt 0> style="width:${width}px;"</#if>>
            <#nested />
        </div>
    </div>
</#macro>

<#macro submit value='提交' icon='' show=false>
    <div class="layui-inline"<#if show=false> style="display: none"</#if>>
        <button type="submit" class="layui-btn layui-btn-normal" lay-submit lay-filter="submit${_}"><#if icon!=''><i
                class="layui-icon ${icon}"></i></#if>${value}</button>
    </div>
</#macro>

<#macro export action>
    <div class="layui-inline" style="width: 80px;">
        <button type="button" action="${action}" class="layui-btn layui-btn-primary export"><i
                    class="layui-icon layui-icon-table"></i>导出
        </button>
    </div>
</#macro>


<#macro  webEditor name value='@name'>
    <div style="width:100%">
        <textarea name="${name}" id="${name}${_}" style="display: none;"></textarea>
        <script id="UE${name}${_}" type="text/plain"
                style="width:100%;height:350px;"><#if value='@name'>${vo[name]}<#else>${value}</#if></script>
        <script> var ue_${name}${_} = UE.getEditor('UE${name}${_}');</script>
    </div>
</#macro>


<#macro footer>
    <script>
        layui.use([], function () {
            <#if pager??>
            layui.jdiyAdmin.init('${_}', {
                pageSize: ${pager.pageSize},
                page: ${pager.page},
                rowCount: ${pager.rowCount},
                thisCount: ${pager.items?size}
            }, {
                field: '${orderField}',
                type: '${(orderDirection?? && orderDirection?lower_case=='desc')?string('desc','asc')}'
            });
            <#else>
            layui.jdiyAdmin.init('${_}');
            </#if>
        });
    </script>
    <#nested /> <#--必须在后，否则menu/ls的表格中的管理按钮不正常-->
</#macro>



<#macro formRow>
    <div class="layui-form-item">
        <#nested />
    </div>
</#macro>
<#macro formItem label col=2>
    <div class="layui-col-xs${12/col}">
        <label class="layui-form-label">${label}</label>
        <div class="layui-input-block">
            <#nested />
        </div>
    </div>
</#macro>
<#macro input name value='@name' verify='' placeholder='' maxlength='' readonly=false vo=vo class=''>
    <input type="text" name="${name}" maxlength="${maxlength}"
           value="<#if value='@name'>${vo[name]}<#else>${value}</#if>"<#if verify??&&verify!=''> lay-verify="${verify}"</#if>
           placeholder="${placeholder}" autocomplete="off" class="layui-input${(class!='')?string(' '+class,'')}"<#if readonly> readonly</#if>/>
</#macro>
<#macro password name value='@name' verify='' placeholder=''>
    <input type="password" name="${name}"
           value="<#if value='@name'>${vo[name]}<#else>${value}</#if>"<#if verify??&&verify!=''> lay-verify="${verify}"</#if>
           placeholder="${placeholder}" autocomplete="off" class="layui-input">
</#macro>
<#macro textarea name value='@name' verify='' style='' placeholder=''>
    <textarea name="${name}" <#if style!=''> style="${style}"</#if>
              class="layui-textarea"
              placeholder="${placeholder}"<#if verify??&&verify!=''> lay-verify="${verify}"</#if>><#if value='@name'>${vo[name]}<#else>${value}</#if></textarea>
</#macro>