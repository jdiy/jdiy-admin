<#if result??>${result}
<#elseif script??>
<script type="text/javascript">${script}</script>
<#elseif url??>
<script type="text/javascript">document.location.href = '${url}';</script>
<#elseif img??>
<img src="${img}"/>
<#elseif urcode??>
<div style="text-align: center;padding:15px;">
    <div style="color:green;margin-bottom:5px;">请使用微信[扫一扫]完成支付</div>
    <img src="${urcode}"/>
</div>
</#if>