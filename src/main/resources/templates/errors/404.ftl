<!DOCTYPE html>
<html id="html" style="font-size:1px">
<head>
    <title>404 File Not Found !</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="user-scalable=false"/>
    <#--<link rel="shortcut icon" type="image/x-icon" href="${ctx}/static/error/favicon.png"/>-->
    <style type="text/css">

        body{
            width:100vw;
            height:100vh;
            background-color:#000000;
        }
        #img{
            position:relative;
            width:900rem;
            height:506rem;
            top:50%;
            left:50%;
            transform:translate(-50%,-50%);
        }
    </style>
</head>
<body >
<img id="img" src="${ctx}/static/errPages/404.jpg">
</body>
</html>