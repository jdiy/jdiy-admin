<#include "/templates/common/jdiy_macros.ftl"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script type="text/javascript" src="${ctx}/static/jslib/jquery.js"></script>
    <style type="text/css">.ExBody th {
        text-align: right;
        background-color: #D5FFD5;
    }

    .ExBody {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 12px;
        color: #002
    }

    .ExTit {
        font-weight: bold;
        line-height: 27px;
        margin-top: 10px;
        color: #060;
    }

    .ExMsg {
        padding: 5px 10px;
        background-color: #D5FFD5;
        border: 1px dotted #12BE0E;
    }</style>
    <title>系统错误页</title></head>
<body>
<div style="color:red;font-family:Verdana, Arial, Helvetica, sans-serif;font-size: 12px;">共找到<strong
        style="font-size:16px"> 1</strong> 处错误。　<input type="button"
                                                       style="background-color:#eee;padding:1px;margin:0px;color:blue"
                                                       onClick="document.location.replace('/');" value="返回首页"/><br/>页面地址：
    <script type="text/javascript">document.write(document.location.href);</script>
    <input type="button" style="background-color:#eee;padding:1px;margin:0px;color:blue"
           onClick="document.location.reload();" value="刷新"/>
    　
</div>
<br/>

<div class="ExBody">
    <div class="ExTit">异常描述1：</div>
    <div class="ExMsg">
        <p>发生系统错误......</p>

        <p>
            出于对系统本身的安全考虑，错误信息已被屏蔽。如果您确认访问地址为正常浏览，请与本程序开发者联系。</p>

    </div>
</div>
<hr/>
<br/><br/><br/>
</body>
</html>
