/** layuiAdmin.std-v1.2.1 LPPL License By http://www.layui.com/admin/ */
;layui.extend({setter: "config", admin: "lib/admin", view: "lib/view"}).define(["setter", "admin"], function (a) {
    var setter = layui.setter, element = layui.element, admin = layui.admin, tabsPage = admin.tabsPage,
        view = layui.view,
        fn = function (tabUrl, tabTit) {
            var sel = null, contentId = 'jdiyAdminTab' + new Date().getTime(),
                tabs = $("#LAY_app_tabsheader>li");
            tabs.each(function (e) {
                var i = $(this), n = i.attr("lay-id");
                if (n === tabUrl) {
                    sel = i;
                    return false;
                }
            });
            if (sel) {
                sel.click();
                setTimeout(function () {
                    jdiyAdmin.reloadTab(tabUrl);
                }, 100);
            } else {
                tabTit = tabTit || "新标签页";
                //$(LAYBODY).find('.layadmin-tabsbody-item.layui-show').removeClass('layui-show');
                $(LAYBODY).append(['<div class="layadmin-tabsbody-item layui-show jdiyAdmin-body" data-url="' + tabUrl + '" id="' + contentId + '">', "</div>"].join(""));
                tabsPage.index = tabs.length;
                element.tabAdd(TABS, {
                    title: "<span>" + tabTit + "</span>",
                    id: tabUrl,
                    attr: contentId
                });
                localStorage.setItem("JDiyTabTextOf_" + tabUrl, tabTit);
                element.tabChange(TABS, tabUrl);
                admin.tabsBodyChange(tabsPage.index, {url: tabUrl, text: tabTit});
                jdiyAdmin.load($(".layadmin-tabsbody-item.layui-show"), tabUrl);
            }

            element.on('tab(' + TABS + ')', function (data) {
                location.hash = '(^_^)' + this.getAttribute('lay-id').replace(jdiyAdmin.ctx + '/mgmt/', '');
                var content = $('#' + this.getAttribute('lay-attr'));
                //if(!content.hasClass('layui-show'))content.addClass('layui-show');
                admin.tabsBodyChange(data.index, {url: this.getAttribute('lay-id'), text: $(this).find('span').text()});
                content.find('.layui-table-view').each(function () {
                    layui.table.resize($(this).attr('lay-id'));
                });
                if ($(this).attr('lay-attr') === 'jdiyAdminHome') {
                    jdiyAdmin.load($('#jdiyAdminHome'));//welcome需要每次点开加载．不然echars可能显示不正常．
                }
            });
            element.on('tabDelete(' + TABS + ')', function () {
                var the = $(this).parent();
                $('#' + the.attr('lay-attr')).remove();
                the.remove();
            });
        },
        LAYBODY = "#LAY_app_body", TABS = "layadmin-layout-tabs", $ = layui.$;
    $(window);
    admin.screen() < 2 && admin.sideFlexible(), layui.config({base: setter.base + "modules/"}), layui.each(setter.extend, function (a, i) {
        var n = {};
        n[i] = "{/}" + setter.base + "lib/extend/" + i, layui.extend(n)
    }), view().autoRender(), layui.use("common"), a("index", {openTabsPage: fn})
});