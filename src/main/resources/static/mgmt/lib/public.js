String.prototype.trim = function() {
    var t = this.replace(/^\s+/,"");
    for(var i=t.length;i--;)
        if(/\S/.test(t.charAt(i))){
            t=t.substring(0,i+1);
            break;
        }
    return t;
};