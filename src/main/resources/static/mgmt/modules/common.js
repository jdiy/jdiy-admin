/** layuiAdmin.std-v1.2.1 LPPL License By http://www.layui.com/admin/ */
;layui.define(function (e) {
    var i = (layui.$, layui.layer, layui.laytpl, layui.setter, layui.view, layui.admin);
    i.events.logout = function () {
        layer.confirm('您确定要退出本系统吗？', {offset:'t'},function(index){
            layer.close(index);
            $.post(jdiyAdmin.ctx + "/mgmt/login?s=logout", function (ret) {
                    if(ret.code===200){
                        document.location.replace(jdiyAdmin.ctx+'/mgmt/');
                    }
            });
        });
    }, e("common", {})
});