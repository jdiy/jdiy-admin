layui.define(['jdhp'], function (exports) {
    function design_ls_main(args) {
        if (window.jdiyAdmin === undefined) window.jdiyAdmin = {};
        let body = $('body'),
            sData = {},
            sBtns = [{}, {}],
            sCols = {},
            sQos = {},
            $g = s => $('#' + s + '_' + args.uid),
            canvas = $g('lsCanvas'),
            rtForm = $g('lsProps'),
            divQos = $g('ls_qosDiv'),
            divBtn0 = $g('ls_btn0Div'),
            $selQo = () => {
                let t = canvas.find('.jdiy-selected');
                return {t: t, d: sQos[t.attr('id').substring(10)]}
            },
            $selBtn = () => {
                let t = canvas.find('.jdiy-selected'),
                    pos = t.hasClass('topbtn') ? 0 : 1;
                return {t: t, pos: pos, d: sBtns[pos][t.attr('id').substring(10)]}
            },
            $selCol = () => {
                let t = canvas.find('.jdiy-selected');
                return {t: t, d: sCols[t.attr('id').substring(10)]}
            },
            $gs = function (s, h = []) {
                if (s.length) for (let i = s.length; i--;) $g(s[i]).show();
                if (h.length) for (let i = h.length; i--;) $g(h[i]).hide();
            },
            fillOpts = (o, pleaseText, items, selVal, jgFunc = (it) => it) => {
                let sa = pleaseText === false ? [] : ['<option value="">' + pleaseText + '</option>'];
                for (let i = 0; i < items.length; i++) {
                    let [v, k] = jgFunc(items[i]);
                    sa.push('<option value="' + v + '"' + (String(v) === String(selVal) ? ' selected' : '') + '>' + k + '</option>');
                }
                return o.html(sa.join(''));
            },
            getColText = (c) =>
                (c.format === 'manyToOne' && c.field ? c.field + ' <strong style="color:#c00">↓</strong>' : '') +
                (c.format === 'oneToMany' ? '<div style="color:gray">一对多</div>' : '') +
                (c.format === 'manyToMany' ? '<div style="color:gray">多对多</div>' : '') +
                (c.format === 'tpl' ? '<div style="color:gray">模板</div>' : '') +
                (c.format === 'img' ? '<div style="color:gray"><i class="layui-icon layui-icon-picture"></i></div>' : '') +
                (c.format === 'file' ? '<div style="color:gray"><i class="layui-icon layui-icon-link"></i></div>' : '') +
                (',manyToOne,oneToMany,manyToMany,'.indexOf(c.format) === -1
                        ? c.field || ''
                        : (c.joinTable && c.joinField ? '<div style="color:#767d07">' + c.joinTable + "." + c.joinField + '</div>' : '')
                )
            ,
            loadFields = function (e, et, s) {
                $.ajax({
                    url: jdiyAdmin.ctx + '/mgmt/JDiyAdmin/ui.' + (e ? 'fields?entity=' : 'columns?table=') + et,
                    success: s
                });
            },
            btnTypes = [
                [
                    ['add', '添加'],
                    ['del', '选中项批量删除'],
                    ['update', '选中项更新字段值'],
                    ['edit', '选中项弹表单更新字段值'],
                    ['ajax', '选中项执行自定义ajax'],
                    ['post', '选中项post到自定义页面'],
                    ['exec', '选中项执行增强代码'],
                    ['page', '打开系统界面'],
                    ['link', "打开自定义页面"]
                ],
                [
                    ['edit', '修改'],
                    ['del', '删除'],
                    ['view', '进详情页'],
                    ['page', '打开系统界面'],
                    ['link', '打开自定义页面'],
                    ['update', '更新字段值'],
                    ['ajax', '执行自定义ajax请求'],
                    ['exec', '执行代码(列表增强)']
                ]
            ],
            selectOn = function (a, b) {
                layui.form.on('select(' + a + '_' + args.uid + ')', b);
            },
            switchOn = function (a, b) {
                layui.form.on('switch(' + a + '_' + args.uid + ')', b);
            },
            qo_fieldsOn = function (o) {
                let {t, d} = $selQo(), v;
                $g('qo_fieldDiv').show();
                if (o.value === '@') {
                    $gs(['qo_tFieldDiv'], ['qo_operDiv']);
                    $g('qo_tField').val(d.field = v = '');
                } else {
                    $gs(['qo_operDiv'], ['qo_tFieldDiv']);
                    v = (d.field = o.value);
                    if (v) v += (d.joinTable && d.joinField ? '(' + d.joinTable + '.' + d.joinField + ')' : '');
                }
                if (d.type !== 'select') {
                    t.find('input').val(v);
                } else {
                    t.find('input.layui-input').val(v);
                }
            },
            col_fieldsOn = function (o) {
                let {d} = $selCol();
                if (o.value === '@') {
                    $gs(['col_tFieldDiv']);
                    $g('col_tField').val(d.field || '');
                } else {
                    d.field = o.value;
                    $gs([], ['col_tFieldDiv']);
                }

                let opt = $('#col_field_' + args.uid)[0],
                    ft = opt[opt.selectedIndex].text;
                if (ft.indexOf('int(') !== -1 || ft.indexOf('decimal(') !== -1 || ft.indexOf('double') !== -1) {
                    $gs(['col_sumDiv']);
                } else {
                    $gs([], ['col_sumDiv']);
                    d.sum = false;
                }
                if (d.sum) {
                    $g('col_sum').prop('checked', true);
                    $gs(['col_sumPosDiv']);
                } else {
                    $g('col_sum').prop('checked', false);
                    $gs([], ['col_sumPosDiv']);
                }

                $('#jdiylsotd_' + d.id).html(getColText(d));
                layui.form.render(null, rtForm.attr('id'));
            },
            optTableOn = function (o) {
                let {d} = $selQo(),
                    ok = function (r) {
                        let jg = (it) => [it[0], it[0] + '　- ' + it[1]];
                        fillOpts($g('optTxtField'), '请选择', r, d.optTxtField, jg);
                        fillOpts($g('optValField'), '请选择', r, d.optValField, jg);
                        layui.form.render('select', rtForm.attr('id'));
                    };
                if (!o.value) {
                    ok([]);
                    return;
                }
                loadFields(d.optType === 'entity', o.value, function (ret) {
                    if (ret.code === 200 && ret.data) {
                        ok(ret.data);
                    } else ok([]);
                });
            },
            optTypeOn = function (o) {
                let {d} = $selQo();
                d.optType = o.value || null;
                if (o.value === 'entity') {
                    $gs(['optsTEDiv', 'optEntityDiv','cascadeDiv'], ['optTableDiv', 'optDictDiv', 'optQryDiv']);
                    optTableOn({value: $g('optEntity').val(d.optEntity || '').val()});
                    $g('optWhere').val(d.optWhere);
                    $g('optSort').val(d.optSort);
                    $g('optWhere').attr('placeholder', "(JPQL条件片段)\n例：o.parent.id=3 and o.user.id='${user.id}'");
                } else if (o.value === 'table') {
                    $gs(['optsTEDiv', 'optTableDiv','cascadeDiv'], ['optEntityDiv', 'optDictDiv', 'optQryDiv']);
                    optTableOn({value: $g('optTable').val(d.optTable || '').val()});
                    $g('optWhere').val(d.optWhere);
                    $g('optSort').val(d.optSort);
                    $g('optWhere').attr('placeholder', "(SQL条件片段)\n例：o.tid=3 and o.user_id='${user.id}'");
                } else if (o.value === 'dict') {
                    //todo 动态加载dict并设置初值
                    $g('optDict').val(d.optDict);
                    $gs(['optDictDiv'], ['optsTEDiv', 'optQryDiv','cascadeDiv']);
                } else if (o.value === 'jql') {
                    $gs(['optQryDiv','cascadeDiv'], ['optsTEDiv', 'optDictDiv']);
                    $g('optQry').val(d.optQry || '')
                        .attr('placeholder', '例：select o.id as id,o.name as name from Xxx o where o.parent.id=9 and user.id=\'${user.id}\'');
                } else if (o.value === 'sql') {
                    $gs(['optQryDiv','cascadeDiv'], ['optsTEDiv', 'optDictDiv']);
                    $g('optQry').val(d.optQry || '')
                        .attr('placeholder', '例：select id,name from xxx where tid=9 and user_id=\'${user.id}\'');
                } else {
                    $gs([], ['optsTEDiv', 'optEntityDiv', 'optTableDiv', 'optDictDiv', 'optQryDiv','cascadeDiv']);
                }
                $g('cascade').prop('checked', d.cascade);
            },
            col_manyTableOn = function (o) {
                let {d} = $selCol();
                d.manyTable = o.value;
                loadFields(false, o.value, function (r) {
                    fillOpts($g('col_manyField0'), '正向关联本表字段', r.data, d.manyField0, (it) => [
                        it[0], it[0] + '　- ' + it[1]
                    ]);
                    fillOpts($g('col_manyField1'), '反向关联外表字段', r.data, d.manyField1, (it) => [
                        it[0], it[0] + '　- ' + it[1]
                    ]);
                    layui.form.render('select', rtForm.attr('id'));
                });
            },
            qo_manyTableOn = function (o) {
                let {d} = $selQo();
                d.manyTable = o.value;
                loadFields(false, o.value, function (r) {
                    fillOpts($g('qo_manyField0'), '正向关联本表字段', r.data, d.manyField0, (it) => [
                        it[0], it[0] + '　- ' + it[1]
                    ]);
                    fillOpts($g('qo_manyField1'), '反向关联外表字段', r.data, d.manyField1, (it) => [
                        it[0], it[0] + '　- ' + it[1]
                    ]);
                    layui.form.render('select', rtForm.attr('id'));
                });
            },
            col_joinTableOn = function (o) {
                let {d} = $selCol();
                d.joinTable = o.value;
                loadFields(false, o.value, function (r) {
                    fillOpts($g('col_joinField'), '请选择外联显示字段', r.data, d.joinField, (it) => [
                        it[0], it[0] + '　- ' + it[1]
                    ]);
                    fillOpts($g('col_joinRef'), '请选择外键字段', r.data, d.joinRef, (it) => [
                        it[0], it[0] + '　- ' + it[1]
                    ]);
                    $('#jdiylsotd_' + d.id).html(getColText(d));
                    layui.form.render('select', rtForm.attr('id'));
                });
            },
            qo_joinTableOn = function (o) {
                let {d} = $selQo();
                d.joinTable = o.value;
                loadFields(false, o.value, function (r) {
                    d.joinField = fillOpts($g('qo_joinField'), '外联表查询字段', r.data, d.joinField, (it) => [
                        it[0], it[0] + '　- ' + it[1]
                    ]).val();
                    d.joinRef = fillOpts($g('qo_joinRef'), '子表外键', r.data, d.joinRef, (it) => [
                        it[0], it[0] + '　- ' + it[1]
                    ]).val();

                    layui.form.render('select', rtForm.attr('id'));
                });
            },
            qo_joinTypeOn = function (o) {
                let {t,d} = $selQo(), setF = function (noAt) {
                    let o = $g('qo_field'), val = d.field || '';
                    loadFields(args.bindType === 'entity', args.bindType === 'table' ? args.mainTable : args.mainEntity, function (r) {
                        fillOpts(o, '请选择', r.data, val, (it) => [
                            it[0], it[0] + '　- ' + it[1]
                        ]);
                        if(!noAt)$g('qo_field').append('<option value="@">其它自定义</option>');
                        if (!val || o.find('option[value="' + val + '"]').length) {
                            $g('qo_tFieldDiv').hide();
                        } else {
                            o.val('@');
                            $g('qo_tFieldDiv').show();
                            $g('qo_tField').val(d.field || '');
                        }
                        layui.form.render('select', rtForm.attr('id'));
                    });
                };
                d.joinType = o.value || null;

                if ($g('qo_field').val() != '@') {
                    $gs(['qo_operDiv']);
                }
                if (!d.joinType) {
                    setF();
                    $gs(['qo_fieldDiv'], ['qo_sqlDiv','qo_joinTableDiv', 'qo_joinRefDiv', 'qo_joinFieldDiv', 'qo_manyTableDiv', 'qo_manyField0Div', 'qo_manyField1Div']);
                    $g('qo_fieldDiv').find('.layui-form-label').html('查询字段');
                } else if (d.joinType === 'manyToOne') {
                    setF(true);
                    qo_joinTableOn({value: $g('qo_joinTable').val(d.joinTable || '').val()});
                    $gs(['qo_fieldDiv', 'qo_joinTableDiv', 'qo_joinFieldDiv'], ['qo_sqlDiv','qo_joinRefDiv', 'qo_manyTableDiv', 'qo_manyField0Div', 'qo_manyField1Div']);
                    $g('qo_fieldDiv').find('.layui-form-label').html('本表外键');
                } else if (d.joinType === 'oneToMany') {
                    qo_joinTableOn({value: $g('qo_joinTable').val(d.joinTable || '').val()});
                    $gs(['qo_joinTableDiv', 'qo_joinRefDiv', 'qo_joinFieldDiv'], ['qo_sqlDiv','qo_fieldDiv', 'qo_tFieldDiv', 'qo_manyTableDiv', 'qo_manyField0Div', 'qo_manyField1Div']);
                } else if (d.joinType === 'manyToMany') {
                    qo_joinTableOn({value: $g('qo_joinTable').val(d.joinTable || '').val()});
                    qo_manyTableOn({value: $g('qo_manyTable').val(d.manyTable || '').val()});
                    $gs(['qo_joinTableDiv', 'qo_joinFieldDiv', 'qo_manyTableDiv', 'qo_manyField0Div', 'qo_manyField1Div'], ['qo_sqlDiv','qo_fieldDiv', 'qo_tFieldDiv', 'qo_joinRefDiv']);
                }else if(d.joinType==='sql'){
                    $gs(['qo_sqlDiv'],['qo_operDiv','qo_tFieldDiv','qo_fieldDiv','qo_joinTableDiv', 'qo_joinRefDiv', 'qo_joinFieldDiv', 'qo_manyTableDiv', 'qo_manyField0Div', 'qo_manyField1Div']);
                }

                let h = builder.qo(d);
                $(h).insertBefore(t).addClass('jdiy-selected');
                t.remove();
                layui.form.render(null, canvas.attr('id'));
            },
            addNewId = 1,
            setOpacity = function (o, v) {
                if (o.hasClass('col')) {
                    let index = o.index() + 1;
                    canvas.find('.jdiy-ls-design-tb thead th:nth-child(' + index + ')').css({opacity: v});
                    canvas.find('.jdiy-ls-design-tb tbody td:nth-child(' + index + ')').css({opacity: v});
                } else {
                    o.css({opacity: v});
                }
                return o;
            },
            needChkbox = function (c) {
                let r = false, i, it;
                for (i = sData.topButtons.length; i--;) {
                    if ((it = sData.topButtons[i]).act === 'del' || it.act === 'update' || it.act === 'exec' || it.act === 'ajax' || it.act === 'page') {
                        r = true;
                        break;
                    }
                }
                if (!c) {
                    if (r) canvas.find('.lsChkBox_' + args.uid).show();
                    else canvas.find('.lsChkBox_' + args.uid).hide();
                }
                return r;
            },
            getLineCss = (t) => {
                if (t.hasClass('col')) {
                    let index = t.index() + 1,
                        o1 = canvas.find('.jdiy-ls-design-tb thead th:nth-child(' + index + ')'),
                        o2 = canvas.find('.jdiy-ls-design-tb tbody td:nth-child(' + index + ')');
                    return {
                        width: '3px',
                        height: (o1.outerHeight() + o2.outerHeight() + 2) + 'px',
                        top: (o1.offset().top) + 'px'
                    }
                } else {
                    return {width: '3px', height: t.outerHeight() + 'px', top: (t.offset().top) + 'px'}
                }
            },
            startDrag = function (e, dragThis) {
                body.addClass('drag-move');
                jdiyAdmin.lsDesign.dragging = true;
                jdiyAdmin.lsDesign.dragThis = setOpacity(dragThis, 0.1);
                jdiyAdmin.lsDesign.mouse = {x: e.clientX, y: e.clientY};
            },
            footerBtnShow = function (d) {
                if (d.footer === undefined || d.footer === null) d.footer = true;
                if (d.act === 'link' && d.target === 'dialog') {
                    $g('btn_footerDiv').show();
                    $g('btn_footer').prop('checked', d.footer);
                    layui.form.render(null, rtForm.attr('id'));
                }
            },
            save = function () {
                let selectedId = canvas.find('.jdiy-selected').attr('id');
                jdiyAdmin.ajax({
                    url: jdiyAdmin.ctx + '/mgmt/JDiyAdmin/list.saveDesign.' + args.uid,
                    data: {s: JSON.stringify(sData)},
                    type: 'post',
                    success: function (r) {
                        if (r.code !== 200) {
                            if (r.code === 203 && r.inputId) doSelect(r.inputId);
                            jdiyAdmin.error(r.msg);
                            return;
                        }
                        reset(null, selectedId);
                        layui.layer.msg(r.msg);
                    }
                });
            },
            addNew = function (t) {
                let id = jdiyAdmin.util.newId(),
                    oid = t.attr('id').substring(10),
                    aa = function (arr, d) {
                        let is = false, i;
                        for (i = arr.length; i--;) {
                            if (arr[i].id === oid) {
                                arr.splice(i, 0, d);
                                is = true;
                                break;
                            }
                        }
                        if (!is) arr.push(d);
                    },
                    createBtn = function (pos) {
                        let d = sBtns[pos][id] = {
                            id: id,
                            text: '新按钮' + addNewId++,
                            act: pos === 0 ? 'add' : 'edit'
                        }, arr = pos === 0 ? sData.topButtons : sData.rowButtons;
                        aa(arr, d);
                        doSelect($(builder.btn(d, pos)).insertBefore(t));
                        layui.layer.msg('[' + sBtns[pos][id].text + '] 已添加并选中，请在页面右侧配置其属性。');
                    };
                if (!t.length) return;
                if (t.hasClass('shobj')) {
                    doSelect($(builder.qo(sQos[id] = {
                        id: id,
                        type: 'text',
                        field: '',
                        label: '搜索' + addNewId++
                    })).insertBefore(t));
                    aa(sData.qos, sQos[id]);
                    layui.layer.msg('[' + sQos[id].label + '] 已添加并选中，请在页面右侧配置其属性。');
                } else if (t.hasClass('topbtn')) {
                    createBtn(0);
                    needChkbox();
                } else if (t.hasClass('rowbtn')) {
                    createBtn(1);
                } else if (t.hasClass('col')) {
                    let nd = {
                        id: id,
                        label: '新列' + addNewId++,
                        format: 'text',
                        align: 'center',
                        sortable: false
                    };
                    aa(sData.columns, nd);
                    builder.cols(sData.columns);
                    doSelect('jdiylsoth_' + id);
                }
            },
            remove = function (t) {
                let id = t.attr('id').substring(10), n, l, i, p = 'jdiylsobj_',
                    update = (a) => {
                        for (i = a.length; i--;) {
                            if (a[i].id === id) {
                                if (i < a.length - 1) n = a[i + 1];
                                else if (i > 0) n = a[i - 1];
                                else n = null;
                                a.splice(i, 1);
                                break;
                            }
                        }
                        return a;
                    };
                if (t.hasClass('col')) {
                    if (sData.columns.length < 2) {
                        layui.layer.msg('至少需要保留一列数据,再删就没有了！');
                        return;
                    }
                    builder.cols(update(sData.columns));
                    p = 'jdiylsoth_';
                } else if (t.hasClass('shobj')) {
                    l = sQos[id].label;
                    if (update(sData.qos).length < 1) builder.emptyQos();
                    else t.remove();
                } else if (t.hasClass('topbtn')) {
                    l = sBtns[0][id].text;
                    if (update(sData.topButtons).length < 1) builder.emptyTopBtn();
                    else t.remove();
                    needChkbox();
                } else if (t.hasClass('rowbtn')) {
                    l = sBtns[1][id].text;
                    if (update(sData.rowButtons).length < 1) builder.cols(sData.columns);
                    else t.remove();
                }
                if (n) doSelect(p + n.id);
                else propRender.nul();
                layui.layer.msg((l ? '[' + l + ']' : '目标对象') + '已从当前区域移除！');
            },
            toolHtml = '<div class="layui-btn-group jdiy-design-tool">\n' +
                '        <button type="button" class="layui-btn layui-btn-xs layui-bg-black add-tool" title="在它前面增加"><i class="layui-icon layui-icon-add-1"></i></button>\n' +
                '        <button type="button" class="layui-btn layui-btn-xs layui-bg-black del-tool" title="移除选中项"><i class="layui-icon layui-icon-delete"></i></button>\n' +
                '    </div>',
            showTool = function (t, isCol) {
                canvas.find('.jdiy-design-tool').remove();
                if (t) {
                    let tool = isCol
                        ? $(toolHtml).prependTo(t).css({
                            marginLeft: 0,
                            marginTop: (t.height()) + 'px'
                        })
                        : $(toolHtml).insertAfter(t).css({
                            marginLeft: Math.max(-65, -t.innerWidth()) + 'px',
                            marginTop: (t.outerHeight()) + 'px'
                        });
                    tool.find('.add-tool').click(function () {
                        let t = canvas.find('.jdiy-selected');
                        if (t.length > 1) t = t.filter('th');
                        addNew(t);
                    });
                    tool.find('.del-tool').click(function () {
                        let t = canvas.find('.jdiy-selected');
                        if (t.length > 1) t = t.filter('th');
                        showTool();
                        remove(t);
                    });
                }
            },
            doSelect = function (target) {
                let oThis = typeof target == 'string' ? $('#' + target) : target, o = oThis;
                if (oThis.is('.jdiy-ls-obj.col')) {
                    layui.element.tabChange('rtTab_' + args.uid, 'rtTab0_' + args.uid);
                    var index = o.index() + 1, otd;
                    canvas.find('.jdiy-ls-obj.jdiy-selected').removeClass('jdiy-selected');
                    canvas.find('.jdiy-ls-design-tb thead th:nth-child(' + index + ')').addClass('jdiy-selected');
                    otd = canvas.find('.jdiy-ls-design-tb tbody td:nth-child(' + index + ')').addClass('jdiy-selected');
                    $('.ls-tpl_' + args.uid).hide();
                    $gs(['ls_cols'], ['nulPropDiv']);
                    propRender.cols(sCols[o.attr('id').substring(10)]);
                    showTool(otd, true);
                } else if ((o = oThis).is('.jdiy-ls-obj.rowbtn') || (o = oThis.parents('.jdiy-ls-obj.rowbtn')).length === 1) {
                    layui.element.tabChange('rtTab_' + args.uid, 'rtTab0_' + args.uid);
                    canvas.find('.jdiy-ls-obj.jdiy-selected').removeClass('jdiy-selected');
                    o.addClass('jdiy-selected');
                    $('.ls-tpl_' + args.uid).hide();
                    $gs(['ls_btns'], ['nulPropDiv']);
                    propRender.btns(sBtns[1][o.attr('id').substring(10)], 1);
                    showTool(o);
                } else if ((o = oThis).is('.jdiy-ls-obj.topbtn') || (o = oThis.parents('.jdiy-ls-obj.topbtn')).length === 1) {
                    layui.element.tabChange('rtTab_' + args.uid, 'rtTab0_' + args.uid);
                    canvas.find('.jdiy-ls-obj.jdiy-selected').removeClass('jdiy-selected');
                    o.addClass('jdiy-selected');
                    $('.ls-tpl_' + args.uid).hide();
                    $gs(['ls_btns'], ['nulPropDiv']);
                    propRender.btns(sBtns[0][o.attr('id').substring(10)], 0);
                    showTool(o);
                } else if ((o = oThis).is('.jdiy-ls-obj.shobj') || (o = oThis.parents('.jdiy-ls-obj.shobj')).length === 1) {
                    layui.element.tabChange('rtTab_' + args.uid, 'rtTab0_' + args.uid);
                    canvas.find('.jdiy-ls-obj.jdiy-selected').removeClass('jdiy-selected');
                    o.addClass('jdiy-selected');
                    $('.ls-tpl_' + args.uid).hide();
                    $gs(['ls_qos'], ['nulPropDiv']);
                    propRender.qos(sQos[o.attr('id').substring(10)]);
                    showTool(o);
                } else if ((o = oThis).is('.jdiy-ls-obj.pager') || (o = oThis.parents('.jdiy-ls-obj.pager')).length === 1) {
                    layui.element.tabChange('rtTab_' + args.uid, 'rtTab1_' + args.uid);
                    canvas.find('.jdiy-ls-obj.jdiy-selected').removeClass('jdiy-selected');
                    o.addClass('jdiy-selected');
                    $gs([], ['nulPropDiv']);
                }
            },
            propRender = {
                nul: function () {
                    rtForm.find('.ls-tpl_' + args.uid).hide();
                    rtForm.find('#nulPropDiv_' + args.uid).show();
                    layui.element.tabChange('rtTab_' + args.uid, 'rtTab1_' + args.uid);
                },
                qos: function (qoit) {
                    $g('qoid').val(qoit.id);
                    let o = $g('qo_type');
                    try {
                        o.val(qoit.type || 'text');
                    } catch (e) {
                        o.val('text');
                    }
                    let hash = args.qoTplHash[qoit.type = o.val()];
                    if (hash) {
                        let sa = [];
                        for (let i = 0; i < hash.length; i++) sa.push('<option value="', hash[i].name, '"' +
                            (qoit.oper === hash[i].name ? ' selected' : '') + '>', hash[i].value, '</option>');
                        qoit.oper = $g('qo_oper').html(sa.join('')).val();
                    }

                    qo_joinTypeOn({value: $g('qo_joinType').val(qoit.joinType || '').val()});

                    $g('qo_sql').val(qoit.sql || '');
                    $g('qo_label').val(qoit.label || '');
                    $g('qo_placeholder').val(qoit.placeholder || '');

                    $g('qo_initial').val(qoit.initial || '');
                    $g('qo_width').val(qoit.width || '');
                    if (qoit.type === 'treeSelect') {
                        $gs(['qoTreeDiv', 'qo_placeholderDiv'], ['itemsDiv', 'optsDiv', 'qo_joinTableDiv']);
                        o = $g('qo_treeId');
                        try {
                            o.val(qoit.treeId || '');
                        } catch (e) {
                            o.val('');
                        }
                        o = $g('qo_treeDisplay');
                        try {
                            o.val(qoit.treeDisplay = qoit.treeDisplay || 'separator');
                        } catch (e) {
                            o.val('');
                        }
                        if (qoit.treeOnlyLeaf) {
                            $g('qo_treeOnlyLeaf').prop('checked', true);
                        } else {
                            $g('qo_treeOnlyLeaf').prop('checked', false);
                        }
                    } else if (qoit.type !== 'select') {
                        $gs(['qo_placeholderDiv'], ['itemsDiv', 'optsDiv', 'qoTreeDiv']);
                    } else {
                        $gs(['itemsDiv', 'optsDiv'], ['qo_placeholderDiv', 'qoTreeDiv']);
                        $g('items').val(qoit.items || '');
                        $g('optType').val(qoit.optType || '');
                        optTypeOn({value: qoit.optType || ''});
                    }
                    layui.form.render(null, rtForm.attr('id'));
                },
                cols: function (col) {
                    let o = $g('col_field'), val = col.field;
                    loadFields(args.bindType === 'entity', args.bindType === 'table' ? args.mainTable : args.mainEntity, function (r) {
                        fillOpts($g('col_field'), '请选择', r.data, val, (it) => [
                            it[0], it[0] + '　- ' + it[1]
                        ]);
                        $g('col_field').append('<option value="@">其它自定义</option>');
                        if (val && !o.find('option[value="' + val + '"]').length) {
                            o.val('@');
                        }

                        col_fieldsOn({value: o.val()});

                        $g('col_label').val(col.label || '');
                        o = $g('col_format');
                        try {
                            o.val(col.format || '');
                        } catch (e) {
                            o.val('');
                        }
                        if (col.format === 'img' || col.format === 'file') {
                            $gs([], ['col_alignDiv']);
                        } else {
                            $gs(['col_alignDiv']);
                        }
                        o = $g('col_align');
                        try {
                            o.val(col.align || 'center');
                        } catch (e) {
                            o.val('center');
                        }
                        $g('col_width').val(col.width || '');

                        if (col.sortable) {
                            $g('col_sortable').prop('checked', true);
                        } else {
                            $g('col_sortable').prop('checked', false);
                        }

                        rtForm.find('.coldivs').hide();
                        if (col.format === 'kv') {
                            $gs(['col_kvDiv']);
                            $g('col_kv').val(col.kv || '');
                        } else if (col.format === 'tpl') {
                            $gs(['col_tplDiv']);
                            $g('col_tpl').val(col.tpl || '')
                                .attr('placeholder', '支持ftl语法，例：\r\n\r\n<#if vo.state=1>\r\n ${vo.auser} 审核于：\r\n ${vo.atime?datetime}\r\n<#else>\r\n 未审核\r\n</#if>');
                        } else if (col.format === 'dict') {
                            $gs(['col_dictIdDiv']);
                            $g('col_dictId').val(col.dictId || '');
                        } else if (col.format === 'manyToOne' || col.format === 'manyToMany' || col.format === 'oneToMany') {
                            $gs(['col_outJoinDiv']);
                            o = $g('col_joinTable');
                            try {
                                o.val(col.joinTable || '');
                            } catch (e) {
                                o.val('');
                            }
                            col_joinTableOn({value: o.val()});
                            if (col.format === 'manyToOne') {
                                $gs([], ['col_manyTableDiv', 'col_manyField0Div', 'col_manyField1Div', 'col_joinRefDiv']);
                            } else if (col.format === 'manyToMany') {
                                $gs(['col_manyTableDiv', 'col_manyField0Div', 'col_manyField1Div'], ['col_joinRefDiv']);
                                $g('col_manyTable').val(col.manyTable || '');
                                col_manyTableOn({value: col.manyTable || ''});
                            } else if (col.format === 'oneToMany') {
                                $gs(['col_joinRefDiv'], ['col_manyTableDiv', 'col_manyField0Div', 'col_manyField1Div']);
                            }
                        } else if (col.format === 'tree') {
                            $gs(['col_treesDiv']);
                            o = $g('col_treeId');
                            try {
                                o.val(col.treeId || '');
                            } catch (e) {
                                o.val('');
                            }
                            o = $g('col_treeDisplay');
                            try {
                                o.val(col.treeDisplay = col.treeDisplay || 'separator');
                            } catch (e) {
                                o.val('');
                            }
                        }

                        layui.form.render(null, rtForm.attr('id'));
                    });
                },
                colOther: function () {
                    $g('ls_rowNum').prop('checked', !!sData.rowNum);
                    if (sData.mge === undefined || sData.mge) {
                        $g('ls_mge').prop('checked', true);
                        $gs(['ls_mgeWidthDiv']);
                    } else {
                        $g('ls_mge').prop('checked', false);
                        $gs([], ['ls_mgeWidthDiv']);
                    }
                    if (sData.excel) {
                        $g('excel').prop('checked', true);
                        $gs(['excelNameDiv', 'exportBtn']);
                        $g('excelName').val(sData.excelName);
                    } else {
                        $g('excel').prop('checked', false);
                        $gs([], ['excelNameDiv', 'exportBtn']);
                    }
                    $g('ls_mgeWidth').val(sData.mgeWidth === undefined ? '' : sData.mgeWidth);
                    $g('ls_tableHeight').val(!!sData.tableHeight ? sData.tableHeight:'');
                    $g('pageSize').val(sData.pageSize === undefined ? '' : sData.pageSize);
                    $g('defaultSort').val(sData.defaultSort || '');
                    $g('sqlFilter').val(sData.sqlFilter || '');
                    $g('pageHandler').val(sData.pageHandler || '');

                    $g('headTips').val(sData.headTips || '');
                    if (sData.headTips != null && sData.headTips !== '') {
                        $g('headTipsBlock').html(sData.headTips).show();
                    }
                    $g('footTips').val(sData.footTips || '');
                    if (sData.footTips != null && sData.footTips !== '') {
                        $g('footTipsBlock').html(sData.footTips).show();
                    }

                    layui.form.render(null, rtForm.attr('id'));
                },
                btns: function (btn, pos) {
                    $g('btn_text').val(btn.text || '');
                    $g('btn_icon').val(btn.icon || (btn.icon = ''));
                    let sa = [], ae = btn.act === 'add' || btn.act === 'edit',
                        types = btnTypes[pos], act = btn.act, i;
                    fillOpts($g('btn_act'), false, types, btn.act);
                    btn.act = $g('btn_act').val();

                    $(rtForm).find('.btnpls').hide();/*先把不同类型的都隐藏*/
                    if (act === 'add' || act === 'edit' || act === 'page' || act === 'view') {
                        $gs(['btn_pageIdDiv', 'btn_targetDiv', 'btn_titleDiv']);
                        if(pos===0 && btn.act==='edit'){
                            $gs([],['btn_targetDiv']);
                            btn.target='dialog';/*选中项弹窗批量更新*/
                            delete btn.confirm;
                        }
                        let wrapOne = (it) => '<option value="' + it.id + '"' + (it.id === btn.goPageId ? ' selected' : '') + '>' + it.name + '</option>';
                        if (ae) {
                            sa = [];
                            for (i = 0; i < args.pageList.length; i++) {
                                if (args.pageList[i].type !== 'form' || !args.pageList[i].isMainTable) continue;
                                sa.push(wrapOne(args.pageList[i]));
                            }
                        }else if (act==='view') {
                            sa = [];
                            for (i = 0; i < args.pageList.length; i++) {
                                if (args.pageList[i].type !== 'view' || !args.pageList[i].isMainTable) continue;
                                sa.push(wrapOne(args.pageList[i]));
                            }
                        } else {
                            sa = ['<option value="">=请选择=</option>'];
                            let inArr = [], lsArr = [], trArr = [],viArr=[];
                            for (i = 0; i < args.pageList.length; i++) {
                                let opt = wrapOne(args.pageList[i]), tpl = args.pageList[i].type;
                                if (tpl === 'form') inArr.push(opt);
                                else if (tpl === 'list') lsArr.push(opt);
                                else if (tpl === 'tree') trArr.push(opt);
                                else if (tpl === 'view') viArr.push(opt);
                            }
                            if (viArr.length) sa.push('<optgroup label="详情界面">', viArr.join('\n'), '</optgroup>');
                            if (inArr.length) sa.push('<optgroup label="表单界面">', inArr.join('\n'), '</optgroup>');
                            if (lsArr.length) sa.push('<optgroup label="列表界面">', lsArr.join('\n'), '</optgroup>');
                            if (trArr.length) sa.push('<optgroup label="树形界面">', trArr.join('\n'), '</optgroup>');
                        }
                        btn.goPageId = $g('btn_pageId').html(sa.join('')).val();

                        if (act === 'page'||act==='add'||act==='edit'||act==='view') {
                            $gs(['btn_pageParamDiv']);
                            $g('btn_pageParam').val(btn.pageParam || '').attr('placeholder', pos === 0 ? '格式：foo=bar&prm2=val' : '例：id=${vo.id}&foo=bar');
                        }

                        btn.target = fillOpts($g('btn_target'), false, [['dialog', '弹出对话框'], ['tab', '新Tab标签']], btn.target).val();
                        $g('btn_title').val(btn.title || '').attr('placeholder', pos === 0 ? '' : (act==='view'?'例：${vo.name}详情':'例：${vo.name}资料修改'));
                    } else if (act === 'link') {
                        $gs(['btn_outLinkDiv', 'btn_targetDiv']);
                        $g('btn_outLink').val(btn.outLink || '').attr('placeholder', pos === 0 ? '' : '例：${ctx}/xx?id=${vo.id}');
                        btn.target = fillOpts($g('btn_target'), false, [['dialog', '弹出对话框'], ['tab', '新Tab标签'], ['_blank', '浏览器新页面']], btn.target).val();

                        if (btn.target !== '_blank') {
                            $gs(['btn_titleDiv']);
                            $g('btn_title').val(btn.title || '').attr('placeholder', pos === 0 ? '' : '例：${vo.name}资料修改');
                        }
                    }else if (act === 'post') {
                        $gs(['btn_outLinkDiv'/*, 'btn_targetDiv'*/]);
                        $g('btn_outLink').val(btn.outLink || '').attr('placeholder', pos === 0 ? '' : '例：${ctx}/foo/bar');
                        btn.target = '_blank';
                    }
                    if (btn.act === 'update') {
                        $gs(['btn_updateCodeDiv']);
                        $g('btn_updateCode').val(btn.updateCode || '');
                    }
                    if (btn.act === 'del' || btn.act === 'update' || btn.act === 'exec' || btn.act === 'ajax') {
                        $gs(['btn_confirmDiv']);
                        $g('btn_confirm').val(btn.confirm || '');
                        if (btn.act === 'ajax') {
                            $gs(['btn_ajaxUrlDiv']);
                            $g('btn_ajaxUrl').val(btn.ajaxUrl || '');
                        }
                    }
                    if (btn.target === 'dialog'
                        && (btn.act === 'add' || btn.act === 'edit' || btn.act === 'view' || btn.act === 'page' || btn.act === 'link')) {
                        $gs(['btn_widthDiv', 'btn_heightDiv']);
                        $g('btn_width').val(btn.width || (btn.width = 640));
                        $g('btn_height').val(btn.height || (btn.height = 480));
                    }
                    if (pos === 1) {
                        $gs(['btn_dblDiv']);
                        $g('btn_dbl').prop('checked', !!btn.dbl);
                    }
                    $gs(['btn_grantCodeDiv', 'btn_conditionShowDiv']);
                    $g('btn_grantCode').val(btn.grantCode || '');
                    $g('btn_conditionShow').val(btn.conditionShow || '');
                    layui.colorpicker.render({
                        elem: '#btn_fg_' + args.uid
                        , color: btn.fg || ''
                        , predefine: true
                        , done: function (color) {
                            btn.fg = color;
                            var t = canvas.find('.jdiy-selected');
                            t.find('button').css({color: color});
                        }
                    });
                    layui.colorpicker.render({
                        elem: '#btn_bg_' + args.uid
                        , color: btn.bg || ''
                        , predefine: true
                        , done: function (color) {
                            btn.bg = color;
                            let t = canvas.find('.jdiy-selected');
                            t.find('button').css({backgroundColor: color});
                        }
                    });

                    footerBtnShow(btn)
                    layui.form.render(null, rtForm.attr('id'));
                }
            },
            reset = function (d, selectedId) {
                propRender.nul();
                let _done = function () {
                    builder.qos(sData.qos);
                    builder.btns(sData.topButtons, 0, 0);
                    builder.cols(sData.columns);
                    builder.totalSum(sData.columns);
                    layui.form.render(null, canvas.attr('id'));
                    propRender.colOther();
                    if (selectedId) doSelect(selectedId);
                };
                if (d) {
                    _done();
                } else {
                    jdiyAdmin.ajax({
                        url: jdiyAdmin.ctx + '/mgmt/JDiyAdmin/ui.meta?pageId=' + args.uid,
                        type: 'post',
                        success: function (r) {
                            if (r.code !== 200) {
                                jdiyAdmin.error(r.msg);
                                return;
                            }
                            sData = r.data;
                            _done();
                        }
                    });
                }

            },
            builder = {
                emptyQos: function () {
                    divQos.html('<div class="jdiy-ls-emptyQos">无搜索项,在实际应用中将不显示搜索区.　若需要增加搜索项，请<button class="layui-btn layui-btn-xs layui-btn-danger">点此添加</button> </div>');
                },
                emptyTopBtn: function () {
                    divBtn0.html('<div class="jdiy-ls-emptyBtn0">无表头控制按钮,在实际应用此行不会显示.　若需增加表头控制按钮，请<button class="layui-btn layui-btn-xs layui-btn-danger">点此添加</button> </div>');
                },
                qos: function (arr) {
                    if (arr && arr.length) {
                        let sa = [], i;
                        for (i = 0; i < arr.length; i++) {
                            sa.push(builder.qo(sQos[arr[i].id] = arr[i]));
                        }
                        sa.push('<div class="layui-inline"><button type="button" class="layui-btn layui-btn-normal layui-disabled">检索</button></div>');
                        sa.push('<div class="layui-inline" id="exportBtn_' + args.uid + '"><button type="button" id="export' + args.uid + '" style="padding:0 5px;" class="layui-btn layui-btn-primary"><i class="layui-icon layui-icon-table"></i>导出</button></div>');
                        divQos.html(sa.join('\n'));
                    } else {
                        builder.emptyQos();
                    }
                },
                qo: (qoit) => {
                    let isSelect = qoit.type === 'select' || qoit.type === 'treeSelect',
                        width = qoit.width && qoit.width === parseInt(qoit.width) && parseInt(qoit.width) > 20 ? ' style="width:' + qoit.width + 'px;"' : '',
                        v = qoit.field || '';
                    if (qoit.joinTable && qoit.joinField) v += '(' + qoit.joinTable + '.' + qoit.joinField + ')';
                    if(qoit.joinType==='sql') v='&lt;SQL&gt;';
                    let inp = isSelect
                        ? '<div class="layui-unselect layui-form-select"><div class="layui-select-title"><input type="text" placeholder="请选择" value="' + v + '" readonly="" class="layui-input layui-unselect"><i class="layui-edge"></i></div></div>'
                        : '<input type="text" class="layui-input layui-unselect" readonly value="' + v + '" title="' + v + '" placeholder="' + (qoit.placeholder || '') + '"/>';

                    return '<div class="layui-inline jdiy-ls-obj shobj" id="jdiylsobj_' + qoit.id + '">' +
                        (qoit.label === undefined || qoit.label === '' ? '' : '<label class="layui-form-mid">' + qoit.label + '</label>') +
                        '<div class="layui-input-inline"' + width + '>' + inp + '</div></div>';
                },
                btns: function (btns, type, pos) {
                    if (!btns || btns.length < 1) {
                        if (type === 0) {
                            builder.emptyTopBtn();
                        } else {
                            $g('ls_btn1Div').html('<div class="jdiy-ls-emptyBtn1">无,若需增加请<button class="layui-btn layui-btn-xs layui-btn-danger">点此添加</button> </div>');
                        }
                        return;
                    }
                    let sa = [], i;
                    for (i = 0; i < btns.length; i++) sa.push(builder.btn(sBtns[type][btns[i].id] = btns[i], pos));
                    $g('ls_btn' + type + 'Div').html(sa.join(''));
                },
                btn: (btn, pos) => {
                    let c1 = ['sm', 'xs'],
                        c2 = ['topbtn', 'rowbtn'],
                        text = btn.text || '',
                        icon = btn.icon
                            ? '<i class="layui-icon ' + btn.icon + '"></i>'
                            : '';
                    return '<div class="layui-inline jdiy-ls-obj ' + c2[pos] + '" id="jdiylsobj_' + btn.id
                        + '"><button style="' +
                        (btn.bg && btn.bg !== '' ? 'background-color:' + btn.bg + ';' : '') +
                        (btn.fg && btn.fg !== '' ? 'color:' + btn.fg + ';' : '') +
                        '" class="layui-btn layui-btn-' + c1[pos] + '" type="button">' + icon + text + '</button></div>'
                },
                cols: function (arr) {
                    let sa = [], i, nc = needChkbox(true);
                    sa.push('<thead><tr>');
                    sa.push('<th style="width:24px;' + (nc ? '' : 'display:none;') + '" class="lsChkBox_' + args.uid + '" title="当表格上方配置了批量处理按钮时，此打钩选择列自动显示"><i class="layui-icon layui-icon-ok layui-disabled" style="background-color:#5FB878;color:white;padding:1px;"></i></th>');
                    sa.push('<th class="lsRowNum_' + args.uid + '" style="cursor:not-allowed;width:32px;' + (sData.rowNum ? '' : 'display:none;') + '">行号</th>');
                    for (i = 0; i < arr.length; i++) {
                        sCols[arr[i].id] = arr[i];
                        sa.push('<th class="jdiy-ls-obj col" id="jdiylsoth_' + arr[i].id + '" style="width:' + (arr[i].width || '') + 'px;">' + arr[i].label +
                            '<span class="layui-table-sort layui-inline"' + (arr[i].sortable ? '' : ' style="display: none;"') +
                            '><i class="layui-edge layui-table-sort-asc" title="升序"></i><i class="layui-edge layui-table-sort-desc" title="降序"></i></span></th>');
                    }
                    sa.push('<th class="lsMge_' + args.uid + '"' + (sData.mgeWidth ? ' width="' + sData.mgeWidth + '"' : '') + ' style="cursor:not-allowed;' + (sData.mge ? '' : 'display:none;') + '">管理</th>');
                    sa.push('</tr></thead><tbody><tr>');

                    sa.push('<td style="' + (nc ? '' : 'display:none;') + '" class="lsChkBox_' + args.uid + '" title="当表格上方配置了批量处理按钮时，此打钩选择列自动显示"><i class="layui-icon layui-icon-ok layui-disabled" style="background-color:#5FB878;color:white;padding:1px;"></i></td>');
                    sa.push('<td class="lsRowNum_' + args.uid + '" style="cursor:not-allowed;' + (sData.rowNum ? '' : 'display:none;') + '">1</td>');
                    for (i = 0; i < arr.length; i++) {
                        let align = arr[i].align ? 'text-align:' + arr[i].align + ';' : '';
                        sa.push('<td class="jdiy-ls-obj col" id="jdiylsotd_' + arr[i].id + '" style="' + align + '">' + getColText(arr[i]) + '</td>');
                    }
                    sa.push('<td class="lsMge_' + args.uid + '" style="' + (sData.mge ? '' : 'display:none;') + '" id="ls_btn1Div_' + args.uid + '"></td></tr></tbody>');
                    $g('jdiy-ls-design-tb').html(sa.join(''));
                    builder.btns(sData.rowButtons, 1, 1);
                },
                totalSum: function (arr) {
                    let sa = [], i;
                    for (i = 0; i < arr.length; i++) {
                        if (arr[i].sum) sa.push(arr[i].label + '：<strong style="color:#c00;">${' + arr[i].field + '}</strong>');
                    }
                    $('#jdiy_design_sum_0' + args.uid).empty();
                    $('#jdiy_design_sum_1' + args.uid).empty();
                    $('#jdiy_design_sum_'+(sData.sumPos||0) + args.uid).html(sa.length ? '<div style="text-align: right;padding:10px 8px 5px 8px;font-size:15px;background-color: #f6f6f6;border-radius: 5px;">\n' +
                        '        <strong>汇总统计：　</strong>\n' + sa.join('　') + '</div>' : '');

                }
            };

        if (!body.data('jdiylsdrag')) {
            jdiyAdmin.lsDesign = {
                dragging: false,
                dragThis: null,
                dragThat: null,
                dragTo: null,
                mouse: null,
                pos: 0, /*1左2右*/
                line: $('<div class="jdiy-drag-line"></div>').appendTo(body),
                follow: $('<div class="jdiy-drag-follow">拖拽鼠标可调整当前元素位置</div>').appendTo(body)
            };
            body.data('jdiylsdrag', true);
            body.mousemove(function (evt) {
                let drag = jdiyAdmin.lsDesign;
                if (drag.dragging) {
                    drag.follow.css({
                        left: (evt.clientX + 30) + 'px',
                        top: (evt.clientY + 20) + 'px'
                    }).show();
                }
            }).mouseup(function () {
                $('body').removeClass('drag-move');
                $('.jdiy-drag-highlight').removeClass('jdiy-drag-highlight');
                let drag = jdiyAdmin.lsDesign || {};

                drag.follow.hide();
                drag.line.hide();
                drag.dragTo = null;
                drag.dragging = false;
            });
        }

        canvas.mousedown(function (e) {
            let dragThis = $(e.target),
                selected = canvas.find('.jdiy-selected'),
                isTool = dragThis.hasClass('jdiy-design-tool') || dragThis.parents('.jdiy-design-tool').length > 0;
            if (isTool) return;

            if (!dragThis.hasClass('jdiy-ls-obj')) dragThis = dragThis.parents('.jdiy-ls-obj');
            if (dragThis.is(selected)) {
                let isPager = dragThis.hasClass('pager') || dragThis.parents('.pager').length > 0;
                if (!isPager) {
                    startDrag(e, dragThis);
                    body.addClass('drag-move');
                }
                showTool(false);
            } else if (dragThis.length) {
                doSelect(dragThis);
            }
        }).mousemove(function (e) {
            let drag = jdiyAdmin.lsDesign || {};
            if (!drag.dragging) return;
            let t = $(e.target), c1 = null;
            if (!t.hasClass('jdiy-ls-obj')) t = t.parents('.jdiy-ls-obj');
            if (t.length < 1) return;
            if (drag.dragThis.hasClass('shobj')) c1 = 'shobj';
            else if (drag.dragThis.hasClass('topbtn')) c1 = 'topbtn';
            else if (drag.dragThis.hasClass('rowbtn')) c1 = 'rowbtn';
            else if (drag.dragThis.hasClass('col')) c1 = 'col';
            if (c1 === null || !t.hasClass(c1)) return;
            let that = drag.dragThat = t;

            if (drag.dragThat.is(drag.dragThis)) {
                drag.line.hide();
                drag.dragTo = null;
                drag.follow.html('拖拽鼠标可调整选中元素位置').css('color', 'red').show();
                canvas.find('.jdiy-drag-highlight').removeClass('jdiy-drag-highlight');
            } else {
                let ofX = e.clientX - drag.mouse.x,
                    lazy = 5,//移过指定像素，才判断显示目标位置
                    toob = that,
                    css1 = getLineCss(that);
                if (ofX > lazy) {//右
                    drag.pos = 2;
                    css1.left = (toob.offset().left + toob.outerWidth()) + 'px';
                } else if (ofX < -lazy) {//左
                    drag.pos = 1;
                    css1.left = (toob.offset().left) + 'px';
                } else {
                    css1 = null;
                }
                setOpacity(drag.dragThis, 0.1);
                if (css1 != null) {
                    canvas.find('.jdiy-drag-highlight').removeClass('jdiy-drag-highlight');
                    if (c1 !== 'col') toob.addClass('jdiy-drag-highlight');
                    drag.mouse = {x: e.clientX, y: e.clientY};
                    drag.dragTo = that;
                    drag.line.show().css(css1);
                    drag.follow.show().html('放开鼠标可将元素移至红线处').css('color', 'green');
                }
            }
        }).mouseup(function (e) {
            setTimeout(function () {
                let oThis = canvas.find('.jdiy-selected');
                if (oThis.hasClass('pager')) showTool();
                else if (oThis.length === 1) showTool(oThis);
                else if (oThis.length === 2) showTool(oThis.filter('td'), true)
            }, 200);

            let drag = jdiyAdmin.lsDesign || {};
            if (drag.dragThis) setOpacity(drag.dragThis, 1);
            if (drag.dragging && drag.dragTo) {
                let i, pos, it,
                    fid = drag.dragThis.attr('id').substring(10),
                    tid = drag.dragTo.attr('id').substring(10),
                    mva = function (a, d) {
                        for (i = a.length; i--;) {
                            pos = drag.pos === 1 ? i : i + 1;
                            if ((it = a[i]).id === fid) a.splice(i, 1);
                            else if (it.id === tid) a.splice(pos, 0, d[fid]);
                        }
                        return a;
                    },
                    mv = function () {
                        if (drag.pos === 1) drag.dragThis.insertBefore(drag.dragTo);
                        else drag.dragThis.insertAfter(drag.dragTo);
                    };

                if (drag.pos && drag.line.is(':visible') && fid !== tid) {
                    if (drag.dragThis.hasClass('col')) {
                        builder.cols(mva(sData.columns, sCols));
                        doSelect('jdiylsoth_' + fid);
                    } else if (drag.dragThis.hasClass('topbtn')) {
                        mva(sData.topButtons, sBtns[0]);
                        mv();
                    } else if (drag.dragThis.hasClass('rowbtn')) {
                        mva(sData.rowButtons, sBtns[1]);
                        mv();
                    } else if (drag.dragThis.hasClass('shobj')) {
                        mva(sData.qos, sQos);
                        mv();
                    }
                }
            } else {
                let o = $(e.target), id, label, ok = false;
                if (o.is('.jdiy-ls-emptyQos button')) {
                    ok = true;
                    id = jdiyAdmin.util.newId();
                    label = '新建搜索' + addNewId++;
                    sData.qos = [{
                        id: id,
                        type: 'text',
                        label: label
                    }];
                    builder.qos(sData.qos);
                } else if (o.is('.jdiy-ls-emptyBtn0 button')) {
                    ok = true;
                    id = jdiyAdmin.util.newId();
                    label = '新按钮' + addNewId++;
                    sData.topButtons = [{
                        id: id,
                        text: label,
                        act:'add'
                    }];
                    builder.btns(sData.topButtons, 0, 0);
                } else if (o.is('.jdiy-ls-emptyBtn1 button')) {
                    ok = true;
                    id = jdiyAdmin.util.newId();
                    label = '新按钮' + addNewId++;
                    sData.rowButtons = [{
                        id: id,
                        text: label,
                        act:'edit'
                    }];
                    builder.btns(sData.rowButtons, 1, 1);
                }
                if (ok) {
                    doSelect('jdiylsobj_' + id);
                    layui.layer.msg('[' + label + '] 已添加并选中，请在页面右侧配置其属性。');
                }
            }
        });


        selectOn('qo_type', function (o) {
            let {t, d} = $selQo();
            d.type = o.value;
            let h = builder.qo(d);
            $(h).insertBefore(t).addClass('jdiy-selected');
            t.remove();
            layui.form.render(null, canvas.attr('id'));
            propRender.qos(d);
        });
        selectOn('qo_treeId', function (o) {
            let {d} = $selQo();
            d.treeId = o.value;
        });
        switchOn('qo_treeOnlyLeaf', function (o) {
            let {d} = $selQo();
            d.treeOnlyLeaf = o.elem.checked;
        });
        selectOn('qo_treeDisplay', function (o) {
            let {d} = $selQo();
            d.treeDisplay = o.value;
        });
        $g('qo_sql').keyup(function () {
            let {d} = $selQo();
            d.sql = this.value;
        });
        $g('qo_label').keyup(function () {
            let {t, d} = $selQo();
            d.label = this.value;
            let h = builder.qo(d);
            $(h).insertBefore(t).addClass('jdiy-selected');
            t.remove();
            layui.form.render(null, canvas.attr('id'));
        });
        $g('qo_placeholder').keyup(function () {
            let {t, d} = $selQo();
            t.find('input').attr('placeholder', d.placeholder = this.value);
        });

        selectOn('qo_joinType', qo_joinTypeOn);
        selectOn('qo_joinTable', qo_joinTableOn);
        selectOn('qo_joinField', function (o) {
            let {d} = $selQo();
            d.joinField = o.value;
        });
        selectOn('qo_joinRef', function (o) {
            let {d} = $selQo();
            d.joinRef = o.value;
        });
        selectOn('qo_manyTable', qo_manyTableOn);
        selectOn('qo_manyField0', function (o) {
            let {d} = $selQo();
            d.manyField0 = o.value;
        });
        selectOn('qo_manyField1', function (o) {
            let {d} = $selQo();
            d.manyField1 = o.value;
        });
        selectOn('qo_field', qo_fieldsOn);

        $g('qo_tField').keyup(function () {
            let {t, d} = $selQo();
            if (this.value !== '' && !/^[a-zA-Z_][a-zA-Z0-9_]*$/.test(this.value)) {
                this.value = $(this).data('oldval');
            } else {
                $(this).data('oldval', this.value);
            }
            d.field = this.value;
            if (d.type !== 'select') {
                t.find('input').val(d.field + ' 自定义');
            } else {
                t.find('input.layui-input').val(d.field + ' 自定义');
            }
        });

        selectOn('qo_oper', function (o) {
            let {d} = $selQo();
            d.oper = o.value;
        });
        $g('qo_width').keyup(function () {
            let {t, d} = $selQo();
            d.width = this.value === '' ? 0 : parseInt(this.value) || 0;
            if (d.width !== 0 && d.width < 30) d.width = 30;
            if (d.width > 0) t.find('.layui-input-inline').css('width', d.width + 'px');
            else t.find('.layui-input-inline').removeAttr('style');
        });
        $g('qo_initial').keyup(function () {
            let {d} = $selQo();
            d.initial = this.value;
        });

        $g('items').keyup(function () {
            let {d} = $selQo();
            d.items = this.value;
        });

        selectOn('optType', optTypeOn);
        selectOn('optDict', function (o) {
            let {d} = $selQo();
            d.optDict = o.value;
        });
        selectOn('optEntity', function (o) {
            let {d} = $selQo();
            optTableOn({value: d.optEntity = o.value});
        });
        selectOn('optTable', function (o) {
            let {d} = $selQo();
            optTableOn({value: d.optTable = o.value});
        });
        selectOn('optTxtField', function (o) {
            let {d} = $selQo();
            d.optTxtField = o.value;
        });
        selectOn('optValField', function (o) {
            let {d} = $selQo();
            d.optValField = o.value;
        });
        $g('optWhere').keyup(function () {
            let {d} = $selQo();
            d.optWhere = this.value;
        });
        $g('optSort').keyup(function () {
            let {d} = $selQo();
            d.optSort = this.value;
        });

        $g('optQry').keyup(function () {
            let {d} = $selQo();
            d.optQry = this.value;
        });
        switchOn('cascade', function (o) {
            let {t, d} = $selQo();
            d.cascade = !!o.elem.checked;
        });

        $g('btn_text').keyup(function () {
            let {t, pos, d} = $selBtn();
            d.text = this.value;
            $(builder.btn(d, pos)).insertBefore(t).addClass('jdiy-selected');
            t.remove();
        });

        $g('btn_icon').click(function () {
            layui.layer.open({
                type: 8,
                title: '选择图标',
                area: ['480px', '340px'],
                content: jdiyAdmin.ctx + '/mgmt/JDiyAdmin/ui.icon?fn=lsChooseIcon_' + args.uid,
                btn: ['确定', '取消'],
                yes: function (index, layero) {
                    layero.find("button.submit").click();
                }
            });
            $(this).data('btnid', canvas.find('.jdiy-selected').attr('id'));
        }).change(function () {
            let id = $(this).data('btnid'),
                btnid = id.substring(10),
                t = $('#' + id),
                pos = t.hasClass('topbtn') ? 0 : 1,
                d = sBtns[pos][btnid];
            d.icon = this.value;
            $(builder.btn(d, pos)).insertBefore(t).addClass('jdiy-selected');
            t.remove();
        });
        selectOn('btn_act', function (o) {
            let {pos, d} = $selBtn();
            d.act = o.value;
            propRender.btns(d, pos);
            if (pos === 0) needChkbox();
        });

        $g('btn_updateCode').keyup(function () {
            let {d} = $selBtn();
            d.updateCode = this.value;
        });
        selectOn('btn_pageId', function (o) {
            let {d} = $selBtn();
            d.goPageId = o.value;
        });
        $g('btn_pageParam').keyup(function () {
            let {d} = $selBtn();
            d.pageParam = this.value;
        });
        $g('btn_outLink').keyup(function () {
            let {d} = $selBtn();
            d.outLink = this.value;
        });
        selectOn('btn_target', function (o) {
            let {pos, d} = $selBtn();
            d.target = o.value;
            propRender.btns(d, pos);
        });

        $g('btn_title').keyup(function () {
            let {d} = $selBtn();
            d.title = this.value;
        });

        $g('btn_width').keyup(function () {
            let {d} = $selBtn();
            d.width = this.value;
        });

        $g('btn_height').keyup(function () {
            let {d} = $selBtn();
            d.height = this.value;
        });

        $g('btn_confirm').keyup(function () {
            let {d} = $selBtn();
            d.confirm = this.value;
        });
        $g('btn_ajaxUrl').keyup(function () {
            let {d} = $selBtn();
            d.ajaxUrl = this.value;
        });
        $g('btn_grantCode').keyup(function () {
            let {d} = $selBtn();
            d.grantCode = this.value;
        });
        $g('btn_conditionShow').keyup(function () {
            let {d} = $selBtn();
            d.conditionShow = this.value;
        });
        switchOn('btn_dbl', function (o) {
            let {d} = $selBtn();
            for (let i = sData.rowButtons.length; i--;) sData.rowButtons[i].dbl = false;
            d.dbl = o.elem.checked;
        });
        switchOn('btn_footer', function (o) {
            let {d} = $selBtn();
            d.footer = o.elem.checked;
        });


        selectOn('col_field', col_fieldsOn);
        $g('col_label').keyup(function () {
            let {d} = $selCol();
            $('#jdiylsoth_' + d.id).text(d.label = this.value);
        });

        selectOn('col_format', function (o) {
            let {d} = $selCol();
            d.format = o.value;
            propRender.cols(d);
        });

        $g('col_kv').keyup(function () {
            let {d} = $selCol();
            d.kv = this.value;
        });
        $g('col_tpl').keyup(function () {
            let {d} = $selCol();
            d.tpl = this.value;
        });

        selectOn('col_dictId', function (o) {
            let {d} = $selCol();
            d.dictId = o.value;
        });


        selectOn('col_joinTable', col_joinTableOn);
        selectOn('col_joinField', function (o) {
            let {d} = $selCol();
            d.joinField = o.value;
            $('#jdiylsotd_' + d.id).html(getColText(d));
        });
        selectOn('col_joinRef', function (o) {
            let {d} = $selCol();
            d.joinRef = o.value;
        });
        selectOn('col_manyTable', col_manyTableOn);
        selectOn('col_manyField0', function (o) {
            let {d} = $selCol();
            d.manyField0 = o.value;
        });
        selectOn('col_manyField1', function (o) {
            let {d} = $selCol();
            d.manyField1 = o.value;
        });

        selectOn('col_treeId', function (o) {
            let {d} = $selCol();
            d.treeId = o.value;
        });
        selectOn('col_treeDisplay', function (o) {
            let {d} = $selCol();
            d.treeDisplay = o.value;
        });

        selectOn('col_align', function (o) {
            let {d} = $selCol();
            d.align = o.value;
            $('#jdiylsotd_' + d.id).css({textAlign: d.align});
        });
        $g('col_tField').keyup(function () {
            let {d} = $selCol();
            if (this.value !== '' && !/^[a-zA-Z_][a-zA-Z0-9_]*$/.test(this.value)) {
                this.value = $(this).data('oldval') || '';
            } else {
                $(this).data('oldval', this.value);
            }
            d.field = this.value;
            $('#jdiylsotd_' + d.id).html(getColText(d));
        });
        $g('col_width').keyup(function () {
            let {d} = $selCol(),
                id = d.id,
                ow = d.width;
            if (this.value !== '') {
                d.width = this.value = parseInt(this.value) || ow || '';
            } else {
                d.width = 0;
            }
            $('#jdiylsoth_' + id).css('width', d.width || '');
        });
        switchOn('col_sortable', function (o) {
            let {d} = $selCol();
            d.sortable = o.elem.checked;
            if (d.sortable) $('#jdiylsoth_' + d.id).find('.layui-table-sort').show();
            else $('#jdiylsoth_' + d.id).find('.layui-table-sort').hide();
        });
        switchOn('col_sum', function (o) {
            let {d} = $selCol();
            if(!!(d.sum = o.elem.checked)) {
                $gs(['col_sumPosDiv']);
            }else{
                $gs([],['col_sumPosDiv']);
            }
            builder.totalSum(sData.columns);
        });
        selectOn('col_sumPos', function (o) {
            sData.sumPos=o.value
            builder.totalSum(sData.columns);
        });
        switchOn('ls_mge', function (o) {
            if (sData.mge = o.elem.checked) {
                canvas.find('.lsMge_' + args.uid).show();
                $gs(['ls_mgeWidthDiv']);
            } else {
                canvas.find('.lsMge_' + args.uid).hide();
                $gs([], ['ls_mgeWidthDiv']);
            }
        });
        switchOn('ls_rowNum', function (o) {
            if (sData.rowNum = o.elem.checked) {
                canvas.find('.lsRowNum_' + args.uid).show();
            } else {
                canvas.find('.lsRowNum_' + args.uid).hide();
            }
        });
        switchOn('excel', function (o) {
            if (sData.excel = o.elem.checked) {
                $gs(['exportBtn', 'excelNameDiv']);
                $g('excelName').val(sData.excelName);
            } else {
                $gs([], ['exportBtn', 'excelNameDiv']);
            }
        });
        $g('excelName').keyup(function () {
            let v = this.value;
            if (v !== '') {
                v = v.replace(/["'\\\/\s<>]/ig, "");
            }
            sData.excelName = this.value = v;
        });

        $g('ls_tableHeight').keyup(function () {
            sData.tableHeight = this.value=this.value.trim().toLowerCase();
        });
        $g('ls_mgeWidth').keyup(function () {
            let ov = sData.mgeWidth, v = this.value;
            if (v !== '') {
                if (v != parseInt(v)) v = ov;
                if (v < 0) v = 0;
            }
            sData.mgeWidth = this.value = v;
            builder.cols(sData.columns);
        });
        $g('pageSize').blur(function () {
            let v;
            if (this.value === '') v = 15;
            else if (this.value == parseInt(this.value)) v = parseInt(this.value);
            else v = 15;
            if (v <= 0) v = 15;
            sData.pageSize = v;
            if (v !== 0) this.value = v;
        });
        $g('defaultSort').blur(function () {
            sData.defaultSort = this.value;
        });
        $g('sqlFilter').blur(function () {
            sData.sqlFilter = this.value;
        });
        selectOn('pageHandler', function (o) {
            sData.pageHandler = o.value;
        });

        $g('lsDesignReset').click(function () {
            layui.layer.confirm('此操作将撤销您对本页面所做的所有更改，确定要这样做吗？', function (index) {
                layui.layer.close(index);
                reset();
                layui.layer.msg('已撤销还原!');
            });
        });

        $g('lsDesignSave').click(function () {
            jdiyAdmin.confirm('确定要保存列表配置信息吗？保存后将不可撤消。', save);
        });

        $g('headTips').keyup(function () {
            if (this.value !== '') {
                $g('headTipsBlock').html(this.value).show();
            } else {
                $g('headTipsBlock').html('').hide();
            }
            sData.headTips = $g('headTipsBlock').html();
        });
        $g('footTips').keyup(function () {
            if (this.value !== '') {
                $g('footTipsBlock').html(this.value).show();
            } else {
                $g('footTipsBlock').html('').hide();
            }
            sData.footTips = $g('footTipsBlock').html();
        });
        $('.pageTips').click(function () {
            canvas.find('.jdiy-selected').removeClass('jdiy-selected');
            layui.element.tabChange('rtTab_' + args.uid, 'rtTab2_' + args.uid);
        });

        $(rtForm).find('.jdiy-form-help').click(function () {
            let ui = 'list', help = $(this).data('help'),
                helpMe = (h) => typeof h == 'object' ? layer.open(h) : layer.tips(h, '.jdev-prop-' + help + args.uid, {
                    tips: [4, '#FF5722'],
                    time: 10000
                });
            help && layui.jdhp && layui.jdhp[ui] && layui.jdhp[ui][help] &&
            helpMe(
                typeof layui.jdhp[ui][help] == 'function'
                    ? layui.jdhp[ui][help](args.uid)
                    : layui.jdhp[ui][help]
            );
        });
        reset();
    }

    exports('listDesign', {
        render: design_ls_main
    });
});