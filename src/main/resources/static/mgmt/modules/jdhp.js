layui.define([], function (exports) {
    const hh_global1 = {
            area: ["500px", "330px"],
            title: "配置帮助",
            content: "<blockquote class='layui-elem-quote' style='color:#c00;'>通过此控件输入内容，到其它表(或实体),根据查询字段查出记录，并将该记录的id值保存到当前控件绑定的字段中(也就是外键)，这叫外键反查。</blockquote><br/>" +
                "使用场景举例：<br/>页面上有个输入框输入用户名，提交保存时，程序从用户表中查询到目标用户，并把该用户的id值保存到当前表的“user_id”列中。"
        }, hh_global2 = {
            area: ["640px", "520px"],
            title: "配置帮助",
            content: "<blockquote class='layui-elem-quote' style='color:#c00;'>当前绑定的字段作为(另一个表的)外键，" +
                "拿输入内容到外部表中匹配相关记录，做left join连接查询。</blockquote>" +
                "<strong>使用场景举例：</strong><br/>假如当前列表界面是产品(goods)管理，每个产品都由某个供应商(provider)生产;" +
                "因此我们在goods表中用一个名为provider_id的外键字段来记录所属供应商。" +
                "若我们想按供应商(provider.name)查找产品，但由于供应商非常多（几百上千），" +
                "我们不可能用一个下拉菜单列出所有的供应商让用户去选（通常的情况是用一个文本框让用户输入供应商名进行检索）；" +
                "此时我们就需要用到外联表查询。<br/><br/>" +
                "<strong>配置方式：</strong><br/>1.当前搜索控件绑定为provider_id字段;<br/>" +
                "2.查询类型选“多对一（外键反查）”；<br/>" +
                "3.外联表选provider表,外联查询字段选provider表的name字段。<br/><br/>配置完成，您Get到了吗～～"
        },
        hh_layout = {
            area: ["620px", "470px"],
            title: "配置帮助",
            content: "<blockquote class='layui-elem-quote' style='color:#c00;'>定义控件在表单界面中的显示方式。 </blockquote>" +
                "<ul>\n" +
                "<li style=\"margin-bottom:10px;\"><strong>列布局：</strong><br/>控件自身宽度固定，同一行放不下时会自动转行显示并与其它行的控件保持列对齐，适用于在新Tab标签页打开的界面使用；\n" +
                "</li>\n" +
                "<li style=\"margin-bottom:10px;\"><strong>栅格化：</strong><br/>控件按比例放置于行内，会随界面宽度改变自动弹性缩放，适用于弹出框口界面使用；\n" +
                "</li>\n" +
                "<li style=\"margin-bottom:10px;\"><strong>自定义：</strong><br/>占位宽度会随标题文字后移，并可由开发者自定义输入控件宽度，不考虑与其它控件对齐。适用于系统属性配置等特殊界面使用。\n" +
                "</li>\n" +
                "<li style='color: gray;'><br/>部分控件（如富文本编辑器/附件上传）的布局样式由系统控制，不可更改。</li> </ul>" +
                ""
        },
        man = {
            form: {
                label: "设置控件前面显示的文字内容",
                field: "此控件要绑定更新的字段名. ",
                tField: "自定义一个非直接存库的字段名称，可用于附件上传，或使用表单增强器进行编程处理等高级特性。",
                type: "设置此控件的类型",
                treeId: "若此处无可选项，需要您预先配置一个树界面。当用户选中树形下拉菜中某个条目项，系统会自动将该条目的id值存入当前绑定字段。",
                treeDisplay: "设置当用户选中树形下拉菜单某个节点后，显示的(选中项)层级文字样式",
                treeOnlyLeaf: "设置树节点有子级时，该父节点是否允许被选择。",
                layout: hh_layout,
                layoutN: hh_layout,
                lay_width: "自定义控件的显示宽度",
                lookupId: "从目标界面查找并将选定数据行的[主键id]和[显示字段]带回到当前表单",
                lookupWH: "设置查找带回弹窗显示大小",
                lookupField: "设置目标界面查找带回后，要显示的字段",
                lookupMulti: "指示查找带回是否可多选<br/>若允许多选，还可设置一对多或多对多级联更新",
                lookupType: {
                    area: ["720px", "520px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>定义查找带回保存方式。目前支持以下三种(假定用户选择了三项，其id值分别为：id1,id2,id3)：</blockquote><ul>" +
                        "<li><strong>１．本表字段直接存储：</strong><br/>　　系统会将选中项的id直接存入当前控件绑定的字段上，格式：'id1','id2','id3'</li>" +
                        "<li><strong>２．一对多外键反向更新：</strong><br/>　　此种方式，当前表为主表(即作为OneToMany中的One)；查找界面数据来源表为外部表(Many),我们在Many表用一个外键字段xx_id来记录当前主表的id值。" +
                        "此配置方式在表单提交保存时，只会更新Many表的xx_id字段,即更新选中项(外部表)的xx_id=当前主表记录的id值；若移除选中项，则更新其xx_id=null</li>" +
                        "<li><strong>３．多对多关系表更新：</strong><br/>　　此种方式，当前表和外部表是互相多对多的映射关系(ManyToMany)，" +
                        "在表设计上，我们通常额外创建一张中间表(ref_xx_yy)来记录多对多关系，ref_xx_yy表只有两个字段(xx_id,yy_id),分别指向当前主表xx的id(正向关联字段),和外部表yy的id（反向关联字段）；" +
                        "此配置方式在表单提交时，系统会自动更新这个中间表ref_xx_yy的相关记录，完成ManyToMany关系映射。</li></ul>" +
                        "<br/><div style=\"color:red\">如果您还不明白如上所述内容，您应该去恶补一下关系数据库的基本知识了～～</div>"
                },
                lookupJoinField: "指定查找带回数据表的外键字段名",
                lookupJoinTable: "指定多对多中间关系表<br/>该表只有两个字段分别关联主/外表的id",
                lookupJoinField0: "定义多对多中间关系表的正向关联字段（指向本表的id）",
                lookupJoinField1: "定义多对多中间关系表的反向关联字段（指向选中项外部表的id）",

                manyType: {
                    area: ["720px", "520px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>定义多选框的数据更新方式。目前支持以下三种(假定用户钩选了三项，其id值分别为：id1,id2,id3)：</blockquote><ul>" +
                        "<li><strong>１．本表字段直接存储：</strong><br/>　　系统会将选中项的id值直接存入当前控件绑定的字段上，格式：'id1','id2','id3'</li>" +
                        "<li><strong>２．一对多外键反向更新：</strong><br/>　　此种方式，当前表为主表(即作为OneToMany中的One)；控件的选项数据来源表为外部表(Many),我们在Many表用一个外键字段xx_id来记录当前主表的id值。" +
                        "此配置方式在表单提交保存时，只会更新Many表的xx_id字段,若条目被钩选，则更新外部表的xx_id=当前主表记录的id值；若取消钩选就设置外部表xx_id=null</li>" +
                        "<li><strong>３．多对多关系表更新：</strong><br/>　　此种方式，当前表和外部表是互相多对多的映射关系(ManyToMany)，" +
                        "在表设计上，我们通常额外创建一张中间表(ref_xx_yy)来记录多对多关系，ref_xx_yy表只有两个字段(xx_id,yy_id),分别指向当前主表xx的id(正向关联字段),和外部表yy的id（反向关联字段）；" +
                        "此配置方式在表单提交时，系统会自动更新这个中间表ref_xx_yy的相关记录，完成ManyToMany关系映射。</li></ul>" +
                        "<br/><div style=\"color:red\">如果您还不明白如上所述内容，您应该去恶补一下关系数据库的基本知识了～～</div>"
                },
                manyTable: "指定多对多中间关系表<br/>该表只有两个字段分别关联主/外表的id",
                manyField0: "定义多对多中间关系表的正向关联字段（指向本表的id）",
                manyField1: "定义多对多中间关系表的反向关联字段（指向选项条目外部表的id）",
                format: function (uid) {
                    return $('#format_' + uid).val() === 'joinEntity' || $('#format_' + uid).val() === 'joinTable'
                        ? hh_global1
                        : "定义此控件的输入内容类型.系统会适当限制输入内容格式或输入方式．"
                },
                pwdType: "设置之后若产生业务数据，请勿再修改加密方式，以免加密方式前后不一致导致相关业务模块密码验证失败",
                joinTable: hh_global1,
                joinEntity: hh_global1,
                joinField: hh_global1,
                fileMultipart: {
                    area: ["600px", "280px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>定义文件上传行为：<br/><ul>" +
                        "<li>１．单文件上传：只能上传单个文件，且再次上传时，先把之前上传的文件删除．</li>" +
                        "<li>２．多文件上传：一次可上传多个文件，且再次上传时，之前上传的文件仍然保留．</li></ul></blockquote>"
                },
                fileDel: "是否允许删除已传的附件，多文件上传模式建议开启",
                fileType: "限制上传文件的类型",
                fileExtensions: '限定可以上传的文件扩展名,多个扩展名用半角逗号隔开',
                room: "指示上传的图片，是否进行缩放处理，以及如何缩放.",
                roomTo: "宽和高至少填写一项(不为0)。若只填了宽(高为0),表示只按宽度进行缩放，只填高同理.",
                fileSize: "限制上传文件的字节大小（单位为KB）,不填或填0表示由服务器默认",
                renameType: {
                    area: ["640px", "380px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>定义上传的附件的文件重命名方式：<br/><ul>" +
                        "<li><strong style='color:red'>自动</strong>：由JDiy系统决定是否对上传的文件进行重命名。</li>" +
                        "<li><strong style='color:red'>保持原文件名</strong>：即不改名（与上传前用户端电脑上的文件名一致，如果是多文件上传模式，当前字段下的同名文件将被覆盖）．</li>" +
                        "<li><strong style='color:red'>自定义文件名</strong>：即自己定义一个文件名称，附件上传后将按用户输入的名称来更名保存．</li>" +
                        "</ul><br/>作用：对于普通的显示类附件（如图片），通常对上传的文件命名称无要求，此功能主要用于那些需要提供给用户下载的附件，对下载时的文件显示名称有一定要求时，我们在配置上传附件时就对文件进行重命名配置显得尤为重要。</blockquote>"
                },fileName: {
                    area: ["680px", "360px"],
                    title: "配置帮助",
                    content: '<blockquote class="layui-elem-quote">自定义附件上传后的文件名，注意不要写扩展名。<br/>'+
                    '若空着不填,上传的附件将被改名为<strong style=\'color:red\'>绑定的字段名.扩展名</strong>, 如：<strong style=\'color:red\'>userPhoto.jpg</strong><br/>'+
                    '若填写，例如填<strong style=\'color:red\'>合同</strong>，则当用户上传一个doc格式的文件时,文件将被重命名为：<strong style=\'color:red\'>合同.doc</strong><br/>'+
                    '支持ftl表达式. 如填：<strong style=\'color:red\'>${vo.id}</strong>, 则文件被重命名为该表单数据的 <strong style=\'color:red\'>id值+扩展名</strong> 如id=5时，用户上传一个jpg，文件名为<strong style=\'color:red\'>5.jpg</strong><br/>'+
                    '如果是多文件上传模式，如填<strong style=\'color:red\'>foo</strong>, 用户上传3个jpg文件时，文件名依次为：</br>'+
                    '<strong style=\'color:red\'>foo.jpg foo_1.jpg foo_2.jpg</strong>　即自定义文件名称后面加上顺序号</blockquote>'
                },
                required: "限制此控件是否为必填项",
                noStore: {
                    area: ["640px", "330px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>定义上传附件与当前主表记录的关联持久化方式：<br/><ul>" +
                        "<li><strong style='color:red'>１．Store对象存储（推荐）</strong>：此模式下，业务数据表中无需额外定义字段来存储上传附件的路径（即控件绑定字段可以是“自定义”的名称，它与业务数据表松偶合）。</li>" +
                        "<li><strong style='color:red'>２．仅存URL到绑定字段</strong>：此模式下，系统将上传附件的URL访问路径直接存入到控件[绑定字段]中（绑定字段必须在物理数据表中真实存在，且最好定义为text类型），若控件允许多文件上传，则多个上传文件路径以竖线(|)分隔后存入绑定字段．</li></ul></blockquote>"
                },
                maxlength: "限制输入内容的最大字符长度<br/>空着不填或者填0表示不限制",
                height: "设置此控件的显示高度，对于多行文本框，显示高度需介于60-600之间",
                placeholder: "设置控件的placeholder属性<br/>即当控件无输入内容时，显示的提示信息",
                tips: "在控件的后面显示一段提示文字(支持HTML,仅适用于非栅格化布局的控件)",
                ont: "设置开关的显示文字。<br/>格式：A|B <br/>A为打开时显示的文字<br/>B为关闭时显示的文字",
                onv: "设置开关的value值。<br/>格式：A|B <br/>A为打开时的值<br/>B为关闭时的值 ",
                search: "当下拉菜单条目项较多时，可开启“带搜索”，允许通过输入来快速定位到目标选项",
                items: "静态选项条目(一行一个).<br/>格式：A:B <br/>A为条目值，B为条目显示文字.<br/>例如下拉菜单控件第一项是[请选择]，则首行填入：<br/>:=请选择=",
                optType: '指定控件的选项条目数据来源',
                optEntity: "为控件增加动态条目，指定条目项的数据来源JPA实体",
                optTable: "为控件增加动态条目，指定条目项的数据来源表",
                optTxtField: "为控件增加动态条目,指定条目项的文字显示字段",
                optValField: "为控件增加动态条目,指定条目项的值字段",
                optWhere: {
                    area: ["720px", "460px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>从来源表（或实体）查询数据时，附加过滤条件.用<strong style='color:#860fc6'>o</strong>表示当前表记录或实体.<br/>" +
                        "例如： o.tid=3 and o.user_id='${user.id}'" +
                        "</blockquote>查询条件支持内嵌EL或FTL表达式(见如下内置变量或示例)<br/><ol>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前用户id:  <span style='color:#860fc6'>${user.id}</span>  又如取登录帐号： <span style='color:#860fc6'>${user.uid}</span></li>" +
                        "<li><strong>qo</strong> 表示界面请求入参.如: <span style='color:#860fc6'>${qo.foobar}</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>${date}</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>${datetime}</span></li>" +
                        "<li><strong>其它任意freemarker内置表达式甚至标签语句</strong> <br/>" +
                        "　　例如: <span style='color:#860fc6'>${.now?string('yyyy-MM')}</span></li>" +
                        "　　又如: <span style='color:#860fc6'>&lt;#if qo.pid??&gt; and o.pid='${qo.pid}'&lt;/#if&gt;</span></li>" +
                        "</ol>"
                },
                optSort: "从来源表（或实体）查询时，指定排序方式,将以此顺序添加到到当前控件中.<br/>例如：o.id desc",
                optDict: "从数据字典拉取数据到当前控件，<br/>字典key作为选项文字<br/>字典value作为选项的值",
                optQlType: '通过SQL(或JQL)语句查询出结果作为控件的条目项．<br/>此处用于指定查询语句的类型．',
                optQry: {
                    area: ["720px", "460px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>通过SQL(或JQL)语句查询出结果作为控件的条目项." +
                        "<br/>查询结果第1例作为选项值，第2例作为显示文字(若只有1列,值和文字都使用这1列数据).<br/>" +
                        "例如(SQL)： select id,name from aaa o where o.bb='cc' and o.user_id='${user.id}' order by o.dd asc<br/>" +
                        "又如(JQL)： select o.id,o.name from Aaa o where o.bb='cc' and o.user.id='${user.id}' order by o.dd asc<br/>" +
                        "</blockquote>查询条件支持内嵌EL或FTL表达式(见如下内置变量或示例)<br/><ol>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前用户id:  <span style='color:#860fc6'>${user.id}</span>  又如取登录帐号： <span style='color:#860fc6'>${user.uid}</span></li>" +
                        "<li><strong>qo</strong> 表示界面请求入参.如: <span style='color:#860fc6'>${qo.foobar}</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>${date}</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>${datetime}</span></li>" +
                        "<li><strong>其它任意freemarker内置表达式甚至标签语句</strong> <br/>" +
                        "　　例如: <span style='color:#860fc6'>${.now?string('yyyy-MM')}</span></li>" +
                        "　　又如: <span style='color:#860fc6'>&lt;#if qo.pid??&gt; and o.pid='${qo.pid}'&lt;/#if&gt;</span></li>" +
                        "</ol>"
                },
                cascade: {
                    area: ["760px", "480px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>指示当前控件的条目选项，会随着其它控件的选项值选中改变，联动更新.</br></br>" +
                        "常见的业务场景如省市地区菜单级联，当选择“省”后，“市”的下拉菜单条目项会联动更新为该省下的数据。" +
                        "此业务场景下，省作为父级菜单，市作为子级菜单，我们为“市”这个子级菜单增加附加查询条件，并将级联选项「开启」即可。" +
                        "" +
                        "</blockquote>要使子级菜单的选项联动，需满足如下3个前置条件：<br/><ul>" +
                        "<li>1.父级控件类型必须是select(下拉菜单),radio(单选),lookup(查找带回) 之一</li>" +
                        "<li>2.子级控件类型必须是select|radio|checkbox之一</li>" +
                        "<li>3.子级控件配置了非字典类型的“动态条目”，且动态条的查询条件(或sql/jql语句)中，包含 ${vo.XXX}这样的内容（XXX为级联触发的父级控件字段名）</li>" +
                        "</ul>例如省市级联菜单，假如有两个表(province,city)分别存储了省和市的数据，且" +
                        "city表中有一个名为province_id的外键字段，记录它所属的省份。我们在界面中分别配置（省和市）两个下拉菜单，然后" +
                        "配置“市”菜单的动态选项附加查询条件为： province_id='${vo.province_id}' 并为市菜单开启级联选项即可。"
                },
                minChk: "限定最少须要打钩选择多少个条目项才允许提交",
                maxChk: "限定最多只能打钩选择多少个条目项才允许提交",
                initial: "仅针对数据[添加]表单有效，设置此控件的默认值",
                readonly: "保留控件样式,禁止用户更改(可配合设置控件初始值使用)",
                pattern: "对用户输入的内容进行正则表达式校验，若校验不通过，不允许提交保存．<br/>例如： ^[a-zA-Z0-9]+$  <br/>(限定只能由字母或数字组成)",
                patternMsg: "正则表达式校验不通过时，显示给用户的提示信息",
                existsChk: "在提交保存时，验证输入的内容是否已在数据库中存在。例如会员的用户名，具有唯一性，提交时需要检测重复",
                existsFilter: {
                    area: ["660px", "400px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>若开启验证重复,系统会在数据保存之前,使用类似下面的SQL检查目标数据是否存在,若存在,则中止保存并提示用户." +
                        "<br/><span style='color:#860fc6'>select o.* from <span style='color:red'>TABLE</span> o where o.<span style='color:red'>field</span>='控件输入值' and o.<span style='color:red'>id</span>&lt;&gt;'当前数据id'  limit 1</span><br/>" +
                        "您可以在此基础上附加其它验重条件,如填写: <br/>" +
                        "<span style='color:#860fc6'>o.removed=0 and o.tid='${vo.tid}'</span><br/>" +
                        "则最终的验重SQL语句将变成:<br/>" +
                        "<span style='color:#860fc6'>select o.* from TABLE o where o.field='控件输入值' and o.id&lt;&gt;<span>'当前数据id' <br/>and (<span style='color:red'>o.removed=0 and o.tid='${vo.tid}'</span>)  limit 1</span></blockquote>"
                },
                existsMsg: "当数据库中存在重复内容时，显示给用户的提示信息",
                conditionShow: {
                    area: ["620px", "300px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>根据条件判断目标控件是否显示（不填无条件显示）." +
                        "<br/>例如：<span style='color:#860fc6'>vo.state==1 && vo.removed==0</span></blockquote><ol>" +
                        "<li>内置变量 <strong>vo</strong> 表示当前数据. 除vo外，还可使用以下内置变量：</li>" +
                        "<li><strong>user</strong> 表示当前登录用户.</li>" +
                        "<li><strong>qo</strong> 表示界面请求入参.如: <span style='color:#860fc6'>qo.foobar</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "<li>...</li>" +
                        "</ol>"
                },

                ajaxSave: "配置表单提交，是由JDiy系统自动处理保存，还是交由开发者自己编写的ajax地址去处理。",
                ajaxUrl: {
                    area: ["750px", "560px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>将表单提交到目标ajax地址去进行处理.注意以下三种写法:　" +
                        "<ol style='padding:5px;background-color: #f1f1be;font-size:12px;'>" +
                        "<li style='line-height:18px;'><strong>1.</strong>直接写相对地址,如<span style='color: #860fc6'>vip/doSave</span>则将请求提交至后台目录地址：<span style='color: #860fc6'><strong>/mgmt/</strong>vip/doSave</span><br/>" +
                        '<span style="color:green;line-height:18px;">(推荐用这种方式,因为对<span style="color:red">/mgmt/</span>路径下的资源访问,系统会自动做后台登录帐号身份认证)</span></li>' +
                        "<li style='margin:5px 0;'><strong>2.</strong>若以<strong style='color:red'>/</strong>开头,则目标地址相对于当前应用程序根目录(contextPath)</li>" +
                        "<li><strong>3.</strong>若以<span style='color: #860fc6'>https://</span>或<span style='color: #860fc6'>http://</span>开头,则提交到该外部URL(需注意外部程序的js跨域调用问题)</li>" +
                        "</ol>" +
                        "目标地址请求需返回带msg, code的json数据,当code=200时表示操作成功,code=其它值为失败,系统会将json的msg内容显示给用户.<br/>" +
                        "成功时返回示例:<br/>" +
                        "<pre style='background-color: #ddd;font-size:12px;line-height:18px;padding-left:10px;'>" +
                        '{"code":200,"msg":"操作成功!"}</pre>' +
                        "失败时返回示例:<br/>" +
                        "<pre style='background-color: #ddd;font-size:12px;line-height:18px;padding-left:10px;'>" +
                        '{"code":500,"msg":"操作失败的提示内容"}</pre>' +
                        "</blockquote>" +
                        "开发者可以自己编写的一个常规的Controller方法处理表单数据的保存，以下示例继承自JDiyCtrl(仅供参考)：<br/>" +
                        "<pre style='background-color: #ddd;font-size:12px;line-height:18px;padding-left:10px;'>" +
                        "import club.jdiy.core.base.domain.Ret;\n" +
                        "import org.springframework.stereotype.Controller;\n" +
                        "import org.springframework.web.bind.annotation.RequestMapping;\n" +
                        "@Controller\n" +
                        "public class VipController extends JDiyCtrl&lt;Vip,VipService&gt;{\n" +
                        "    \n" +
                        "    @RequestMapping(\"/mgmt/vip/doSave\")          //定义一个普通的Spring Controller方法，用于保存数据\n" +
                        "    @ResponseBody          //注意class类上方的注为@Controller时需加这行,若上面是@RestController,则此行省略\n" +
                        "    public Ret<?> doSave(Vip qo, HttpServletRequest request){\n" +
                        "        try{\n" +
                        "            service.doSave(qo);//此处自行编写表单数据保存的service层业务代码\n" +
                        "            return Ret.success();\n" +
                        "        }catch (Exception ex){\n" +
                        "            return Ret.error(ex);\n" +
                        "        }\n" +
                        "    }\n" +
                        "} </pre>"
                },

                pkTpl: "当数据添加表单提交时，设置主键的插值策略．请结合数据表的主键定义合理设置",
                pkPre: {
                    area: ["720px", "460px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>指定前缀字符串." +
                        "<br/>支持内嵌FTL表达式,实现自定义动态前缀.　例如：</blockquote><ol>" +
                        "<li><strong style='color: #860fc6'>T${.now?string('yyyyMMdd')}</strong> <br/>" +
                        "　表示以\"T\"+当前日期(yyyyMMdd格式)作为前缀，最终生成的前缀字符串形如： T20210501...  T20210502...<br/>&nbsp;</li>" +
                        "<li><strong style='color: #860fc6'>${(vo.gender==1)?string('BOY','GIRL')}${.now?string('yyyy')}</strong><br/>" +
                        "　假定当前表单中有一个名为gender的字段（其值1表示男，２表示女）, <br/>" +
                        "　若性别为男，生成的前缀形如：BOY2021... <br/>" +
                        "　若姓别为女，则前缀形如:GIRL2021...<br/>&nbsp;</li>" +
                        "<li><strong>更多的前缀生成方式，自已根据业务需求发挥吧...</strong></li>" +
                        "</ol>"
                },
                pkLen: '定义顺序号长度，例如3位，则当顺序号小于100时，前面自动补0凑足3位',
                hiddenParams: {
                    area: ["760px", "480px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>隐藏字段不会出现在用户界面，在表单提交时由系统自动更新。<br/>" +
                        "填写格式：<br/>" +
                        "<strong style='color: #860fc6'>name:value</strong>　" +
                        "或者：　" + "<strong style='color: #d90909'>@</strong><strong style='color: #860fc6'>name:value</strong><br/><br/>" +
                        "name为字段名，value为字段值<br/>" +
                        "当name前面加一个<strong style='color: #d90909'>@</strong>符号时，表示强制更新，即无论是添加还是修改数据都更新字段的值；否则，仅在数据添加(insert)时更新。</blockquote>" +
                        "其中value值内容支持FTL表达式，常用的表达式变量如下：<ol>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前用户id:  <span style='color:#860fc6'>${user.id}</span>  又如取登录帐号： <span style='color:#860fc6'>${user.uid}</span></li>" +
                        "<li><strong>qo</strong> 表示当前表单界面打开时的URL地址入参.如: <span style='color:#860fc6'>${qo.foobar}</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>${date}</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>${datetime}</span></li>" +
                        "<li><strong>ctx</strong> 表示当前应用程序上下文根路径.</li>" +
                        "</ol>"
                },
                pageHandler: {
                    area: ["620px", "345px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>由您编写一个java类并实现<strong style='color:#d90909'>club.jdiy.dev.view.FormHandler</strong>接口，" +
                        "以编程的方式干预表单界面，可完成更加复杂的业务逻辑处理</blockquote>" +
                        "首先在<span style='color:#d90909'>application.yml</span>中配置<span style='color:#d90909'>jdiy.handler-scan</span>(它用于指定增强器的类扫描根路径，" +
                        "若未配置，其默认值为 <span style='color:#d90909'>club.jdiy</span>)<br/>" +
                        "然后编写FormHandler类(放在handler-scan所在包或任意子包路径下均可）；<br/>" +
                        "重启项目，JDiyAdmin会自动识别您编写的增强处理类并在这个下拉菜单中列出来，供您选择配置使用．"
                },
                eventScript: '编写客户端js事件脚本',
                headTips: '界面头部显示的提示信息，允许输入HTML代码',
                footTips: '界面底部显示的提示信息，允许输入HTML代码'
            },
            list: {
                qoid: {
                    area: ["620px", "360px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>搜索控件的id具有唯一性，它由系统自动生成且不可修改．<br/>" +
                        "当您使用了列表增强功能，如<br/><strong style='color:#d90909'>ListHandler.onView(Pager<Rs> pager, Map<String,String> qo)</strong>　" +
                        "您可能会用到它.</blockquote>" +
                        "ListHandler.onView的第二个参数qo(Query Objects的简写)，储存了用户输入的搜索条件(即Key/Value形式的Map键值对)，" +
                        "其健名Key=搜索控件id, 即您可以通过id获取到控件的用户输入，以便增强处理（如针对表格中的每行数据，到其它子表中按搜索条件汇总统计，并将统计结果显示到行上某个特定的列）"
                },
                qo_sql: {
                    area: ["720px", "550px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>自定义SQL查询片段，用o表示当前列表界面绑定的主表；${value}表示当前查询框的用户输入值.<br/>" +
                        "直接写条件片段即可，请勿再添加where, order by等其它内容！<br/>" +
                        "例1： someField='${value}'<span style='color:grey'>　　　或者：　　</span>o.someField='${value}'<br/>" +
                        "例2： o.name='${value}' or o.phone='${value}' <span style='color:grey'>//可写多个条件</span><br/>" +
                        "例3： exists (select 0 from othertable ot where ot.id=o.ot_id and ot.removed=0 and ot.somefield like '%${value}%')" +
                        "<span style='color:grey'>//更复杂的条件</span><br/>" +
                        "</blockquote>查询条件支持内嵌EL或FTL表达式(见如下内置EL变量或示例)<br/><ol>" +
                        "<li><strong>value</strong> 表示当前的查询控件的用户输入值，写为<span style='color:#860fc6'>${value}</span></li>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前用户id:  <span style='color:#860fc6'>${user.id}</span>  又如取登录帐号： <span style='color:#860fc6'>${user.uid}</span></li>" +
                        "<li><strong>qo</strong> 表示界面请求入参.如: <span style='color:#860fc6'>${qo.foobar}</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>${date}</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>${datetime}</span></li>" +
                        "<li><strong>其它任意freemarker内置表达式甚至标签语句</strong> <br/>" +
                        "　　例如: someField='${value}' and monthField='<span style='color:#860fc6'>${.now?string('yyyy-MM')}</span>'</li>" +
                        "　　又如: someField='${value}'<span style='color:#860fc6'>&lt;#if qo.pid??&gt; or o.pid='${qo.pid}'&lt;/#if&gt;</span></li>" +
                        "</ol>"
                },
                qo_field: "绑定要查询的表字段名",
                qo_tField: "可以任意填写非持久字段名称，主要用于列表界面增强处理(以编程方式查询)",
                qo_label: "设置控件前面显示的文字内容",
                qo_type: function (uid) {
                    return $('#qo_type_' + uid).val() === 'joinTable' ? hh_global2 : "设置此控件的类型"
                },
                qo_joinRef: "外联表的外键(关联当前主表id)",
                qo_joinType: "定义关联表查询类型",
                qo_joinTable: function (uid) {
                    return $('#qo_joinType_' + uid).val() === 'manyToOne' ? hh_global2 : "选择要查询的外联表名"
                },
                qo_joinField: function (uid) {
                    return $('#qo_joinType_' + uid).val() === 'manyToOne' ? hh_global2 : "选择外联表中要查询的字段"
                },
                qo_manyTable: "多对多关系，使用一个双向关联的中间表记录关系。此选项用于配置中间表名",
                qo_manyField0: "多对多关系，使用一个双向关联的中间表记录关系。此选项用于配置中间表指向本表id的字段名",
                qo_manyField1: "多对多关系，使用一个双向关联的中间表记录关系。此选项用于配置中间表指向外表id的字段名",
                qo_treeId: "若此处无可选项，需要您预先配置一个树界面",
                qo_treeDisplay: "设置当用户选中树形下拉菜单某个节点后，显示的(选中项)层级文字样式",
                qo_treeOnlyLeaf: "设置树节点有子级时，该父节点是否允许被选择。",
                qo_oper: "查询字段的匹配方式",
                qo_width: "设置控件的显示宽度",
                qo_placeholder: "设置输入框的占位提示文字<br/>即 placeholder<br/>例如可以将控件前的标题文字置空，然后用占位提示替代",
                items: "静态选项条目(一行一个).<br/>格式：A:B <br/>A为条目值，B为条目显示文字.<br/>例如下拉菜单控件第一项是[不限]，则首行填入：<br/>:=不限=",
                optType: '指定控件的选项条目数据来源',
                optEntity: "为控件增加动态条目，指定条目项的数据来源JPA实体",
                optTable: "为控件增加动态条目，指定条目项的数据来源表",
                optTxtField: "为控件增加动态条目,指定条目项的文字显示字段",
                optValField: "为控件增加动态条目,指定条目项的值字段",
                optWhere: {
                    area: ["720px", "460px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>从来源表（或实体）查询数据时，附加过滤条件.用<strong style='color:#860fc6'>o</strong>表示当前表记录或实体.<br/>" +
                        "例如： o.tid=3 and o.user_id='${user.id}'" +
                        "</blockquote>查询条件支持内嵌EL或FTL表达式(见如下内置变量或示例)<br/><ol>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前用户id:  <span style='color:#860fc6'>${user.id}</span>  又如取登录帐号： <span style='color:#860fc6'>${user.uid}</span></li>" +
                        "<li><strong>qo</strong> 表示界面请求入参.如: <span style='color:#860fc6'>${qo.foobar}</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>${date}</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>${datetime}</span></li>" +
                        "<li><strong>其它任意freemarker内置表达式甚至标签语句</strong> <br/>" +
                        "　　例如: <span style='color:#860fc6'>${.now?string('yyyy-MM')}</span></li>" +
                        "　　又如: <span style='color:#860fc6'>&lt;#if qo.pid??&gt; and o.pid='${qo.pid}'&lt;/#if&gt;</span></li>" +
                        "</ol>"
                },
                optSort: "从来源表（或实体）查询时，指定排序方式,将以此顺序添加到到当前控件中.<br/>例如：o.id desc",
                optDict: "从数据字典拉取数据到当前控件，<br/>字典key作为选项文字<br/>字典value作为选项的值",
                optQlType: '通过SQL(或JQL)语句查询出结果作为控件的条目项．<br/>此处用于指定查询语句的类型．',
                optQry: {
                    area: ["720px", "460px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>通过SQL(或JQL)语句查询出结果作为控件的条目项." +
                        "<br/>查询结果第1例作为选项值，第2例作为显示文字(若只有1列,值和文字都使用这1列数据).<br/>" +
                        "例如(SQL)： select id,name from aaa o where o.bb='cc' and o.user_id='${user.id}' order by o.dd asc<br/>" +
                        "又如(JQL)： select o.id,o.name from Aaa o where o.bb='cc' and o.user.id='${user.id}' order by o.dd asc<br/>" +
                        "</blockquote>查询条件支持内嵌EL或FTL表达式(见如下内置变量或示例)<br/><ol>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前用户id:  <span style='color:#860fc6'>${user.id}</span>  又如取登录帐号： <span style='color:#860fc6'>${user.uid}</span></li>" +
                        "<li><strong>qo</strong> 表示界面请求入参.如: <span style='color:#860fc6'>${qo.foobar}</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>${date}</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>${datetime}</span></li>" +
                        "<li><strong>其它任意freemarker内置表达式甚至标签语句</strong> <br/>" +
                        "　　例如: <span style='color:#860fc6'>${.now?string('yyyy-MM')}</span></li>" +
                        "　　又如: <span style='color:#860fc6'>&lt;#if qo.pid??&gt; and o.pid='${qo.pid}'&lt;/#if&gt;</span></li>" +
                        "</ol>"
                },
                cascade: {
                    area: ["760px", "480px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>指示当前控件的条目选项，会随着其它控件的选项值选中改变，联动更新.</br></br>" +
                        "常见的业务场景如省市地区菜单级联，当选择“省”后，“市”的下拉菜单条目项会联动更新为该省下的数据。" +
                        "此业务场景下，省作为父级菜单，市作为子级菜单，我们为“市”这个子级菜单增加附加查询条件，并将级联选项「开启」即可。" +
                        "" +
                        "</blockquote>要使子级菜单的选项联动，需满足如下3个前置条件：<br/><ul>" +
                        "<li>1.父级控件类型必须是select(下拉菜单)</li>" +
                        "<li>2.子级控件类型必须是select(下拉菜单)</li>" +
                        "<li>3.子级控件配置了非字典类型的“动态条目”，且动态条的查询条件(或sql/jql语句)中，包含 ${qo.XXX}这样的内容（XXX为级联触发的父级控件字段名）</li>" +
                        "</ul>例如省市级联菜单，假如有两个表(province,city)分别存储了省和市的数据，且" +
                        "city表中有一个名为province_id的外键字段，记录它所属的省份。我们在界面中分别配置（省和市）两个下拉菜单，然后" +
                        "配置“市”菜单的动态选项附加查询条件为： province_id='${vo.province_id}' 并为市菜单开启级联选项即可。"
                },
                qo_initial: "设置此控件的默认值<br/>有值则默认带上此条件查询",

                btn_text: "按钮的文字内容",
                btn_icon: "为钮选一个图标",
                btn_color: "设置按钮的背景或文字颜色",
                btn_act: "设置用户点击按钮做什么事情",
                btn_updateCode: {
                    area: ["720px", "480px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>设置点击按钮后，更新该数据的字段的值。若有多个字段要更新，则一行填一个." +
                        "<br/>格式：字段='新值'<br/>如果值是一个数字，则单引号可省略" +
                        "</blockquote>支持内嵌EL或FTL表达式(见如下内置变量或示例)<br/><ol>" +
                        "<li><strong>vo</strong> 表示当前数据.如: <span style='color:#860fc6'>num=${vo.num+1}</span></li>" +
                        "<li><strong>qo</strong> 表示URL地址中的参数.如: <span style='color:#860fc6'>${qo.xx}</span>等效于request.getParameter(\"xx\")</li>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前登录用户id存入auditUser_id字段:  <span style='color:#860fc6'>auditUser_id='${user.id}'</span></li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>editDate='${date}'</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>updateTime='${datetime}'</span></li>" +
                        "<li><strong>其它freemarker内置表达式</strong> <br/>" +
                        "　　例如: <span style='color:#860fc6'>foo='${.now?string('yyyy-MM')}'</span></li>" +
                        "　　又如: <span style='color:#860fc6'>finishFlag='${(vo.state==3)?string('1','0')}'</span></li>" +
                        "</ol>"
                },
                btn_ajaxUrl: {
                    area: ["620px", "390px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>设置自定义的Ajax执行URL地址.<br/>" +
                        "<br/>示例：<span style='color:#860fc6'>${ctx}/mgmt/order/delete?id=${vo.id}</span><br/>" +
                        "其中<span style='color:#860fc6'>id=${vo.id}</span>可以省略（行管理按钮会自动添加该参数）</blockquote>" +
                        "支持内嵌EL或FTL表达式(见如下内置变量或示例)<br/><ol>" +
                        "<li><strong>ctx</strong> 表示当前应用程序上下文,相当于request.getContextPath()</li>" +
                        "<li><strong>vo</strong> 表示当前行数据(行按钮适用).如: ${ctx}/mgmt/foo/bar?name=<span style='color:#860fc6'>${vo.name}</span></li>" +
                        "<li><strong>qo</strong> 表示URL地址中的参数.如: <span style='color:#860fc6'>${qo.xx}</span>等效于request.getParameter(\"xx\")</li>" +
                        "</ol>"
                },
                btn_pageId: "设置点击按钮要打开的界面",
                btn_pageParam: {
                    area: ["560px", "420px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>使用url参数拼接格式<br/>" +
                        "支持EL表达式，行管理按钮可以用vo表示当前行数据<br/>例如：" +
                        "<br/><span style='color:#860fc6'>foo1=bar1&xx_id=${vo.id}</span><br/>" +
                        "此外还可以使用<span style='color:#860fc6'>qo</span>表示当前页面的URL地址入参(通常由菜单或其它界面内的操作按钮上配置传入)，" +
                        "<br/>如：<span style='color:#860fc6'>${qo.xx}</span>相当于request.getParameter(\"xx\")<br/>" +
                        "<span style='color:red'>特别注意：</span><br/>数据行右侧的[修改]或[详情页]按钮，无需再配置ID传参，如：<span style='color:#860fc6'>id=${vo.id}</span><br/>" +
                        "(系统会自动添加＂当前表主键=值＂的参数至目标界面)" +
                        "</blockquote>"
                },
                btn_outLink: {
                    area: ["550px", "420px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>设置要打开的自定义页面地址<br/>" +
                        "可以是一个相对路径，也可以是一个外部http地址" +
                        "<br/><br/>例如<span style='color:red'>(vo变量仅表格行管理按钮可用)</span>：" +
                        "<br/><span style='color:#860fc6'>${ctx}/mgmt/foo/bar?xx_id=${vo.id}</span><br/>" +
                        "#其中${ctx}表示程序根路径(即contextPath)；vo表示当前行对象<br/>" +
                        "又如：" +
                        "<span style='color:#860fc6'>http:/foo.bar.com/xxx/xxx?id=${vo.id}</span>　　#打开外部http链接<br/><br/>" +
                        "如果是表格上面的按钮，且操作类型配置为［选中项post到自定义页面］，系统会将用户钩选的所有条目项的id值以post方式(弹出新窗口)打开目标页面．" +
                        "</blockquote>"
                },
                btn_target: "页面打开方式",
                btn_title: {
                    area: ["480px", "280px"],
                    title: "配置帮助",
                    content: '<blockquote class="layui-elem-quote">配置按钮点击后的弹出窗口(或Tab页)标题；<br/>' +
                        '空着不填则将按钮文字作为标题显示；<br/>' +
                        '支持EL表达式，如数据行上的管理按钮可用vo表示当前行数据。<br/>' +
                        '例： <span style="color:#860fc6">${vo.name}修改</span>' +
                        "</blockquote>"
                },
                btn_width: "根据弹窗中显示的内容适当调整",
                btn_height: "根据弹窗中显示的内容适当调整",
                btn_confirm: "操作确认提示文字",
                btn_dbl: "若绑定，当用户双击表格行时，会自动触发此按钮的点击操作",
                btn_footer: "指示弹窗底部的[保存]和[取消]控制按钮是否显示",
                btn_grantCode: {
                    area: ["540px", "340px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>用于预定义界面内按钮级别的细粒度权限控制.<br/>" +
                        "<br/>例如[删除]按钮权限代码填<span style='color:#860fc6'>del</span>, 则[系统管理]-[角色管理]模块维护角色时,会在绑定了此界面的菜单右边出现按钮级别的权限控制项．<br/><br/>" +
                        "注：若同一界面内多个按钮的权限代码相同,这些按钮将自动归并为同一个权限控制项." +
                        "" +
                        "</blockquote>"
                },
                btn_conditionShow: {
                    area: ["620px", "300px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>根据条件判断按钮是否显示（不填无条件显示）." +
                        "<br/>例如：<span style='color:#860fc6'>vo.state==1 && vo.removed==0</span></blockquote><ol>" +
                        "<li>内置变量 <strong>vo</strong> 表示当前行数据(行按钮适用). 除vo外，还可使用以下内置变量：</li>" +
                        "<li><strong>user</strong> 表示当前登录用户.</li>" +
                        "<li><strong>qo</strong> 表示界面请求入参.如: <span style='color:#860fc6'>qo.foobar</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "</ol>"
                },
                col_field: '设置此列的绑定字段',
                col_tField: "可以任意填写（数据库中不存在的）字段名．用于自定义模板或编程显示输出",
                col_label: "设置此列的表头文字",
                col_format: function (uid) {
                    let fmt = $('#col_format_' + uid).val();
                    switch (fmt) {
                        case 'datetime':
                            return '表字段需要为时间类型才可用<br/>格式化显示为：<br/>yyyy-MM-dd HH:mm:ss'
                        case 'date':
                            return '表字段需要为时间类型才可用<br/>格式化显示为：<br/>yyyy-MM-dd'
                        case 'time':
                            return '表字段需要为时间类型才可用<br/>格式化显示为：<br/>HH:mm:ss'
                        case 'number':
                            return '表字段需要为数字类型才可用<br/>格式化保留两位小数显示'
                        case 'kv':
                            return '定义一批[值:显示文字]条目，把当前列值映射转换成对应的文字显示';
                        case 'dict':
                            return '把当前列当作字典值，映射显示字典文字';
                        case 'manyToOne':
                            return '将当前列绑定的字段当作外键，显示外联表中某个字段的值';
                        case 'oneToMany':
                            return '一对多关系，显示外部表中与当前记录相关的数据列表';
                        case 'manyToMany':
                            return '多对多关系，使用一个双向关联的中间表记录关系。显示与当前记录相关的外部数据列表';
                        case 'tpl':
                            return '通过EL表达式或FTL标签语句自定义显示内容';
                        case 'java':
                            return '通过java编程方式自定义显示内容（需与列表增强器配合使用）';
                        default:
                            return "设置此列的显示内容格式，请务必结合表字段类型正确设置"
                    }
                },
                col_joinTable: "指定外部表名",
                col_joinField: "选择要显示的外部表字段名",
                col_joinRef: "一对多关系，在外部表上用一个外键字段关联本表的主键",
                col_manyTable: "多对多关系，使用一个双向关联的中间表记录关系。此选项用于配置中间表名",
                col_manyField0: "多对多关系，使用一个双向关联的中间表记录关系。此选项用于配置中间表指向本表id的字段名",
                col_manyField1: "多对多关系，使用一个双向关联的中间表记录关系。此选项用于配置中间表指向外表id的字段名",
                col_kv: "键值:键名[:颜色] 转换条目列表<br/>(一行填一个).<br/>格式：V:K 或 V:K:C <br/>其中：<br/>V=值,B=显示文字,C=显示颜色<br/>若无匹配项，将直接显示原值",
                col_dictId: "选择字典类型<br/>若字典中无匹配项，将显示原值",
                col_treeId: "若此处无可选项，需要您预先配置一个树界面",
                col_treeDisplay: "设置树形节点显示的层级路径文字样式",
                col_tpl: {
                    area: ["720px", "500px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>通过EL表达式或者freemarker模板自定义输出显示." +
                        "</blockquote><ol>" +
                        "<li>内置变量 <strong>vo</strong> 表示当前行数据.如: <span style='color:#860fc6'>${vo.fieldName}</span>表示输出显示fieldName字段值</li>" +
                        "<li><strong>freemarker模板示例：</strong> <br/>" +
                        "<span style='color:#860fc6'>&lt;#if vo.state==1&gt;<br/>&nbsp;&nbsp;&nbsp;&nbsp;${vo.auditUser}审核于${vo.auditTime?string('yyyy-MM-dd HH:mm')}<br/>" +
                        "&lt;#else&gt;<br/>&nbsp;&nbsp;&nbsp;&nbsp;&lt;span style=\"color:red\"&gt;未审核&lt;/span&gt<br/>&lt;/#if&gt;</li>" +
                        "<li>除vo外，还可使用以下内置变量：</li>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前用户id:  <span style='color:#860fc6'>${user.id}</span>  又如取登录帐号： <span style='color:#860fc6'>${user.uid}</span></li>" +
                        "<li><strong>qo</strong> 表示界面请求入参.如: <span style='color:#860fc6'>${qo.foobar}</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>${date}</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>${datetime}</span></li>" +
                        "<li><strong>ctx</strong> 表示当前应用程序上下文根路径</li>" +
                        "</ol>"
                },
                col_align: "此列的对齐方式",
                col_width: "根据实际数据内容适当调整．不填则由系统自动分配",
                col_sortable: "若开启，用户点击表头可排序<br/>(非数据库字段此配置无效)",
                col_sum: "在页面底部或右上角显示此列的数据汇总",
                col_sumPos: "指明数据汇总显示位置",

                pageSize: '设置每页显示的记录条数<br/>默认为15条<br/>填0表示不分页（慎用！数据过多时不分页可能造成网页卡死）',
                defaultSort: "设置列表查询时的排序方式<br/>用o表示当前主表<br/>根据业务需要调整",
                sqlFilter: {
                    area: ["720px", "450px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>列表数据查询，附加全局过滤条件." +
                        "用<span style='color:#860fc6'>o</span>表示当前主表(或实体)" +
                        "<br/>" +
                        "例如(SQL)： o.user_id='${user.id}' and o.removed=0<br/>" +
                        "又如(JQL)： o.user.id='${user.id}' and o.removed=false<br/>" +
                        "</blockquote>查询条件支持内嵌EL或FTL表达式(见如下内置变量或示例)<br/><ol>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前用户id:  <span style='color:#860fc6'>${user.id}</span>  又如取登录帐号： <span style='color:#860fc6'>${user.uid}</span></li>" +
                        "<li><strong>qo</strong> 表示界面请求入参.如: <span style='color:#860fc6'>${qo.foobar}</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>${date}</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>${datetime}</span></li>" +
                        "<li><strong>其它任意freemarker内置表达式甚至标签语句</strong> <br/>" +
                        "　　例如: <span style='color:#860fc6'>foobar='${.now?string('yyyy-MM')}</span>'</li>" +
                        "　　又如: <span style='color:#860fc6'>1=1 &lt;#if qo.pid??&gt; and o.pid='${qo.pid}'&lt;/#if&gt;</span></li>" +
                        "</ol>"
                },
                ls_tableHeight: {
                    area: ["750px", "510px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>指定数据表格的高度，支持如下三种" +
                        "</blockquote><ol>" +
                        "<li><strong>不填写</strong><br/>默认情况。高度随数据列表而适应(当数据超过了主界面可视区高度，要拖动右侧滚动条才可以看到表格下方的分页条).</li>" +
                        "<li><strong>固定值</strong><br/>设定一个数字，用于固定表格容器高度. 注意当表格里的数据超过了您设定的高度，表格容器会出现纵向滚动条，同时如果整个列表页面高度也超过了浏览器可视区高度时,右侧将会多出一个滚动条．</li>" +
                        "<li><strong>full-差值</strong><br/>高度将始终铺满，无论浏览器尺寸如何。这是一个特定的语法格式，其中 full 是固定的，而 差值 则是一个数值，这需要你来预估并尝试给出一个大概的值，即表格容器距离浏览器顶部和底部的距离之和" +
                        "(即系统主界面头部区域+当前这个列表界面上方的搜索条高度+底部的分页条高度, 若顶部的搜索条只占用了一行,输入 <strong style='color:green'> full-270 </strong>基本上能让表格铺满且让分页条始终固定在浏览器可视区底部, 如果您发现分页条偏下（要拖动右边滚动条才看得到）或者右边出现两个纵向滚动条，您应该适当增大此数值．</li>" +
                        "</ol>"
                },
                ls_mge: '若关闭，将不显示表格右侧的管理栏(适用于日志列表等不需要修改/删除的列表使用)',
                ls_mgeWidth: "设置表格最右边管理栏宽度<br/>根据配置的按钮数量适当调整",
                ls_rowNum: '是否在表格左侧显示行号',
                excel: '是否显示数据导出按钮',
                excelName: '点击导出按钮后，导出显示的下载文件名',
                pageHandler: {
                    area: ["620px", "345px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>由您编写一个java类并实现<strong style='color:#d90909'>club.jdiy.dev.view.ListHandler</strong>接口，" +
                        "以编程的方式干预列表界面，可完成更加复杂的业务逻辑处理</blockquote>" +
                        "首先在<span style='color:#d90909'>application.yml</span>中配置<span style='color:#d90909'>jdiy.handler-scan</span>(它用于指定增强器的类扫描根路径，" +
                        "若未配置，其默认值为 <span style='color:#d90909'>club.jdiy</span>)<br/>" +
                        "然后编写ListHandler类(放在handler-scan所在包或任意子包路径下均可）；<br/>" +
                        "重启项目，JDiyAdmin会自动识别您编写的增强处理类并在这个下拉菜单中列出来，供您选择配置使用．"
                },
                headTips: '界面头部显示的提示文字信息，允许输入HTML代码',
                footTips: '界面底部显示的提示文字信息，允许输入HTML代码'
            },
            tree: {
                btn_text: "按钮的文字内容",
                btn_icon: "为钮选一个图标",
                btn_color: "设置按钮的背景或文字颜色",
                btn_act: "设置用户点击按钮做什么事情",
                btn_updateCode: {
                    area: ["720px", "480px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>设置点击按钮后，更新该数据的字段的值。若有多个字段要更新，则一行填一个." +
                        "<br/>格式：字段='新值'<br/>如果值是一个数字，则单引号可省略" +
                        "</blockquote>支持内嵌EL或FTL表达式(见如下内置变量或示例)<br/><ol>" +
                        "<li><strong>vo</strong> 表示当前数据.如: <span style='color:#860fc6'>num=${vo.num+1}</span></li>" +
                        "<li><strong>qo</strong> 表示URL地址中的参数.如: <span style='color:#860fc6'>${qo.xx}</span>等效于request.getParameter(\"xx\")</li>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前登录用户id存入auditUser_id字段:  <span style='color:#860fc6'>auditUser_id='${user.id}'</span></li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>editDate='${date}'</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>updateTime='${datetime}'</span></li>" +
                        "<li><strong>其它freemarker内置表达式</strong> <br/>" +
                        "　　例如: <span style='color:#860fc6'>foo='${.now?string('yyyy-MM')}'</span></li>" +
                        "　　又如: <span style='color:#860fc6'>finishFlag='${(vo.state==3)?string('1','0')}'</span></li>" +
                        "</ol>"
                },
                btn_ajaxUrl: {
                    area: ["620px", "390px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>设置自定义的Ajax执行URL地址.<br/>" +
                        "<br/>示例：<span style='color:#860fc6'>${ctx}/mgmt/order/delete?id=${vo.id}</span><br/>" +
                        "其中<span style='color:#860fc6'>id=${vo.id}</span>可以省略（行管理按钮会自动添加该参数）</blockquote>" +
                        "支持内嵌EL或FTL表达式(见如下内置变量或示例)<br/><ol>" +
                        "<li><strong>ctx</strong> 表示当前应用程序上下文,相当于request.getContextPath()</li>" +
                        "<li><strong>vo</strong> 表示当前行数据(行按钮适用).如: ${ctx}/mgmt/foo/bar?name=<span style='color:#860fc6'>${vo.name}</span></li>" +
                        "<li><strong>qo</strong> 表示URL地址中的参数.如: <span style='color:#860fc6'>${qo.xx}</span>等效于request.getParameter(\"xx\")</li>" +
                        "</ol>"
                },
                btn_pageId: "设置点击按钮要打开的界面",
                btn_pageParam: {
                    area: ["560px", "420px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>使用url参数拼接格式<br/>" +
                        "支持EL表达式，行管理按钮可以用vo表示当前行数据<br/>例如：" +
                        "<br/><span style='color:#860fc6'>foo1=bar1&xx_id=${vo.id}</span><br/>" +
                        "此外还可以使用<span style='color:#860fc6'>qo</span>表示当前页面的URL地址入参(通常由菜单或其它界面内的操作按钮上配置传入)，" +
                        "<br/>如：<span style='color:#860fc6'>${qo.xx}</span>相当于request.getParameter(\"xx\")<br/>" +
                        "<span style='color:red'>特别注意：</span><br/>数据行右侧的[修改]或[详情页]按钮，无需再配置ID传参，如：<span style='color:#860fc6'>id=${vo.id}</span><br/>" +
                        "(系统会自动添加＂当前表主键=值＂的参数至目标界面)" +
                        "</blockquote>"
                },
                btn_outLink: {
                    area: ["540px", "380px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>设置要打开的自定义页面地址地址<br/>" +
                        "可以是一个相对路径，也可以是一个外部http地址" +
                        "<br/><br/>例如：" +
                        "<br/><span style='color:#860fc6'>${ctx}/mgmt/foo/bar?xx_id=${vo.id}</span><br/>" +
                        "#其中${ctx}表示程序根路径(即contextPath)；vo表示当前行对象<br/>" +
                        "<br/>又如：" +
                        "<br/><span style='color:#860fc6'>http:/foo.bar.com/xxx/xxx?id=${vo.id}</span>　　#打开外部http链接" +
                        "</blockquote>"
                },
                btn_target: "页面打开方式",
                btn_title: {
                    area: ["480px", "280px"],
                    title: "配置帮助",
                    content: '<blockquote class="layui-elem-quote">配置按钮点击后的弹出窗口(或Tab页)标题；<br/>' +
                        '空着不填则将按钮文字作为标题显示；<br/>' +
                        '支持EL表达式，如数据行上的管理按钮可用vo表示当前行数据。<br/>' +
                        '例： <span style="color:#860fc6">${vo.name}修改</span>' +
                        "</blockquote>"
                },
                btn_width: "根据弹窗中显示的内容适当调整",
                btn_height: "根据弹窗中显示的内容适当调整",
                btn_confirm: "操作确认提示文字",
                btn_dbl: "若绑定，当用户双击表格行时，会自动触发此按钮的点击操作",
                btn_grantCode: {
                    area: ["540px", "340px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>用于预定义界面内按钮级别的细粒度权限控制.<br/>" +
                        "<br/>例如[删除]按钮权限代码填<span style='color:#860fc6'>del</span>, 则[系统管理]-[角色管理]模块维护角色时,会在绑定了此界面的菜单右边出现按钮级别的权限控制项．<br/><br/>" +
                        "注：若同一界面内多个按钮的权限代码相同,这些按钮将自动归并为同一个权限控制项." +
                        "" +
                        "</blockquote>"
                },
                btn_conditionShow: {
                    area: ["620px", "300px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>根据条件判断按钮是否显示（不填无条件显示）." +
                        "<br/>例如：<span style='color:#860fc6'>vo.state==1 && vo.removed==0</span></blockquote><ol>" +
                        "<li>内置变量 <strong>vo</strong> 表示当前行数据(行按钮适用). 除vo外，还可使用以下内置变量：</li>" +
                        "<li><strong>user</strong> 表示当前登录用户.</li>" +
                        "<li><strong>qo</strong> 表示界面请求入参.如: <span style='color:#860fc6'>qo.foobar</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "</ol>"
                },
                btn_footer: "指示弹窗底部的[保存]和[取消]控制按钮是否显示",
                btn_depthShow: "控制按钮在树的哪些层级显示<br/>默认全部层级都显示",
                col_field: '设置此列的绑定字段',
                col_tField: "可以任意填写（数据库中不存在的）字段名．用于自定义模板或编程显示输出",
                col_label: "设置此列的表头文字",
                col_format: function (uid) {
                    let fmt = $('#col_format_' + uid).val();
                    switch (fmt) {
                        case 'datetime':
                            return '表字段需要为时间类型才可用<br/>格式化显示为：<br/>yyyy-MM-dd HH:mm:ss'
                        case 'date':
                            return '表字段需要为时间类型才可用<br/>格式化显示为：<br/>yyyy-MM-dd'
                        case 'time':
                            return '表字段需要为时间类型才可用<br/>格式化显示为：<br/>HH:mm:ss'
                        case 'number':
                            return '表字段需要为数字类型才可用<br/>格式化保留两位小数显示'
                        case 'kv':
                            return '定义一批[值:显示文字]条目，把当前列值映射转换成对应的文字显示名';
                        case 'dict':
                            return '把当前列当作字典值，映射显示字典文字';
                        case 'manyToOne':
                            return '将当前列绑定的字段当作外键，显示外联表中某个字段的值';
                        case 'oneToMany':
                            return '一对多关系，显示外部表中与当前记录相关的数据列表';
                        case 'manyToMany':
                            return '多对多关系，使用一个双向关联的中间表记录关系。显示与当前记录相关的外部数据列表';
                        case 'tpl':
                            return '通过EL表达式或FTL标签语句自定义显示内容';
                        case 'java':
                            return '通过java编程方式自定义显示内容（需与列表增强器配合使用）';
                        default:
                            return "设置此列的显示内容格式，请务必结合表字段类型正确设置"
                    }
                },
                col_joinTable: "指定外部表名",
                col_joinField: "选择要显示的外部表字段名",
                col_joinRef: "一对多关系，在外部表上用一个外键字段关联本表的主键",
                col_manyTable: "多对多关系，使用一个双向关联的中间表记录关系。此选项用于配置中间表名",
                col_manyField0: "多对多关系，使用一个双向关联的中间表记录关系。此选项用于配置中间表指向本表id的字段名",
                col_manyField1: "多对多关系，使用一个双向关联的中间表记录关系。此选项用于配置中间表指向外表id的字段名",
                col_kv: "键值:键名[:颜色] 转换条目列表<br/>(一行填一个).<br/>格式：V:K 或 V:K:C <br/>其中：<br/>V=值,B=显示文字,C=显示颜色<br/>若无匹配项，将直接显示原值",
                col_dictId: "选择字典类型<br/>若字典中无匹配项，将显示原值",
                col_treeId: "若此处无可选项，需要您预先配置一个树界面",
                col_treeDisplay: "设置树形节点显示的层级路径文字样式",
                col_tpl: {
                    area: ["720px", "500px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>通过EL表达式或者freemarker模板自定义输出显示." +
                        "</blockquote><ol>" +
                        "<li>内置变量 <strong>vo</strong> 表示当前行数据.如: <span style='color:#860fc6'>${vo.fieldName}</span>表示输出显示fieldName字段值</li>" +
                        "<li><strong>freemarker模板示例：</strong> <br/>" +
                        "<span style='color:#860fc6'>&lt;#if vo.state==1&gt;<br/>&nbsp;&nbsp;&nbsp;&nbsp;${vo.auditUser}审核于${vo.auditTime?string('yyyy-MM-dd HH:mm')}<br/>" +
                        "&lt;#else&gt;<br/>&nbsp;&nbsp;&nbsp;&nbsp;&lt;span style=\"color:red\"&gt;未审核&lt;/span&gt<br/>&lt;/#if&gt;</li>" +
                        "<li>除vo外，还可使用以下内置变量：</li>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前用户id:  <span style='color:#860fc6'>${user.id}</span>  又如取登录帐号： <span style='color:#860fc6'>${user.uid}</span></li>" +
                        "<li><strong>qo</strong> 表示界面请求入参.如: <span style='color:#860fc6'>${qo.foobar}</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>${date}</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>${datetime}</span></li>" +
                        "<li><strong>ctx</strong> 表示当前应用程序上下文根路径</li>" +
                        "</ol>"
                },
                col_align: "此列的对齐方式",
                col_width: "根据实际数据内容适当调整．不填则由系统自动分配",

                nameField: '树形列显示字段',
                pidField: {
                    area: ["720px", "400px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>配置[父级id]字段名.也就是关联上级节点id的外键字段名称．</blockquote>" +
                        "假设当前表存储省/市/地区数据，我们以如下三条层级地区为例：<br/><span style='color:#860fc6'>" +
                        "湖北省 id=111<br/>" +
                        "　　宜昌市 id=222<br/>" +
                        "　　　　夷陵区 id=333</span><br/>" +
                        "此时，我们可以就定义一个<strong style='color:#860fc6'>parentId</strong>字段,记录该地区所属的上级地区,值分别如下：<br/><span style='color:#860fc6'>" +
                        "湖北省 id=111, parentId=<strong style='color:#c00;background-color: yellow'>null</strong><br/>" +
                        "宜昌市 id=222, parentId=<strong style='color:#c00;background-color: yellow'>111</strong><br/>" +
                        "夷陵区 id=333, parentId=<strong style='color:#c00;background-color: yellow'>222</strong></span>"
                },
                rootPid: {
                    area: ["720px", "450px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>顶层节点的[父级id]字段存值内容．" +
                        "在标准的数据表结构化设计中（定义ForeignKey外键），当外键字段没有关联数据时，其值应该为null," +
                        "但有些时候，开发者没有在数据库DDL中定义外键，有可能是用其它值（例如空字符串，或数字０）来表示无外键关系，因此jdiy增加了此配置项. </blockquote>" +
                        "假设当前表存储省/市/地区数据，我们以如下层级地区为例：<br/><span style='color:#860fc6'>" +
                        "湖北省 id=111<br/>" +
                        "　　宜昌市 id=222<br/>" +
                        "　　　　夷陵区 id=333</span><br/>" +
                        "此时，我们可以就定义一个<strong style='color:#860fc6'>parentId</strong>的外键字段,记录该地区所属的上级地区,但湖北省它是属于顶层省份（没有上级了），" +
                        "此配置项用于设置顶层的parentId取值，若配置为null(推荐)，则湖北省的parentId=null. " +
                        "此时，系统就可以通过　select * from 地区表　where parentId is null 来列出所有顶层地区．"
                },
                pathField: {
                    area: ["720px", "450px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>JDiy树形表需要一个额外的字段来存储节点层级路径." +
                        "用来避免递归查询，让搜索更加高效．<br/>" +
                        "路径列的数据库字段类型需为<strong style='color:#860fc6'> text </strong>. 或varchar(2000)  （注：视层级深度而定，保留足够冗余）<br/>" +
                        "其值＝它所在的所有祖父级节点id和它自身的id值用点隔开（并再在前后各加一个点）</blockquote>" +
                        "假设当前表存储省/市/地区数据，我们以如下三条层级地区为例：<br/><span style='color:#860fc6'>" +
                        "湖北省 id=111<br/>" +
                        "　　宜昌市 id=222<br/>" +
                        "　　　　夷陵区 id=333</span><br/>" +
                        "此时，我们可以就定义一个<strong style='color:#860fc6'>sortPath</strong>字段,其值分别如下：<br/><span style='color:#860fc6'>" +
                        "湖北省 id=111, sortPath=<strong style='color:#c00;background-color: yellow'>.111.</strong><br/>" +
                        "宜昌市 id=222, sortPath=<strong style='color:#c00;background-color: yellow'>.111.222.</strong><br/>" +
                        "夷陵区 id=333, sortPath=<strong style='color:#c00;background-color: yellow'>.111.222.333.</strong></span>"
                },
                sortField: '设置查询节点后的排序方式<br/>此项用于指定排序字段',
                sortType: '设置查询节点后的排序方式',
                lazy: {
                    area: ["600px", "330px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>若开启动态加载，系统默认只加载顶层数据，" +
                        "当用户点击某个树节点，再动态的去获取它的子级显示；" +
                        "<br/>若关闭此功能，则一次性加载全部节点数据．<br/><br/>" +
                        "当树形界面维护的数据量很少时，可考虑使用全部加载模式；<br/>" +
                        "若数据量较大（例如全国省市地区数据）时必须开启动态加载，否则用户进入树形界面会非常缓慢甚至页面卡死．</blockquote>" +
                        ""
                },
                sqlFilter: {
                    area: ["720px", "450px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>列表数据查询，附加全局过滤条件." +
                        "用<span style='color:#860fc6'>o</span>表示当前主表(或实体)" +
                        "<br/>" +
                        "例如(SQL)： o.user_id='${user.id}' and o.removed=0<br/>" +
                        "又如(JQL)： o.user.id='${user.id}' and o.removed=false<br/>" +
                        "</blockquote>查询条件支持内嵌EL或FTL表达式(见如下内置变量或示例)<br/><ol>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前用户id:  <span style='color:#860fc6'>${user.id}</span>  又如取登录帐号： <span style='color:#860fc6'>${user.uid}</span></li>" +
                        "<li><strong>qo</strong> 表示界面请求入参.如: <span style='color:#860fc6'>${qo.foobar}</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>${date}</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>${datetime}</span></li>" +
                        "<li><strong>其它任意freemarker内置表达式甚至标签语句</strong> <br/>" +
                        "　　例如: <span style='color:#860fc6'>foobar='${.now?string('yyyy-MM')}</span>'</li>" +
                        "　　又如: <span style='color:#860fc6'>1=1 &lt;#if qo.pid??&gt; and o.pid='${qo.pid}'&lt;/#if&gt;</span></li>" +
                        "</ol>"
                },
                ls_mge: '若关闭，将不显示树形表格右侧的管理栏',
                ls_mgeWidth: "设置表格最右边管理栏宽度<br/>根据配置的按钮数量适当调整",
                ls_rowNum: '是否在表格左侧显示行号',
                pageHandler: {
                    area: ["620px", "345px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>由您编写一个java类并实现<strong style='color:#d90909'>club.jdiy.dev.view.TreeHandler</strong>接口，" +
                        "以编程的方式干预列表界面，可完成更加复杂的业务逻辑处理</blockquote>" +
                        "首先在<span style='color:#d90909'>application.yml</span>中配置<span style='color:#d90909'>jdiy.handler-scan</span>(它用于指定增强器的类扫描根路径，" +
                        "若未配置，其默认值为 <span style='color:#d90909'>club.jdiy</span>)<br/>" +
                        "然后编写TreeHandler类(放在handler-scan所在包或任意子包路径下均可）；<br/>" +
                        "重启项目，JDiyAdmin会自动识别您编写的增强处理类并在这个下拉菜单中列出来，供您选择配置使用．"
                },
                headTips: '界面头部显示的提示文字信息，允许输入HTML代码',
                footTips: '界面底部显示的提示文字信息，允许输入HTML代码'
            },
            view: {
                label: "设置此字段要显示的标题文字",
                field: "设置要绑定显示的字段名. ",
                tField: "自定义一个非直接存库的字段显示(如Store附件字段名)，或自定义模板(此处可随意填)。",
                joinType: {
                    area: ["720px", "420px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>定义显示外部关联表中的相关字段。目前支持以下三种：</blockquote><ul>" +
                        "<li><strong>１．(多对一)外键关联：</strong><br/>　　当前绑定字段作为外键（其值=另一个表的主键id），显示另个表中的某个字段的值。</li>" +
                        "<li><strong>２．(一对多)外键关联：</strong><br/>　　与多对一相反，查找多表上的一些记录列表(根据多表上的某个外键关联本表id)，并显示那些记录的某字段值到此处（多条之间分号分隔）。此种配置方式，“绑定字段”无需设置。" +
                        "<li><strong>３．(多对多)外键关联：</strong><br/>　　显示多对多的映射关系(ManyToMany)，" +
                        "在表设计上，我们通常额外创建一张中间表(ref_xx_yy)来记录多对多关系，ref_xx_yy表只有两个字段(xx_id,yy_id),分别指向当前主表xx的id(正向关联字段),和外部表yy的id（反向关联字段）；" +
                        "此配置方式显示与当前表相关的它表数据列表（显示他表中的某个字段值，多个分号分隔）。</li></ul>"
                },
                joinTable: "指定外部表名",
                joinField: "选择要显示的外部表字段名",
                joinRef: "指定外部表的外键（关联本表id）",
                manyTable: "指定多对多中间关系表<br/>该表有两个字段分别关联主/外表的id",
                manyField0: "定义多对多中间关系表的正向关联字段（指向本表的id）",
                manyField1: "定义多对多中间关系表的反向关联字段（指向选项条目外部表的id）",
                type: '设置字段的显示方式',
                kv: "键值:键名[:颜色] 转换条目列表<br/>(一行填一个).<br/>格式：V:K 或 V:K:C <br/>其中：<br/>V=值,B=显示文字,C=显示颜色<br/>若无匹配项，将直接显示原值",
                dictId: "选择字典类型<br/>若字典中无匹配项，将显示原值",
                treeId: "若此处无可选项，需要您预先配置一个树界面",
                treeDisplay: "设置树形节点显示的层级路径分隔文字样式",
                tpl: {
                    area: ["800px", "580px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>通过EL表达式或者freemarker模板自定义输出显示." +
                        "</blockquote><ol>" +
                        "<li>内置变量 <strong>vo</strong> 表示当前行数据.如: <span style='color:#860fc6'>${vo.fieldName}</span>表示输出显示fieldName字段值</li>" +
                        "<li><strong>freemarker模板示例：</strong> <br/>" +
                        "<span style='color:#860fc6'>&lt;#if vo.state==1&gt;<br/>&nbsp;&nbsp;&nbsp;&nbsp;${vo.auditUser}审核于${vo.auditTime?string('yyyy-MM-dd HH:mm')}<br/>" +
                        "&lt;#else&gt;<br/>&nbsp;&nbsp;&nbsp;&nbsp;&lt;span style=\"color:red\"&gt;未审核&lt;/span&gt<br/>&lt;/#if&gt;</li>" +
                        "<li>除vo外，还可使用以下内置变量：</li>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前用户id:  <span style='color:#860fc6'>${user.id}</span>  又如取登录帐号： <span style='color:#860fc6'>${user.uid}</span></li>" +
                        "<li><strong>qo</strong> 界面请求入参(即QueryObject简写).如: <span style='color:#860fc6'>${qo.foobar}</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "<li><strong>jo</strong> 外联表数据(即JoinObject简写).当关联显示为<strong>多对一外联字段</strong>时有效，jo=外联数据．<br/>" +
                        "　　如: 当外联表名为b_vip时,<span style='color:#860fc6'>${jo.name}</span> 即取b_vip.name字段值</li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>${date}</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>${datetime}</span></li>" +
                        "<li><strong>ctx</strong> 表示当前应用程序上下文根路径</li>" +
                        "<li><strong>store</strong> 表示当前数据的JDiyStore对象，它是所有上传文件的存储对象，如获取名为pic附件字段的地址：${store.pic.url}</li>" +
                        "</ol>"
                },

                btn_text: "按钮的文字内容",
                btn_icon: "为钮选一个图标",
                btn_color: "设置按钮的背景或文字颜色",
                btn_act: "设置用户点击按钮做什么事情",
                btn_updateCode: {
                    area: ["720px", "480px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>设置点击按钮后，更新该数据的字段的值。若有多个字段要更新，则一行填一个." +
                        "<br/>格式：字段='新值'<br/>如果值是一个数字，则单引号可省略" +
                        "</blockquote>支持内嵌EL或FTL表达式(见如下内置变量或示例)<br/><ol>" +
                        "<li><strong>vo</strong> 表示当前数据.如: <span style='color:#860fc6'>num=${vo.num+1}</span></li>" +
                        "<li><strong>qo</strong> 表示URL地址中的参数.如: <span style='color:#860fc6'>${qo.xx}</span>等效于request.getParameter(\"xx\")</li>" +
                        "<li><strong>user</strong> 表示当前登录用户.如获取当前登录用户id存入auditUser_id字段:  <span style='color:#860fc6'>auditUser_id='${user.id}'</span></li>" +
                        "<li><strong>date</strong> 表示当前日期（返回yyyy-MM-dd格式的字符串）:  如：　<span style='color:#860fc6'>editDate='${date}'</span> </li>" +
                        "<li><strong>datetime</strong> 表示当前日期时间（返回yyyy-MM-dd HH:mm:ss格式的字符串）:  如：　<span style='color:#860fc6'>updateTime='${datetime}'</span></li>" +
                        "<li><strong>其它freemarker内置表达式</strong> <br/>" +
                        "　　例如: <span style='color:#860fc6'>foo='${.now?string('yyyy-MM')}'</span></li>" +
                        "　　又如: <span style='color:#860fc6'>finishFlag='${(vo.state==3)?string('1','0')}'</span></li>" +
                        "</ol>"
                },
                btn_ajaxUrl: {
                    area: ["620px", "390px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote' style='color:#c00;'>设置自定义的Ajax执行URL地址.<br/>" +
                        "<br/>示例：<span style='color:#860fc6'>${ctx}/mgmt/order/delete?id=${vo.id}</span><br/>" +
                        "其中<span style='color:#860fc6'>id=${vo.id}</span>可以省略（行管理按钮会自动添加该参数）</blockquote>" +
                        "支持内嵌EL或FTL表达式(见如下内置变量或示例)<br/><ol>" +
                        "<li><strong>ctx</strong> 表示当前应用程序上下文,相当于request.getContextPath()</li>" +
                        "<li><strong>vo</strong> 表示当前行数据(行按钮适用).如: ${ctx}/mgmt/foo/bar?name=<span style='color:#860fc6'>${vo.name}</span></li>" +
                        "<li><strong>qo</strong> 表示URL地址中的参数.如: <span style='color:#860fc6'>${qo.xx}</span>等效于request.getParameter(\"xx\")</li>" +
                        "</ol>"
                },
                btn_pageId: "设置点击按钮要打开的界面",
                btn_pageParam: {
                    area: ["500px", "420px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>使用url参数拼接格式<br/>" +
                        "支持EL表达式，行管理按钮可以用vo表示当前行数据<br/>例如：" +
                        "<br/><span style='color:#860fc6'>foo1=bar1&xx_id=${vo.id}</span><br/>" +
                        "此外还可以使用<span style='color:#860fc6'>qo</span>表示当前页面的URL地址入参(通常由菜单或其它界面内的操作按钮上配置传入)，" +
                        "<br/>如：<span style='color:#860fc6'>${qo.xx}</span>相当于request.getParameter(\"xx\")<br/>" +
                        "<span style='color:red'>特别注意：</span><br/>无需再配置ID传参，如：<span style='color:#860fc6'>id=${vo.id}</span><br/>" +
                        "(系统会自动添加＂当前表主键=值＂的参数至目标界面)" +
                        "</blockquote>"
                },
                btn_outLink: {
                    area: ["540px", "380px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>设置要打开的自定义页面地址<br/>" +
                        "可以是一个相对路径，也可以是一个外部http地址" +
                        "<br/><br/>例如：" +
                        "<br/><span style='color:#860fc6'>${ctx}/mgmt/foo/bar?xx_id=${vo.id}</span><br/>" +
                        "#其中${ctx}表示程序根路径(即contextPath)；vo表示当前主表数据对象<br/>" +
                        "<br/>又如：" +
                        "<br/><span style='color:#860fc6'>http:/foo.bar.com/xxx/xxx?id=${vo.id}</span>　　#打开外部http链接" +
                        "</blockquote>"
                },
                btn_target: "页面打开方式",
                btn_title: {
                    area: ["480px", "280px"],
                    title: "配置帮助",
                    content: '<blockquote class="layui-elem-quote">配置按钮点击后的弹出窗口(或Tab页)标题；<br/>' +
                        '空着不填则将按钮文字作为标题显示；<br/>' +
                        '支持EL表达式，用vo表示当前主表。<br/>' +
                        '例： <span style="color:#860fc6">${vo.name}修改</span>' +
                        "</blockquote>"
                },
                btn_width: "根据弹窗中显示的内容适当调整",
                btn_height: "根据弹窗中显示的内容适当调整",
                btn_confirm: "操作确认提示文字",
                btn_dbl: "若绑定，当用户双击表格行时，会自动触发此按钮的点击操作",
                btn_footer: "指示弹窗底部的[保存]和[取消]控制按钮是否显示",
                btn_grantCode: {
                    area: ["540px", "340px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>用于预定义界面内按钮级别的细粒度权限控制.<br/>" +
                        "<br/>例如[删除]按钮权限代码填<span style='color:#860fc6'>del</span>, 则[系统管理]-[角色管理]模块维护角色时,会在绑定了此界面的菜单右边出现按钮级别的权限控制项．<br/><br/>" +
                        "注：若同一界面内多个按钮的权限代码相同,这些按钮将自动归并为同一个权限控制项." +
                        "" +
                        "</blockquote>"
                },
                btn_conditionShow: {
                    area: ["620px", "300px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>根据条件判断按钮是否显示（不填无条件显示）." +
                        "<br/>例如：<span style='color:#860fc6'>vo.state==1 && vo.removed==0</span></blockquote><ol>" +
                        "<li>内置变量 <strong>vo</strong> 表示当前行数据(行按钮适用). 除vo外，还可使用以下内置变量：</li>" +
                        "<li><strong>user</strong> 表示当前登录用户.</li>" +
                        "<li><strong>qo</strong> 表示界面请求入参.如: <span style='color:#860fc6'>qo.foobar</span> 就类似于java中的 request.getParameter(\"foobar\")</li>" +
                        "</ol>"
                },

                tab_label: "设置此Tab显示的标题",
                tab_type: "设置此Tab的内容界面类型",
                tab_goPageId: "设置此Tab的目标界面",
                tab_goPageParam: "设置目标界面URL传参<br/>格式:<br/>prm1=111&prm2=222<br/>(支持FTL表达式)<br/>提示：目标界面中可以在相应支持FTL的配置项中用qo接收参数",
                tab_goPageFilter: "此配置项针对列表界面和树形界面有效，用于为目标界面的数据查询添加全局的SQL条件过滤。<br/>用字母o表示目标界面的主表",
                tab_goUrl: '设置自定义页面的URL地址<br/>通常以 ${' + 'ctx}/mgmt/ 开头，<br/>即指向当前应用程序后台路径下的某个地址。',

                tabPos: '定义与当前详情页相关联的子表业务数据TAB显示位置',
                tabTitle: '若配置了子表TAB,此配置用于设置默认的主表TAB标题',
                vi_btn: '配置是否在详情页下方显示操作按钮',
                pageHandler: {
                    area: ["620px", "345px"],
                    title: "配置帮助",
                    content: "<blockquote class='layui-elem-quote'>由您编写一个java类并实现<strong style='color:#d90909'>club.jdiy.dev.view.ViewHandler</strong>接口，" +
                        "以编程的方式干预详情页界面，可完成更加复杂的业务逻辑处理</blockquote>" +
                        "首先在<span style='color:#d90909'>application.yml</span>中配置<span style='color:#d90909'>jdiy.handler-scan</span>(它用于指定增强器的类扫描根路径，" +
                        "若未配置，其默认值为 <span style='color:#d90909'>club.jdiy</span>)<br/>" +
                        "然后编写FormHandler类(放在handler-scan所在包或任意子包路径下均可）；<br/>" +
                        "重启项目，JDiyAdmin会自动识别您编写的增强处理类并在这个下拉菜单中列出来，供您选择配置使用．"
                },

                headTips: '界面头部显示的提示文字信息，允许输入HTML代码',
                footTips: '界面底部显示的提示文字信息，允许输入HTML代码'
            }
        };
    exports('jdhp', {
        form: man.form,
        list: man.list,
        tree: man.tree,
        view: man.view,
    });
});